<?php

namespace sisonenet\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ImagenPequeFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id_img_old'=>'required',
            'titulo_imagen'=>'required',
            'imagen'=>'required|mimes:jpeg,bmp,png,gif|dimensions:min_width=500,min_height=300,max_width=700,max_height=500',
        ];
    }
}
