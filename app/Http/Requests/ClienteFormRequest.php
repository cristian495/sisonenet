<?php

namespace sisonenet\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClienteFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre_razon'=>'required|max:120',
            'dni_ruc'=>'required',
            'telefono'=>'required',
            //'email'=>'email',
            //'direccion'=>'required',
            //'imagen_satelital'=>'mimes:jpeg,bmp,png,gif',
            'tipo_via'=>'required',
            'nombre_via'=>'required',
            'numero_vivienda'=>'required',
            'tipo_zona'=>'required',
            'departamento'=>'required',
            'provincia'=>'required',
            'distrito'=>'required',
            'referencia'=>'required',
            'nombre_de_usuario'=>'required|unique:users,name',
            'clave'=>'required'
        ];
    }
}
