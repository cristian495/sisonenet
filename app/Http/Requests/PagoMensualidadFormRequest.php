<?php

namespace sisonenet\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PagoMensualidadFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'idcliente'=>'required|integer',
            'idmensualidad'=>'required|integer',
            'tipo_comprobante'=>'required|max:8',
            'num_comprobante'=>'required|max:20',
            'costo_total' => 'required|numeric|between:0,4999.99',
        ];
    }
}
