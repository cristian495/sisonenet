<?php

namespace sisonenet\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class contratoPaginaFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if(request('quien')=="persona")
        {
            return [
                'tipo_persona'=>'required',
                'apellido_paterno'=>'required',
                'apellido_materno'=>'required',
                'nombres'=>'required',
                'dni'=>'required|max:40',
                'sexo'=>'required|max:10',
                'telefono'=>'required|max:10',
                //'operador_telefono'=>'required|max:20',
                'email'=>'required|max:100',
                'tipo_via'=>'required',
                'nombre_via'=>'required',
                'numero_vivienda'=>'required',
                'tipo_zona'=>'required',
                'departamento'=>'required',
                'provincia'=>'required',
                'distrito'=>'required',
                'nombre_plan'=>'required',
                'precio_mensual'=>'required',
                'inicio_del_contrato'=>'required',
                'fin_del_contrato'=>'required',
                'fecha_de_pago'=>'required',
                'fecha_de_corte'=>'required',
                'duracion_del_contrato'=>'required',
                'tipo_pago_ap'=>'required',
                'tipo_comprobante'=>'required',
            ];
        }else if (request('quien')=="empresa")
        {
            return [
                'tipo_persona'=>'required',
                'razon_social' => 'required',
                'ruc' => 'required',
                'telefono'=>'required|max:10',
                //'operador_telefono'=>'required|max:20',
                'email'=>'required|max:100',
                'tipo_via'=>'required',
                'nombre_via'=>'required',
                'numero_vivienda'=>'required',
                'tipo_zona'=>'required',
                'departamento'=>'required',
                'provincia'=>'required',
                'distrito'=>'required',
                'nombre_plan'=>'required',
                'precio_mensual'=>'required',
                'inicio_del_contrato'=>'required',
                'fin_del_contrato'=>'required',
                'fecha_de_pago'=>'required',
                'fecha_de_corte'=>'required',
                'duracion_del_contrato'=>'required',
                'tipo_pago_ap'=>'required',
                'tipo_comprobante'=>'required',
            ];
        }else{
            return array('error interno'=>'error');
        }

    }
}
