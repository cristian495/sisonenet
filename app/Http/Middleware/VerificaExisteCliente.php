<?php

namespace sisonenet\Http\Middleware;
use sisonenet\Cliente;
use Closure;
use Illuminate\Support\Facades\Input;

class VerificaExisteCliente
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handle($request, Closure $next)
    {
        $apellido = Input::get('apellido_paterno').' '.Input::get('apellido_materno');
        $query='';

        if(Input::get('tipo_persona')=='persona')
        {
            $query= Cliente::where('dni','=',Input::get('dni'))
                ->where('estado','=','1')
                ->first();

        }elseif(Input::get('tipo_persona')=='empresa')
        {
            $query= Cliente::where('dni','=',Input::get('ruc'))
                ->where('nombre','=',Input::get('razon_social'))
                ->where('estado','=','1')
                ->first();
        }else
        {
            return redirect()->back();
        }

        if( $query != null)
        {
            session(['error_registro'=>'yes']);
            return redirect()->to( '/registro_cancelado');
        }

        return $next($request);
    }
}
