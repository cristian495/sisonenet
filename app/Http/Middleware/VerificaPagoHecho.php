<?php

namespace sisonenet\Http\Middleware;

use Closure;

class VerificaPagoHecho
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$request->session()->has('paypal_payment_id')) {
           return redirect()->to('/');
        }

        return $next($request);
    }
}
