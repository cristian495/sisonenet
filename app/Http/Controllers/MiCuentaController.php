<?php

namespace sisonenet\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;
use sisonenet\User;
use sisonenet\Http\Requests\MiUsuarioFormRequest;



class MiCuentaController extends Controller
{
    public function index()
    {
        $user_client = User::select(['id','name','email'])
            ->where('idcliente','=',Auth::user()->idcliente)
            ->first();

        return view('consultas.micuenta.index',["user_client"=>$user_client]);
    }

    public function edit_password(MiUsuarioFormRequest $request)
    {
        $data = $request->all();
        $idusuario= $data['idusuario'];


        $user= User::findOrFail($idusuario);
        $user->password= bcrypt($data['new_pass']);

        if($user->update())
        {
            return view("mensajes.msj_correcto")->with("msj","Contraseña cambiada correctamente");
        }else
        {
            return view("mensajes.msj_rechazado")->with("msj","Error al editar la contraseña");
        }
    }
}
