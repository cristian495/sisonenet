<?php

/*
 * Taken from
 * https://github.com/laravel/framework/blob/5.3/src/Illuminate/Auth/Console/stubs/make/controllers/HomeController.stub
 */

namespace sisonenet\Http\Controllers;

use sisonenet\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index()
    {
        /*$cliente = DB::table('cliente')
                        ->select('nombre_razon')
                        ->where('idcliente','=',Auth::user()->idcliente)
                        ->first();
        $ID = Auth::user()->idcliente;
        session(['cliente_session' => $cliente]);

        $num_clientes = $this->getNumeroClientes();
        $num_contratos = $this->getNumeroContratos();
        $planes = $this->getNombresPlanes();


        return view('adminlte::home',["num_clientes"=>$num_contratos
                                        ,"num_contratos"=>$num_contratos
                                         ,"planes"=>$planes]);*/
        return $this->getCantidadPlanes();
    }

    public function  getNumeroClientes()
    {
        $num_clientes = DB::table('cliente')

            ->where('estado','=','1')
                        ->count();
        return $num_clientes;
    }
    public function  getNumeroContratos()
    {
        $num_contratos = DB::table('contrato')

            ->where('estado','=','1')
            ->count();
        return $num_contratos;
    }

    public function getNombresPlanes()
    {
        $paquetes = DB::table('paquete')
                    ->select('nombre')
                    ->where('estado','=','1')
                    ->get();
        return $paquetes;
    }
    public function getCantidadPlanes()
    {
        $paquetes = DB::table('contrato')
            ->select('idpaquete',
                    DB::raw('count("idpaquete") as cant_pa'))
            ->groupBy('idpaquete')
            ->get();
        return $paquetes;
    }
}