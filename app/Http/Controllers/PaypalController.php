<?php

namespace sisonenet\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;
use PayPal\Api\CurrencyConversion;
use Illuminate\Support\Facades\Input;
use sisonenet\Http\Requests\contratoPaginaFormRequest;


class PaypalController extends Controller
{
    private $_api_context;


    public function __construct()
    {
        // setup PayPal api context
        $paypal_conf = \Config::get('paypal');
        $paypal_conf = config('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential($paypal_conf['client_id'], $paypal_conf['secret']));
        $this->_api_context->setConfig($paypal_conf['settings']);
    }

    /** FUNCION PARA CONVERTIR DE SOLES A DOLARES AMERICANOS**/
    private function convertCurrency($amount, $from, $to){
        $data = file_get_contents("https://finance.google.com/finance/converter?a=$amount&from=$from&to=$to");
        preg_match("/<span class=bld>(.*)<\/span>/",$data, $converted);
        $converted = preg_replace("/[^0-9.]/", "", $converted[1]);
        return number_format(round($converted, 1),1);
    }


    /** FUNCION PARA CALCULAR PORCENTAJE**/
    function porcentaje($cantidad,$porciento,$decimales){
        return number_format($cantidad*$porciento/100 ,$decimales);
    }

    /** CALCULAR COSTO DEL AP Y EL PRIMER MES **/
    public function costo_access_point($tipo_pago,$costo_soles_ap,$num_meses){
        if($tipo_pago=="mensual")
        {
            $ap_mensual = round((floatval($costo_soles_ap)/intval($num_meses))+floatval($this->porcentaje(floatval($costo_soles_ap),1.13,1)),1);
        }
        else if($tipo_pago =="contado")
        {
            $ap_mensual = round(floatval($costo_soles_ap),1);
        }
        else
        {
            $ap_mensual = 0.00;
        }

        return $ap_mensual;
    }

    /** CALCULAR COSTO PRIMER MES**/
    public function costo_primer_mes($costo_usd_instalacion,$costo_usd_plan,$costo_usd_ap)
    {

            $costo_usd_primermes = round(
                                    floatval($costo_usd_instalacion) +
                                    floatval($costo_usd_plan) +
                                    floatval($costo_usd_ap)
                                 ,1);
        return $costo_usd_primermes;
    }

    /** FUNCION PARA CREAR UNA SESION CON LOS DATOS DEL CLIENTE Y EL PAQUETE QUE HA ELEGIDO**/
    private function setSessionClientePlan($request,$costo_ap_mensual)
    {
        $nombre = '';
        $apellido='';
        $dni_ruc='';

        if($request->tipo_persona =='persona')
        {
            $nombre = $request->nombres;
            $apellido = $request->apellido_paterno . ' ' . $request->apellido_materno;
            $dni_ruc = $request->dni ;
        }
        elseif($request->tipo_persona =='empresa')
        {
            $nombre = $request->razon_social;
            $apellido = '';
            $dni_ruc = $request->ruc ;
        }
        else{
            $nombre ='No ingresó';
            $apellido = 'No ingresó';
            $dni_ruc = 'No ingresó' ;
        }



        $cliente_plan = array(
            'cliente'=>[
                'tipo_persona'=> $request->tipo_persona,
                'nombre'=> $nombre,
                'apellido'=>  $apellido,
                'dni_ruc'=>  $dni_ruc,
                'sexo'=>  $request->sexo,
                'telefono'=>  $request->telefono,
                'telefono_adicional'=>  $request->telefono_adicional,
                'operador_telefono'=>  $request->operador_telefono,
                'email'=>  $request->email,
                'tipo_via'=>  $request->tipo_via,
                'nombre_via'=>  $request->nombre_via,
                'numero_via'=>  $request->numero_via,
                'numero_vivienda'=>  $request->numero_vivienda,
                'tipo_zona'=>  $request->tipo_zona,
                'nombre_zona'=>  $request->nombre_zona,
                'departamento'=>  $request->departamento,
                'provincia'=>  $request->provincia,
                'distrito'=>  $request->distrito,
                'referencia'=>  $request->referencia,

            ],

            'plan' => [
                'id_plan'=>session('plan')->idpaquete,
                'nombre_plan'=>session('plan')->nombre,
                'precio_plan'=>session('plan')->precio_mensual,
                'megas_descarga'=> session('plan')->megas_bajada,
                'costo_ap'=> session('costo_ap'),
                'costo_cuotas_ap'=> $costo_ap_mensual,
                'costo_instalacion'=> session('costo_instalacion'),
                'tipo_pago_ap'=>  $request->tipo_pago_ap,
                'inicio_del_contrato'=>  $request->inicio_del_contrato,
                'fin_del_contrato'=>  $request->fin_del_contrato,
                'fecha_de_pago'=>  $request->fecha_de_pago,
                'fecha_de_corte'=>  $request->fecha_de_corte,
                'duracion_del_contrato'=>  $request->duracion_del_contrato,
                'tipo_comprobante'=>  $request->tipo_comprobante,
            ]
        );

        session(['cliente_plan' => $cliente_plan]);

    }


    public function postPayment(contratoPaginaFormRequest $request)
    {

        $request->all();

        ///DATOS DEL PLAN

        $nombre_plan = session('plan')->nombre;

        $costo_plan = session('plan')->precio_mensual;

        $costo_accespoint = session('costo_ap');

        $costo_instalacion= session('costo_instalacion');

        $inicioContrato = Carbon::createFromFormat('d/m/Y',$request->input('inicio_del_contrato'));

        $finContrato = Carbon::createFromFormat('d/m/Y',$request->input('fin_del_contrato'));

        $numMeses = intval($finContrato->diffInMonths($inicioContrato))+1;

        $costoPrimerMes = 0.00;

        $ap_mensual_soles = $this->costo_access_point($request->tipo_pago_ap,$costo_accespoint,$numMeses);


        ///////////////////


        // CONVERSION DE SOLES A DOLARES

        $costo_plan_usd = $this->convertCurrency(round($costo_plan,1), "PEN", "USD");
        $costo_instalacion_usd = $this->convertCurrency(round($costo_instalacion,1), "PEN", "USD");
        $costo_apmensual_usd = $this->convertCurrency($ap_mensual_soles, "PEN", "USD");
        $costo_primer_mes_usd = $this->costo_primer_mes($costo_instalacion_usd,$costo_plan_usd,$costo_apmensual_usd);
        //$costo_primer_mes_usd = $this->convertCurrency($primera_mensualidad_soles, "PEN", "USD");

        //////////////////////



        //return $ap_mensual.'<br>'.$costo_apmensual_usd;
        //return $costo_plan_usd.'<br>'.$costo_instalacion_usd.'<br>'.$costo_apmensual_usd.'<br><br>'.$costo_primer_mes_usd;
        //return $costo_plan_usd.'  '.$costo_instalacion_usd.'  '.$costo_apmensual_usd.'  '.$costo_primer_mes_usd.'total: '.cos;

        $payer = new Payer();
        $payer->setPaymentMethod('paypal');

        $item1 = new Item();
        $item1->setName('Costo mensual del plan')
            ->setCurrency('USD')
            ->setQuantity(1)
            ->setPrice($costo_plan_usd);

        $item2 = new Item();
        $item2->setName('Instalación')
            ->setCurrency('USD')
            ->setQuantity(1)
            ->setPrice($costo_instalacion_usd);

        $item3 = new Item();
        $item3->setName('Acces Point ('. $request->tipo_pago_ap .')')
            ->setCurrency('USD')
            ->setQuantity(1)
            ->setPrice($costo_apmensual_usd);


        $item_list = new ItemList();
        $item_list->setItems(array($item1,$item2,$item3));
        //$item_list->addItem($item1);


        $amount =  new Amount();
        $amount ->setCurrency('USD')
                ->setTotal(strval($costo_primer_mes_usd));

        $transaction = new Transaction();
        $transaction->setAmount($amount)
                    ->setItemList($item_list)
                    ->setDescription('Primera mensualidad del plan: '. $nombre_plan);


        $redirectUrls = new RedirectUrls();
        $redirectUrls->setReturnUrl(url('payment/status'))
                     ->setCancelUrl(url('servicios/internet_inalambrico'));

        $payment = new Payment();
        $payment->setIntent('sale')
                ->setPayer($payer)
                ->setTransactions(array($transaction))
                ->setRedirectUrls($redirectUrls);

        try
        {
            $payment->create($this->_api_context);
            $this->setSessionClientePlan($request,$ap_mensual_soles);

        }
        catch (\PayPal\Exception\PayPalConnectionException $ex)
        {
            echo $ex->getData();
        }

        $approvalUrl = $payment->getApprovalLink();
        session(['paypal_payment_id' => $payment->getId()]);

        return redirect()->away($approvalUrl);
    }











    public function getPaymentStatus()
    {
        // Get the payment ID before session clear
        $payment_id = session('paypal_payment_id');

        $payerId = Input::get('PayerID');
        $token = Input::get('token');


        if (empty($payerId) || empty($token)) {
            return redirect()->to('/')
                ->with('message', 'Hubo un problema al intentar pagar con Paypal');
        }

        $payment = Payment::get($payment_id, $this->_api_context);

        // PaymentExecution object includes information necessary
        // to execute a PayPal account payment.
        // The payer_id is added to the request query parameters
        // when the user is redirected from paypal back to your site
        $execution = new PaymentExecution();
        $execution  ->setPayerId(Input::get('PayerID'))
                    ->setTransactions($payment->getTransactions());
        //Execute the payment
        $result = $payment->execute($execution, $this->_api_context);

        //echo '<pre>';print_r($result);echo '</pre>';exit; // DEBUG RESULT, remove it later
        if ($result->getState() == 'approved')
        { // payment made
            // Registrar el pedido --- ok
            // Registrar el Detalle del pedido  --- ok
            // Eliminar carrito
            // Enviar correo a user
            // Enviar correo a admin
            // Redireccionar
            //$this->saveOrder(\Session::get('cart'));

            session()->forget('plan');
            session()->forget('costo_ap');
            session()->forget('costo_instalacion');
            return redirect()->to('/compra_correcta');
        }


        return redirect()->to('/')
            ->with('message', 'La compra fue cancelada');
    }

}
