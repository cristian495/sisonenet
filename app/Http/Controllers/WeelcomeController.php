<?php

namespace sisonenet\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class WeelcomeController extends Controller
{
    public function index(){
        $imgPeque01 = DB::table('publicidad')
                            ->select('ruta_imagen','titulo')
                            ->where('tipo_imagen','=','peque01')
                            ->first();
        $imgPeque02 = DB::table('publicidad')
                            ->select('ruta_imagen','titulo')
                            ->where('tipo_imagen','=','peque02')
                            ->first();

        return view('webPage.welcome',['imgPeque01'=>$imgPeque01,'imgPeque02'=>$imgPeque02]);
    }
}
