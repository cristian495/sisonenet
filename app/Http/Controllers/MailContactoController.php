<?php

namespace sisonenet\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use Session;
use Redirect;
//use Illuminate\Support\Facades\Response;

class MailContactoController extends Controller
{
    public function enviar(Request $request)
    {
        $resul = Mail::send('webPage.emails.contacto',$request->all(),function($msj){
            $msj -> subject('Correo de contacto');
            $msj -> to (config('constantes.my_email'));
            // HOSTING
            //$msj -> to ('rpicong@onenet.com.pe','informes@onenet.com.pe');
        });

        Session::flash('mensaje_contacto','Mensaje enviado correctamente');
        return Redirect::to('/contactenos');

    }

    public function cotizacionMail(Request $request)
    {
        //return $request->all();
        $resul = Mail::send('webPage.emails.cotizacion',$request->all(),function($msj){
            $msj -> subject('Solicitud de cotización');
            $msj -> to (config('constantes.my_email'));
        });
        if(!Mail::failures())
        {
            return view("mensajes.msj_correcto")->with("msj","Mensaje enviado correctamente");
        }
        else
        {
            return view("mensajes.msj_rechazado")->with("msj","Hubo un error vuelva a intentarlo");
        }
    }
}
