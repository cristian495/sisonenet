<?php

namespace sisonenet\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use sisonenet\Cliente;
use sisonenet\User;
use sisonenet\Contrato;
use sisonenet\Mensualidad;
use sisonenet\PagoMensualidad;
use sisonenet\DetallePago;
use sisonenet\Paquete;
use DB;
use PDF;
use Mail;


class PagoCorrectoController extends Controller
{
    /** FUNCION PARA CALCULAR PORCENTAJE**/
    function porcentaje($cantidad,$porciento,$decimales){
        return number_format($cantidad*$porciento/100 ,$decimales);
    }

    private function crearCliente(){
        $nombre_completo = session('cliente_plan.cliente.nombre'). ' '.session('cliente_plan.cliente.apellido');
        $cliente= new Cliente();
        $cliente->tipo_persona= session('cliente_plan.cliente.tipo_persona');
        $cliente->nombre_razon= session('cliente_plan.cliente.nombre').' '.session('cliente_plan.cliente.apellido');
        //$cliente->apellido= session('cliente_plan.cliente.apellido');
        $cliente->nombre_razon = $nombre_completo;
        $cliente->dni= session('cliente_plan.cliente.dni_ruc');
        $cliente->telefono= session('cliente_plan.cliente.telefono');
        $cliente->telefono_adicional= session('cliente_plan.cliente.telefono_adicional');

        $cliente->tipo_via= session('cliente_plan.cliente.tipo_via');
        $cliente->nombre_via= session('cliente_plan.cliente.nombre_via');
        $cliente->numero_vivienda= session('cliente_plan.cliente.numero_vivienda');
        $cliente->tipo_zona= session('cliente_plan.cliente.tipo_zona');
        $cliente->nombre_zona= session('cliente_plan.cliente.nombre_zona');
        $cliente->departamento= session('cliente_plan.cliente.departamento');
        $cliente->provincia= session('cliente_plan.cliente.provincia');
        $cliente->distrito= session('cliente_plan.cliente.distrito');
        $cliente->referencia_direccion= session('cliente_plan.cliente.referencia');

        $resul = $cliente->save();

        $idcliente = $cliente->idcliente;

        $user = new User();
        $user->idcliente = $idcliente;
        $user->name = session('cliente_plan.cliente.dni_ruc');
        $user->email= session('cliente_plan.cliente.email');
        $user->password = bcrypt(session('cliente_plan.cliente.dni_ruc'));
        $user->tipo_usuario= '0';
        $user->save();

        if($resul)
        {
            return $idcliente;
            /*$user = Auth::User();
            return $user;*/
        }
        else
        {
            return 'error al crear el cliente';
        }
    }


    private function crearContrato($idCliente){
        //   try {
        $num_cuotas=0;
        $costoPrimerMes = 0;
        $demasMeses = 0;
        //    DB::beginTransaction();

        $contrato= new Contrato();

        $contrato->idcliente= $idCliente;
        $contrato->idpaquete= session('cliente_plan.plan.id_plan');
        $fechaInicio = Carbon::createFromFormat('d/m/Y', session('cliente_plan.plan.inicio_del_contrato'))->format('Y-m-d');
        $contrato->fecha_inicio_contrato= $fechaInicio;
        $contrato->duracion_contrato= session('cliente_plan.plan.duracion_del_contrato');
        $contrato->fecha_fin_contrato=Carbon::createFromFormat('d/m/Y', session('cliente_plan.plan.fin_del_contrato'))->format('Y-m-d');
        $contrato->fecha_pago_servicio= session('cliente_plan.plan.fecha_de_pago');
        $contrato->fecha_corte= session('cliente_plan.plan.fecha_de_corte');
        $contrato->costo_ap= session('cliente_plan.plan.costo_ap');
        $contrato->tipo_pago_ap= session('cliente_plan.plan.tipo_pago_ap');
        $contrato->costo_instalacion= session('cliente_plan.plan.costo_instalacion');

        $costo_instalacion  = session('cliente_plan.plan.costo_instalacion');
        $costo_mensualidad  = session('cliente_plan.plan.precio_plan');
        $costo_ap           = session('cliente_plan.plan.costo_ap');


        $inicioContrato = Carbon::createFromFormat('d/m/Y',session('cliente_plan.plan.inicio_del_contrato'));
        $finContrato = Carbon::createFromFormat('d/m/Y',session('cliente_plan.plan.fin_del_contrato'));
        $num_cuotas = intval($finContrato->diffInMonths($inicioContrato))+1;


       // $num_cuotas=(intval(session('cliente_plan.plan.duracion_del_contrato'))*13);

        //$tipo_pago_ap       = $data['tipo_pago_ap'];

        if(session('cliente_plan.plan.tipo_pago_ap') == 'contado')
        {
            $apmensual=0.00;
            $costoPrimerMes = floatval($costo_instalacion) + floatval($costo_ap) + floatval($costo_mensualidad);
            $demasMeses = $costo_mensualidad;

        }elseif (session('cliente_plan.plan.tipo_pago_ap') == 'mensual')
        {

            $apmensual = round(floatval($costo_ap)/$num_cuotas + floatval($this->porcentaje(floatval($costo_ap),1.13,1)),1);
            $costoPrimerMes= floatval($costo_instalacion) + floatval($costo_mensualidad)+ $apmensual;
            $demasMeses = floatval($costo_mensualidad)+$apmensual;
        }

        $contrato->costo_ap_mensualmente= $apmensual;

        $contrato->save();

        $idcontrato = $contrato->idcontrato;
        $cFechaInicio = Carbon::createFromFormat('d/m/Y',session('cliente_plan.plan.inicio_del_contrato'));
        $cFechaFin= Carbon::createFromFormat('d/m/Y',session('cliente_plan.plan.fin_del_contrato'));



        $cont = 1;

        for($date = $cFechaInicio; $cont <= $date->lte($cFechaFin); $date->addMonthsNoOverflow(1))
        {

            $mensualidad = new Mensualidad();

            $mensualidad->idcontrato = $idcontrato;
            $mensualidad->num_cuota = $cont;

            if($cont == 1)
            {
                $mensualidad->costo= $costoPrimerMes;
            }else
            {
                $mensualidad->costo= $demasMeses;
            }

            $mensualidad->fecha_pago = $date->toDateString();
            $mensualidad->estado = 'pendiente';
            $mensualidad->color_estado = 'amarillo';


            $mensualidad->save();

            if($cont == 1)  $idmensualidadprimera = $mensualidad->idmensualidad;

            $cont = $cont + 1;
        }

        /*
         * PAGO DE LA PRIMERA MENSUALIDAD
         */

        $pagoMensualidad = new PagoMensualidad();
        $pagoMensualidad->idcliente  = $idCliente;
        $pagoMensualidad->fecha_hora_pagado = Carbon::createFromFormat('d/m/Y', session('cliente_plan.plan.inicio_del_contrato'))->format('Y-m-d H:i:s');
        $pagoMensualidad->tipo_comprobante = session('cliente_plan.plan.tipo_comprobante');
        $pagoMensualidad->serie_comprobante = '';
        $pagoMensualidad->num_comprobante  = substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 10);
        $pagoMensualidad->costo_mensualidad = $costoPrimerMes;
        $pagoMensualidad->total_pago= $costoPrimerMes;
        $pagoMensualidad->estado= 'pagado';

        $pagoMensualidad->save();

        $idpagomensualidad = $pagoMensualidad->idpago_mensualidad;


        /*
         *  ACTUALIZACION A PAGADO DE LA PRIMERA MENSUALIDAD
         */

        Mensualidad::where('idcontrato', '=', $idcontrato)
            ->where('idmensualidad','=',$idmensualidadprimera)
            ->where('num_cuota','=','1')
            ->update([
                'estado' => 'pagado',
                'color_estado' => 'verde'
            ]);

        /*
         *  DETALLE DEL PRIMER PAGO DE MENSUALIDAD
         */

        $detallePago = new DetallePago();
        $detallePago->idpago_mensualidad = $idpagomensualidad ;
        $detallePago->idmensualidad = $idmensualidadprimera;

        $detallePago->save();

        /*
         * ACTUALIZACION CAMPO CLIENTE PARA QUE FIGURE QUE TIENE UN CONTRATO
         */
        $cliente= Cliente::findOrFail($data['cliente']);
        $cliente->en_contrato = '1';
        $cliente->update();


        //     DB::commit();
        return true;


        // }catch (\Exception $e) {
        //     DB::rollback();
        //    return view("mensajes.msj_rechazado")->with("msj","hubo un error vuelva a intentarlo");
        //  }
    }


    public function generatePdf()
    {
        $paquetes = Paquete::select(['idpaquete','nombre','precio_mensual','megas_bajada'])
            ->where('estado','=','1')
            ->get();

             if(session('cliente_plan.plan.tipo_pago_ap') =='mensual')
             {
                 $ap_mensual= session('cliente_plan.plan.costo_cuotas_ap');
            }
             elseif(session('cliente_plan.plan.tipo_pago_ap') =='contado')
             {
                $ap_mensual = 0.00;
             }
        //$direccion_cliente = $this->getDireccionCompletaCliente();

        $var = [
            'cliente_nombre' => session('cliente_plan.cliente.nombre'),
            'cliente_apellido' => session('cliente_plan.cliente.apellido'),
            'cliente_dni' => session('cliente_plan.cliente.dni_ruc'),
            'cliente_email' => session('cliente_plan.cliente.email'),
            'tipo_via' => session('cliente_plan.cliente.tipo_via'),
            'nombre_via' => session('cliente_plan.cliente.nombre_via'),
            'numero_via' => session('cliente_plan.cliente.numero_via'),
            'numero_vivienda' => session('cliente_plan.cliente.numero_vivienda'),
            'tipo_zona' => session('cliente_plan.cliente.tipo_zona'),
            'nombre_zona' => session('cliente_plan.cliente.nombre_zona'),
            'departamento' => session('cliente_plan.cliente.departamento'),
            'provincia' => session('cliente_plan.cliente.provincia'),
            'distrito' => session('cliente_plan.cliente.distrito'),
            'referencia' => session('cliente_plan.cliente.referencia'),
            'cliente_telefono' => session('cliente_plan.cliente.telefono'),
            'cliente_telefono_adicional' => session('cliente_plan.cliente.telefono_adicional'),
            'anios_contrato' => session('cliente_plan.plan.duracion_del_contrato'),
            'costo_cuotas_ap' => $ap_mensual,
            'fecha_de_pago' => session('cliente_plan.plan.fecha_de_pago'),
            'id_plan' => session('cliente_plan.plan.id_plan')
        ];


        $html = view('plantillas_pdf.contrato',['var'=>$var,'planes'=>$paquetes])->render();

        PDF::SetTitle('contrato_'.session('cliente_plan.cliente.nombre').'_'.session('cliente_plan.cliente.apellido'));
        PDF::AddPage();
        PDF::writeHTML($html, true, false, true, false, '');
        PDF::Output('contrato_'.session('cliente_plan.cliente.nombre').'_'.session('cliente_plan.cliente.apellido'));



    }

    public function enviarEmail()
    {
        //$men = Mensualidad::findOrFail('1');
        //highlight_string("<?php\n\$data =\n" . var_export($men, true) . ";\n");
        $direccion_cliente = session('cliente_plan.cliente.tipo_via')
            .' '
            .session('cliente_plan.cliente.nombre_via')
            .' '
            /*.session('cliente_plan.cliente.numero_via')
            .' '*/
            .session('cliente_plan.cliente.numero_vivienda')
            .' - '
            .session('cliente_plan.cliente.tipo_zona')
            .' '
            .session('cliente_plan.cliente.nombre_zona')
            .' - '
            .session('cliente_plan.cliente.distrito')
            .' '
            .session('cliente_plan.cliente.provincia')
            .' '
            .session('cliente_plan.cliente.departamento');

        $cliente= $data = [
            'nombre_cliente' => session('cliente_plan.cliente.nombre'),
            'direccion_cliente' =>$direccion_cliente,
            'nombre_plan' => session('cliente_plan.plan.nombre_plan'),
            'usuario' => session('cliente_plan.cliente.dni_ruc'),
            'password' => session('cliente_plan.cliente.dni_ruc'),
            'fecha_de_pago' => session('cliente_plan.plan.fecha_de_pago')
        ];;
        //var_dump($cliente[0]['plan']);
        $resul = Mail::send('webPage.emails.felicitaciones_contrato',['cliente'=>$cliente],function($msj)
        {
            $msj -> subject('Bienvenido a onenet');
            $msj -> to (config('constantes.my_email'));
        });

       // return view('webPage.emails.felicitaciones_contrato',['cliente'=>$cliente]);
    }

    public function index()
    {

        /*if(session('cliente_plan'))
        {*/
            $idCliente = $this->crearCliente();
            $this->crearContrato($idCliente);
            $this->enviarEmail();
            return view('webPage.adquirir_plan.contrato_exitoso');
        /*}else{*/
             //return $this->enviarEmail();
            //return redirect()->to('/');
        /*}*/

       //$this->generatePdf();


    }
}
