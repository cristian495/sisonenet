<?php

namespace sisonenet\Http\Controllers;

use Illuminate\Http\Request;


use sisonenet\Contrato;
use sisonenet\DetallePago;
use sisonenet\Mensualidad;
use sisonenet\Paquete;
use sisonenet\Cliente;
use PDF;
use sisonenet\AntenaEmisora;
use sisonenet\PagoMensualidad;
use sisonenet\ViewContrato;
use sisonenet\Http\Requests;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use sisonenet\Http\Requests\ContratoFormRequest;
use Yajra\Datatables\Facades\Datatables;
use Carbon\Carbon;
use DB;
use Barryvdh\DomPDF\Facade as PDFDOM;
use Excel;




class ContratoController extends Controller
{/** FUNCION PARA CALCULAR PORCENTAJE**/
    function porcentaje($cantidad,$porciento,$decimales){
        return number_format($cantidad*$porciento/100 ,$decimales);
    }

    
    public function index(){
        return view('servicio.contratos.index');
    }
   public function getContratos()
    {
        $contratos = DB::table('view_contrato_online')
            ->select('id',
                    //DB::raw('CONCAT(nombre_cliente,\' \',apellido_cliente) AS nombre_apellido'),
                    'idcliente',
                    'nombre_razon_cliente',
                    'telefono',
                    //DB::raw('CONCAT(precio_mensual,\' soles\') AS precio_mensual'),
                    DB::raw('(precio_mensual+costo_ap_mensualmente) AS precio_mensual'),
                    DB::raw('CONCAT(megas_descarga_comercial,\' Megas\') AS megas'),
                    DB::raw('CONCAT(fecha_pago,\'\') AS fecha_pago'),
                    'estado')
                     //DB::raw('DATE_FORMAT(fecha_inicio_contrato,\'%d/%m/%Y %h:%i %p\' ) AS fecha_inicio_contrato'),
                     //DB::raw('TO_CHAR(fecha_inicio_contrato,\'dd/mm/yyyy hh24:mi:ss\' ) AS fecha_inicio_contrato'),
//                     'duracion_contrato',
                     //DB::raw('DATE_FORMAT(fecha_fin_contrato,\'%d/%m/%Y %h:%i %p\') AS fecha_fin_contrato'))
                     //DB::raw('TO_CHAR(fecha_fin_contrato,\'dd/mm/yyyy hh24:mi:ss\') AS fecha_fin_contrato'))
            ->get();
        //$clientes = Cliente::select(['idcliente','nombre','apellido','dni','direccion'])->where('estado','=','1')->get();

        return Datatables::of($contratos)->make(true);
    }

    public function create(){
       /* $clientes =  DB::table('cliente')
                    ->select('idcliente',
                             //DB::raw('CONCAT(nombre,apellido) AS nombre_apellido'),
                             DB::raw('CONCAT(tipo_zona,\' \',
                                    nombre_zona,\' \',
                                    tipo_via,\' \',
                                    nombre_via,\' \',
                                    numero_vivienda,\' - \',
                                    distrito,\' - \',
                                    departamento) AS direccion'),
                             'nombre_razon',
                             'dni',
                             'telefono')
                    //MYSQL
                    ->where('estado','=','1')

                    //POSTGRESQL
                    //->where('estado','=','true')
                    ->get();*/
        $clientes =  DB::table('cliente')
            ->select('idcliente',
                //DB::raw('CONCAT(nombre,apellido) AS nombre_apellido'),
                DB::raw('CONCAT(tipo_zona,\' \',
                                    nombre_zona,\' \',
                                    tipo_via,\' \',
                                    nombre_via,\' \',
                                    numero_vivienda,\' - \',
                                    distrito,\' - \',
                                    departamento) AS direccion'),
                'nombre_razon',
                'dni',
                'telefono')
            //MYSQL
            /*->whereNotExists(function($query){
                    $query  ->select('contrato.idcliente')
                            ->from('contrato')
                            ->whereRaw('contrato.idcliente = cliente.idcliente');
            })
            ->orWhereExists(function($query){
                    $query  ->select('contrato.idcliente')
                            ->from('contrato')
                            //->whereRaw('contrato.idcliente=cliente.idcliente AND contrato.estado = \'0\' OR contrato.estado = \'1\'');
                            ->whereRaw('contrato.idcliente=cliente.idcliente AND contrato.estado = \'0\'');
            })*/
            ->where('en_contrato','=','0')
            ->get();

       /* SELECT cliente.idcliente FROM cliente WHERE NOT EXISTS (SELECT contrato.idcliente FROM contrato
                                                                  WHERE contrato.idcliente = cliente.idcliente)
                                            OR NOT EXISTS (SELECT contrato.idcliente FROM contrato
                                                       WHERE contrato.idcliente = cliente.idcliente
        AND contrato.estado = '0' or contrato.estado = '1')*/



        $antenas_emisoras = DB::table('antena_emisora')
                          ->select('idantena_emisora',
                                   'essid',
                                   'mac',
                                   'ip')
                          //MYSQL
                          ->where('estado','=','1')

                          //POSTGRESQL
                          //->where('estado','=','true')
                          ->get();

        $paquetes = DB::table('paquete')
                    ->select('idpaquete',
                             'nombre',
                             'precio_mensual',
                             'megas_descarga_comercial',
                             'megas_subida_comercial')
                    //MYSQL
                    ->where('estado','=','1')

                    //POSTGRESQL
                    //->where('estado','=','true')
                    ->get();


       // var_dump($allClients);
        return view('servicio.contratos.create',['clientes'=>$clientes,'antenas_emisoras'=>$antenas_emisoras,'paquetes'=>$paquetes]);
    }





   /* private  function generarMensualidades($idcontrato, Carbon $start_date, Carbon $end_date)
    {
        $mensualidad = new Mensualidad();

        $dates = [];

        for($date = $start_date; $date->lte($end_date); $date->addMonth()) {
            $mensualidad->idcontrato = $idcontrato;
            $mensualidad->fecha_pago = $date->format('Y-m-d');
            $mensualidad->estado = 'pendiente';
            $mensualidad->color_estado = 'verde';

            $mensualidad->save();
            //$dates[] = $date->format('Y-m-d');
        }

        //return $dates;
    }*/


    public function store(ContratoFormRequest $request){




     //   try {
            $num_cuotas=0;
            $costoPrimerMes = 0;
            $demasMeses = 0;
            $data = $request->all();


            $query= Contrato::where('idcliente','=',$data['cliente'])
                ->where('estado','=','1')
                ->first();
            if($query != null){
                return view("mensajes.msj_rechazado")->with("msj","El cliente ya cuenta con un plan de internet");
            }
        //    DB::beginTransaction();

            $contrato= new Contrato();

            $contrato->idcliente= $data['cliente'];
            $contrato->idpaquete= $data['idpaquete'];

            //OBTENGO EL NUMERO DE MESES
            $inicioContrato = Carbon::createFromFormat('d/m/Y',Carbon::createFromFormat('d/m/Y h:i:s a', $data['fecha_inicio_contrato'])->format('d/m/Y'));
            $finContrato = Carbon::createFromFormat('d/m/Y',Carbon::createFromFormat('d/m/Y h:i:s a', $data['fecha_fin_contrato'])->format('d/m/Y'));
            $num_cuotas = intval($finContrato->diffInMonths($inicioContrato))+1;

         //return $num_cuotas;




            // INAPROPIADA FORMA DE CALCULAR EL NUMERO DE MESES
            //$num_cuotas=(intval($data['duracion_contrato'])*13);


            //$contrato->idantena_emisora= $data['idantena_emisora'];

            $contrato->fecha_inicio_contrato= Carbon::createFromFormat('d/m/Y H:i:s a', $data['fecha_inicio_contrato'])->format('Y-m-d H:i:s');
            $contrato->duracion_contrato= $data['duracion_contrato'];
            $contrato->fecha_fin_contrato=Carbon::createFromFormat('d/m/Y H:i:s a', $data['fecha_fin_contrato'])->format('Y-m-d H:i:s');// Carbon::parse(strtotime($data['fecha_fin_contrato']))->format('Y-m-d');//date('Y-d-m', strtotime($data['fecha_fin_contrato']));
            $contrato->fecha_pago_servicio= $data['fecha_pago_servicio'];
            $contrato->fecha_corte= $data['fecha_corte'];
            $contrato->costo_ap= $data['costo_ap'];
            $contrato->tipo_pago_ap= $data['tipo_pago_ap'];
            $contrato->costo_instalacion= $data['costo_instalacion'];

            $costo_instalacion  = $data['costo_instalacion'];
            $costo_mensualidad  = $data['costo_paquete'];
            $costo_ap           = floatval($data['costo_ap']);

            //$tipo_pago_ap       = $data['tipo_pago_ap'];

            if($data['tipo_pago_ap'] == 'contado')
            {
                $apmensual = 0.00;
                $costoPrimerMes = floatval($costo_instalacion) + floatval($costo_ap) + floatval($costo_mensualidad);
                $demasMeses = $costo_mensualidad;

            }elseif ($data['tipo_pago_ap'] == 'mensual')
            {
                if($data['duracion_contrato'] == '2')
                {
                    $apmensual = round($costo_ap / 24);
                    $costoPrimerMes = floatval($costo_instalacion) + floatval($costo_mensualidad) +$apmensual;
                    $demasMeses =floatval($costo_mensualidad)+$apmensual;
                }else
                {
                    $apmensual = round(floatval($costo_ap)/$num_cuotas + floatval($this->porcentaje(floatval($costo_ap),1.13,1)),1);
                    $costoPrimerMes= floatval($costo_instalacion) + floatval($costo_mensualidad)+ $apmensual;
                    $demasMeses = floatval($costo_mensualidad)+$apmensual;
                }


            }

           /* $contrato->marca_ap= $data['marca_ap'];
            $contrato->modelo_ap= $data['modelo_ap'];
            $contrato->serie_ap= $data['serie_ap'];
            $contrato->marca_router= $data['marca_router'];
            $contrato->modelo_router= $data['modelo_router'];
            $contrato->serie_router= $data['serie_router'];*/
            $contrato->costo_ap_mensualmente = $apmensual;

        //return 'costo_ap: '.$costo_ap.' - numero cuotas: '. $num_cuotas. ' - ap mensual: '. $apmensual;
            $contrato->save();

            $idcontrato = $contrato->idcontrato;


            // ALGORITMO PARA GENERAR LAS MENSUALIDADES UTILIZANDO LA FECHA INICIO Y FECHA FIN
            $cFechaInicio = Carbon::createFromFormat('d/m/Y H:i:s a',$data['fecha_inicio_contrato']);
            $cFechaFin= Carbon::createFromFormat('d/m/Y H:i:s a',$data['fecha_fin_contrato']);

            $cont = 1;

            for($date = $cFechaInicio; $cont <= $date->lte($cFechaFin); $date->addMonthsNoOverflow(1))
            {

                $mensualidad = new Mensualidad();

                $mensualidad->idcontrato = $idcontrato;
                $mensualidad->num_cuota = $cont;

                if($cont == 1)
                {
                    $mensualidad->costo= $costoPrimerMes;
                }else
                {
                    $mensualidad->costo= $demasMeses;
                }

                $mensualidad->fecha_pago = $date->toDateString();
                $mensualidad->estado = 'pendiente';
                $mensualidad->color_estado = 'amarillo';


                $mensualidad->save();

                if($cont == 1)  $idmensualidadprimera = $mensualidad->idmensualidad;

                $cont = $cont +1;
            }

            /*
             * PAGO DE LA PRIMERA MENSUALIDAD
             */

            $pagoMensualidad = new PagoMensualidad();
            $pagoMensualidad->idcliente  = $data['cliente'];
            $pagoMensualidad->fecha_hora_pagado = Carbon::createFromFormat('d/m/Y H:i:s a', $data['fecha_inicio_contrato'])->format('Y-m-d H:i:s');
            $pagoMensualidad->tipo_comprobante = $data['tipo_comprobante'];
            $pagoMensualidad->serie_comprobante = $data['serie_comprobante'];
            $pagoMensualidad->num_comprobante = $data['num_comprobante'];
            $pagoMensualidad->costo_mensualidad= $costoPrimerMes;
            $pagoMensualidad->total_pago= $costoPrimerMes;
            $pagoMensualidad->estado= 'pagado';

            $pagoMensualidad->save();

            $idpagomensualidad = $pagoMensualidad->idpago_mensualidad;


            /*
             *  ACTUALIZACION A PAGADO DE LA PRIMERA MENSUALIDAD
             */

            Mensualidad::where('idcontrato', '=', $idcontrato)
                ->where('idmensualidad','=',$idmensualidadprimera)
                ->where('num_cuota','=','1')
                ->update([
                    'estado' => 'pagado',
                    'color_estado' => 'verde'
                ]);

            /*
             *  DETALLE DEL PRIMER PAGO DE MENSUALIDAD
             */

            $detallePago = new DetallePago();
            $detallePago->idpago_mensualidad = $idpagomensualidad ;
            $detallePago->idmensualidad = $idmensualidadprimera;

            $detallePago->save();


            /*
             * ACTUALIZACION CAMPO CLIENTE PARA QUE FIGURE QUE TIENE UN CONTRATO
             */
            $cliente= Cliente::findOrFail($data['cliente']);
            $cliente->en_contrato = '1';

            $cliente->update();


       //     DB::commit();
            return view("mensajes.msj_correcto")->with("msj","Contrato creado correctamente");


       // }catch (\Exception $e) {
       //     DB::rollback();
        //    return view("mensajes.msj_rechazado")->with("msj","hubo un error vuelva a intentarlo");
      //  }
    }




















    /*SHOW**/
    public function show($id){
        $contrato = DB::table('view_contrato_online')
                    ->select('id',
                            // DB::raw('CONCAT(nombre_cliente,\' \',apellido_cliente) AS nombre_apellido'),
                             'nombre_razon_cliente',
                             'dni',
                             'telefono',
                             DB::raw('CONCAT(tipo_zona,\' \',
                                    nombre_zona,\' \',
                                    tipo_via,\' \',
                                    nombre_via,\' \',
                                    numero_vivienda,\' - \',
                                    distrito,\' - \',
                                    departamento) AS direccion'),
                             'paquete',
                             'precio_mensual',
                             'megas_descarga_comercial',
                             'megas_subida_comercial',
                             'fecha_pago',
                             'fecha_corte',
                             //DB::raw('DATE_FORMAT(fecha_inicio_contrato,"%d/%m/%Y") AS fecha_inicio_contrato'),


                             //MYSQL
                             DB::raw('DATE_FORMAT(fecha_inicio_contrato,\'%d/%m/%Y %h:%i %p\' ) AS fecha_inicio_contrato'),

                             //POSTGRESQL
                             //DB::raw('TO_CHAR(fecha_inicio_contrato,\'dd/mm/yyyy hh24:mi:ss\') AS fecha_inicio_contrato'),
                             'duracion_contrato',


                             //MYSQL
                             DB::raw('DATE_FORMAT(fecha_fin_contrato,\'%d/%m/%Y %h:%i %p\' ) AS fecha_fin_contrato'),

                             //POSTGRESQL
                             //DB::raw('TO_CHAR(fecha_fin_contrato,\'dd/mm/yyyy hh24:mi:ss\') AS fecha_fin_contrato'),

                             /*'antena_essid',
                             'antena_mac',
                             'antena_ip',
                             'marca_ap',
                             'modelo_ap',
                             'serie_ap',
                             'marca_router',
                             'modelo_router',
                             'serie_router',*/
                             'costo_instalacion',
                             'costo_ap',
                             'costo_ap_mensualmente',
                             'tipo_pago_ap',
                             'estado')
                    ->where('id','=',$id)
                    ->first();


        return view("servicio.contratos.show",["contrato"=>$contrato]);
    }


    public function edit($id){
        $contrato = DB::table('view_contrato')
            ->select('id',
                DB::raw('CONCAT(nombre_cliente,\' \',apellido_cliente) AS nombre_apellido'),
                'idcliente',
                'dni',
                'telefono',
                'direccion',
                'idpaquete',
                'paquete',
                'precio_mensual',
                'megas_bajada',
                'megas_subida',
                'fecha_pago',
                'fecha_corte',

                //MYSQL
                DB::raw('DATE_FORMAT(fecha_inicio_contrato,\'%d/%m/%Y %h:%i %p\' ) AS fecha_inicio_contrato'),

                //POSTGRESQL
                //DB::raw('TO_CHAR(fecha_inicio_contrato,\'dd/mm/yyyy hh24:mi:ss\') AS fecha_inicio_contrato'),
                'duracion_contrato',


                //MYSQL
                DB::raw('DATE_FORMAT(fecha_fin_contrato,\'%d/%m/%Y %h:%i %p\' ) AS fecha_fin_contrato'),

                //POSTGRESQL
                //DB::raw('TO_CHAR(fecha_fin_contrato,\' dd/mm/yyyy hh24:mi:ss \') AS fecha_fin_contrato'),

                'idantena_emisora',
                'antena_essid',
                'antena_mac',
                'antena_ip',
                'marca_ap',
                'modelo_ap',
                'serie_ap',
                'marca_router',
                'modelo_router',
                'serie_router',
                'costo_instalacion',
                'costo_ap',
                'tipo_pago_ap',
                'estado')
            ->where('id','=',$id)
            ->first();

        $antenas_emisoras= DB::table('antena_emisora')->select('idantena_emisora','essid')->get();

        return view("servicio.contratos.edit",["contrato"=>$contrato,'antenas_emisoras'=>$antenas_emisoras]);
    }


    public function update(ContratoFormRequest $request){
        $data = $request->all();
        $idcontrato = $data['idcontrato'];

        $contrato= Contrato::findOrFail($idcontrato);
        $contrato->costo_instalacion = $data['costo_instalacion'];
        $contrato->marca_ap = $data['marca_ap'];
        $contrato->modelo_ap = $data['modelo_ap'];
        $contrato->serie_ap = $data['serie_ap'];
        $contrato->marca_router = $data['marca_router'];
        $contrato->modelo_router = $data['modelo_router'];
        $contrato->serie_router = $data['serie_router'];

        if($contrato->update()){
            return view("mensajes.msj_correcto")->with("msj","Contrato editado correctamente");
        }else{
            return view("mensajes.msj_rechazado")->with("msj","huuubo un error vuelva a intentarlo");
        }
    }


    public function delete($id){
        $contrato = DB::table('view_contrato_online')
            ->select('id','idcliente')
            ->where('id','=',$id)
            ->first();

        return view('servicio.contratos.delete',["contrato"=>$contrato]);
    }


    public function destroy($idcliente,$idcontrato){
        //ACTUALIZO TABLA CONTRATO
        $contrato = Contrato::findOrFail($idcontrato);
        $contrato->estado = '0';

        //ACTUALIZO TABLA MENSUALIDADES
        //PARA QUE LAS MENSUALIDADES DEL CONTRATO FIGUREN COMO ANULADAS

        Mensualidad::where('idcontrato', '=', $idcontrato)
            ->update([
                'estado' => 'anulado',
                'color_estado' => 'rojo'
            ]);


        //ACTUALIZO TABLA CLIENTE PARA QUE FIGURE QUE NO TIENE UN CONTRATO
        $cliente = Cliente::findOrFail($idcliente);
        $cliente->en_contrato = '0';
        ;
        if($contrato->update() and $cliente->update())
        {
            return 'afdsaf';
        }else{
            return 'error al eliminar';
        }
    }


    public function generar_pdf($id,$idcliente){
        /*if(!Auth::check()){
            return 'Debe <a href="'.url('/login').'">iniciar sesion</a> para visualizar su contrato';
        }*/
        $paquetes = Paquete::select(['idpaquete','nombre','precio_mensual','megas_bajada'])
            ->where('estado','=','1')
            ->get();

        /*$contrato = ViewContratoOnline::select()
            ->where('idcliente','=',Auth::user()->idcliente)
            ->get();*/

        $contrato = DB::table('view_contrato_online')->select()->where('id','=',$id)->first();

        $email = DB::table('users')->select('email')
            ->where('idcliente','=',$idcliente)
            ->first();


        $inicioContrato = Carbon::createFromFormat('Y-m-d H:i:s',$contrato->fecha_inicio_contrato);
        $finContrato = Carbon::createFromFormat('Y-m-d H:i:s',$contrato->fecha_fin_contrato);
        $numMeses = intval($finContrato->diffInMonths($inicioContrato))+1;


        $html = view('plantillas_pdf.contrato_online',['email'=>$email,'numMeses'=>$numMeses,'contrato'=>$contrato,'planes'=>$paquetes])->render();

        PDF::SetTitle('contrato_'.$contrato->nombre_razon_cliente);
        PDF::AddPage();
        PDF::writeHTML($html, true, false, true, false, '');
        PDF::Output('contrato_'.$contrato->nombre_razon_cliente);



    }


    public function search_cli_contrato($id)
    {
        $clientes_contrato = DB::table('view_contrato')
            ->select('id',
                     'idcliente',
                     'dni',
                     'telefono',
                     'direccion')
            ->where('idcliente','=',$id)
            //MYSQL
            ->where('estado','=','1')

            //POSTGRESQL
            //->where('estado','=',true)
            ->first();
        return response()->json($clientes_contrato);
        //return response()->json(["nombre"=>'cristian']);
    }

    public function pdf()
    {
        $contratos = DB::table('view_contrato_online')
            ->select('id',
                'nombre_razon_cliente',
                'dni',
                //DB::raw('CONCAT(nombre,\' \',apellido) AS nombre_apellido'),
                DB::raw('CONCAT(tipo_zona,\' \',
                                    nombre_zona,\' \',
                                    tipo_via,\' \',
                                    nombre_via,\' \',
                                    numero_vivienda,\' - \',
                                    distrito,\' - \',
                                    departamento) AS direccion'),

                'paquete',
                DB::raw('CONCAT(fecha_pago,\' de cada mes \') AS fecha_pago'),
                DB::raw('CONCAT(fecha_corte,\' de cada mes \') AS fecha_corte'),
                'fecha_inicio_contrato',
                'duracion_contrato',
                'fecha_fin_contrato',
                'costo_instalacion',
                'tipo_pago_ap',
                'costo_ap_mensualmente',
                'estado'
                )
            ->get();

        //$pdf1 = new PDFDOM();

        $pdf = PDFDOM::loadView('plantillas_pdf.listado_contratos', compact('contratos'))->setPaper('a4', 'landscape');

        return $pdf->download('Listado de contratos.pdf');
    }

    public function xls()
    {
        Excel::create('Listado de contratos', function($excel)
        {
            $excel->sheet('Contratos', function($sheet){
                //otra opción -> $products = Product::select('name')->get();
                $contratos = DB::table('view_contrato_online')
                    ->select('id',
                        'nombre_razon_cliente as Cliente',
                        'dni',
                        //DB::raw('CONCAT(nombre,\' \',apellido) AS nombre_apellido'),
                        DB::raw('CONCAT(tipo_zona,\' \',
                                    nombre_zona,\' \',
                                    tipo_via,\' \',
                                    nombre_via,\' \',
                                    numero_vivienda,\' - \',
                                    distrito,\' - \',
                                    departamento) AS Dirección'),

                        'paquete as Plan',
                        DB::raw('CONCAT(fecha_pago,\' de cada mes \') AS fecha_pago'),
                        DB::raw('CONCAT(fecha_corte,\' de cada mes \') AS fecha_corte'),
                        'fecha_inicio_contrato as Inicio/contrato',
                        'duracion_contrato as duración/contrato',
                        'fecha_fin_contrato as fin/contrato',
                        'costo_instalacion as costo/instalación',
                        'tipo_pago_ap as tipo pago/AP',
                        'costo_ap_mensualmente as  \'costo/AP mensualmente\'',
                        'estado'
                    )
                    ->get()
                    ->map(function ($item) {
                        return get_object_vars($item);
                    });
                ;
                $sheet->fromArray($contratos);
                $sheet->setOrientation('landscape');
            });
        })->export('xls');

    }
}