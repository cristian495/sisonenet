<?php

namespace sisonenet\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use sisonenet\Paquete;


class AdquirirPlanController extends Controller
{



    public function index($nombre_plan){
       $plan =  DB::table('paquete')
            ->select('idpaquete','nombre','megas_subida','megas_bajada','precio_mensual')
            ->where('nombre','=',$nombre_plan)
            ->first();



        if(count($plan)!=0){
            session(['plan' => $plan]);
            session(['costo_ap' => '300']);
            session(['costo_instalacion' => '1']);

           // session()->push('plan.costo_ap','300');
            return view('webPage.adquirir_plan.index',['plan'=>$plan]);
        }else{
            return view('webPage.adquirir_plan.no_found');
        }
    }
}
