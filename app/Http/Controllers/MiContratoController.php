<?php

namespace sisonenet\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use PDF;
use Illuminate\Support\Facades\Auth;
use sisonenet\Paquete;
use Carbon\Carbon;

class MiContratoController extends Controller
{
    public function generatePdf()
    {
        if(!Auth::check()){
            return 'Debe <a href="'.url('/login').'">iniciar sesion</a> para visualizar su contrato';
        }
        $paquetes = Paquete::select(['idpaquete','nombre','precio_mensual','megas_bajada'])
            ->where('estado','=','1')
            ->get();

        /*$contrato = ViewContratoOnline::select()
            ->where('idcliente','=',Auth::user()->idcliente)
            ->get();*/

        $contrato = DB::table('view_contrato_online')->select()
            ->where('idcliente','=',Auth::user()->idcliente)
            ->where('estado','=','1')
            ->first();

        if(count($contrato) < 1)
        {
            return "No cuenta con ningun contrato hasta la fecha";
        }
        $email = DB::table('users')->select('email')
            ->where('idcliente','=',Auth::user()->idcliente)
            ->first();

        //return $contrato->fecha_inicio_contrato;
        //'2017-02-02 02:02:03'
        $inicioContrato = Carbon::createFromFormat('Y-m-d H:i:s',$contrato->fecha_inicio_contrato);
        //return  $inicioContrato;
        $finContrato = Carbon::createFromFormat('Y-m-d H:i:s',$contrato->fecha_fin_contrato);
        $numMeses = intval($finContrato->diffInMonths($inicioContrato))+1;


        //$direccion_cliente = $this->getDireccionCompletaCliente();

        $var = [
            'cliente_nombre' => session('cliente_plan.cliente.nombre'),
            'cliente_apellido' => session('cliente_plan.cliente.apellido'),
            'cliente_dni' => session('cliente_plan.cliente.dni_ruc'),
            'cliente_email' => session('cliente_plan.cliente.email'),
            'tipo_via' => session('cliente_plan.cliente.tipo_via'),
            'nombre_via' => session('cliente_plan.cliente.nombre_via'),
            'numero_via' => session('cliente_plan.cliente.numero_via'),
            'numero_vivienda' => session('cliente_plan.cliente.numero_vivienda'),
            'tipo_zona' => session('cliente_plan.cliente.tipo_zona'),
            'nombre_zona' => session('cliente_plan.cliente.nombre_zona'),
            'departamento' => session('cliente_plan.cliente.departamento'),
            'provincia' => session('cliente_plan.cliente.provincia'),
            'distrito' => session('cliente_plan.cliente.distrito'),
            'referencia' => session('cliente_plan.cliente.referencia'),
            'cliente_telefono' => session('cliente_plan.cliente.telefono'),
            'cliente_telefono_adicional' => session('cliente_plan.cliente.telefono_adicional'),
            'anios_contrato' => session('cliente_plan.plan.duracion_del_contrato'),
            'costo_cuotas_ap' => session('cliente_plan.plan.costo_cuotas_ap'),
            'fecha_de_pago' => session('cliente_plan.plan.fecha_de_pago'),
            'id_plan' => session('cliente_plan.plan.id_plan')
        ];


        $html = view('plantillas_pdf.contrato_online',['email'=>$email,'numMeses'=>$numMeses,'contrato'=>$contrato,'planes'=>$paquetes])->render();

        PDF::SetTitle('contrato_'.$contrato->nombre_razon_cliente);
        PDF::AddPage();
        PDF::writeHTML($html, true, false, true, false, '');
        PDF::Output('contrato_'.$contrato->nombre_razon_cliente);



    }
}
