<?php

namespace sisonenet\Http\Controllers;

use sisonenet\DetallePago;
use sisonenet\Mensualidad;
use sisonenet\PagoMensualidad;

use sisonenet\Http\Requests\ComprobanteFormRequest;

use Yajra\Datatables\Facades\Datatables;
use Carbon\Carbon;
use DB;
use Barryvdh\DomPDF\Facade as PDFDOM;
use Excel;

//use Illuminate\Support\Facades\Input;

class ComprobantesController extends Controller
{

    public function index(){
        $contratos_cliente = DB::table('view_contrato_online')
            ->select('id','nombre_razon_cliente')
            ->get();
        return view('servicio.cobros.comprobantes.index',['contratos_cliente'=>$contratos_cliente]);
    }

    public function getComprobantes($idForSearch)
    {
        if($idForSearch != 0){
            $comprobantes = DB::table('view_pagos')
                ->select('idpago_mensualidad',
                    //DB::raw('CONCAT(nombre,\' \',apellido) AS nombre_apellido'),
                    'nombre_razon',
                    'tipo_comprobante',
                    'num_comprobante',
                    DB::raw('SUM(costo) AS total'),

                    //MYSQL
                    DB::raw('DATE_FORMAT(fecha_hora_pagado,\'%d/%m/%Y %h:%i %p\' ) AS fecha_hora_pagado')

                //POSTGRESQL
                //DB::raw('TO_CHAR(fecha_hora_pagado,\'dd/mm/yyyy hh24:mi:ss\') AS fecha_hora_pagado')
                )

                ->where('idcontrato','=', $idForSearch)

                ->groupBy('idpago_mensualidad','nombre_razon','tipo_comprobante','num_comprobante','fecha_hora_pagado')
                ->get();
        }else{
            $comprobantes = DB::table('view_pagos')
                ->select('idpago_mensualidad',
                    //DB::raw('CONCAT(nombre,\' \',apellido) AS nombre_apellido'),
                    'nombre_razon',
                    'tipo_comprobante',
                    'num_comprobante',
                    DB::raw('SUM(costo) AS total'),

                    //MYSQL
                    DB::raw('DATE_FORMAT(fecha_hora_pagado,\'%d/%m/%Y %h:%i %p\' ) AS fecha_hora_pagado')

                //POSTGRESQL
                //DB::raw('TO_CHAR(fecha_hora_pagado,\'dd/mm/yyyy hh24:mi:ss\') AS fecha_hora_pagado')
                )

                ->groupBy('idpago_mensualidad','nombre_razon','tipo_comprobante','num_comprobante','fecha_hora_pagado')
                ->get();
        }


        return Datatables::of($comprobantes)->make(true);
    }

    /*MUESTRA TODOS LOS CLIENTES ACTIVOS Y SUS CONTRATOS TAMBIEN ACTIVOS PARA EFECTUAR EL PAGO
    DE ALGUNA MENSUALIDAD*/
    public function create(){
        $clientes =  DB::table('view_contrato_online')
            ->select('id',
                'idcliente',
                //DB::raw('CONCAT(nombre_cliente,\' \',apellido_cliente) AS nombre_apellido'))
                'nombre_razon_cliente')
            //MYSQL
            ->where('estado','=','1')

            //POSTGRESQL
            //->where('estado','=',true)

            //MYSQL
            ->where('estado_cliente','=','1')

            //POSTGRESQL
            //->where('estado_cliente','=',true)
            ->get();
        return view('servicio.cobros.comprobantes.create',['clientes'=>$clientes]);
    }

    public function store(ComprobanteFormRequest $request){
        try {
            $data = $request->all();

            DB::beginTransaction();
            /*REGISTRANDO EL PAGO*/
            $pago_mensualidad = new PagoMensualidad();
            $pago_mensualidad->idcliente = $data['cliente'];
            $pago_mensualidad->fecha_hora_pagado=$carbon = new Carbon();
            $pago_mensualidad->tipo_comprobante=$data['tipo_comprobante'];
            $pago_mensualidad->serie_comprobante=$data['serie_comprobante'];
            $pago_mensualidad->num_comprobante=$data['num_comprobante'];
            $pago_mensualidad->mora='0.00';
            $pago_mensualidad->costo_mensualidad='0.00';
            $pago_mensualidad->total_pago = $data['total'];
            $pago_mensualidad->estado='pagado';


            $pago_mensualidad->save();

            $idpago_mensualidad = $pago_mensualidad->idpago_mensualidad;

            for($i=0; $i<count($data['mensualidades']); $i++){


                /*ACTUALIZAR MENSUALIDADES COMO PAGADAS*/
                Mensualidad::where('idmensualidad','=',$data['mensualidades'][$i])
                    ->update([
                        'estado' => 'pagado',
                        'color_estado' => 'verde'
                    ]);


                /*REGISTRO DEL DETALLE DEL PAGO*/

                $detalle_pago = new DetallePago();
                $detalle_pago->idpago_mensualidad = $idpago_mensualidad;
                $detalle_pago->idmensualidad = $data['mensualidades'][$i];
                $detalle_pago->save();

            }

            DB::commit();
            return view("mensajes.msj_correcto")->with("msj","Comprobante emitido correctamente.");
        }catch (\Exception $e) {
            return view("mensajes.msj_rechazado")->with("msj","Error al general el comprobante.");
        }

        //return $data['mensualidades'][0];
    }

    public function show($id){

        $pago = DB::table('pago_mensualidad as pm')
            ->join('cliente as c','pm.idcliente','=','c.idcliente')
            //->join('detalle_pago as dp','pm.idpago_mensualidad')
            ->select('pm.idpago_mensualidad',

                //MYSQL
                DB::raw('DATE_FORMAT(pm.fecha_hora_pagado,\'%d/%m/%Y %h:%i %p\' ) AS fecha_hora_pagado'),

                //POSTGRESQL
                //DB::raw('TO_CHAR(pm.fecha_hora_pagado,\'dd/mm/yyyy hh12:mi:ss\') AS fecha_hora_pagado'),

                //DB::raw('CONCAT(c.nombre,\' \',c.apellido) AS nombre_apellido'),
                'c.nombre_razon',
                'c.dni',
                'c.telefono',
                DB::raw('CONCAT(\'VIA: \',c.tipo_via,\' NOMBRE VIA: \',c.nombre_via,\' NUMERO VIVIENDA: \',c.numero_vivienda,\' TIPO ZONA: \',c.tipo_zona,\' NOMBRE ZONA: \',c.nombre_zona) AS direccion'),
                'pm.tipo_comprobante',
                'pm.serie_comprobante',
                'pm.num_comprobante',
                'pm.total_pago')
            ->where('idpago_mensualidad','=',$id)
            ->first();

      /*  $detalles = DB::table('detalle_venta as d')
            ->join('articulo as a', 'd.idarticulo', '=', 'a.idarticulo')
            ->select('a.nombre as articulo', 'd.cantidad', 'd.descuento','d.precio_venta')
            ->where('d.idventa', '=', $id)
            ->get();*/

        $detalle_pago =DB::table('detalle_pago as d')
            ->join('mensualidad as m', 'd.idmensualidad','=','m.idmensualidad')
            ->select('m.num_cuota',
                'm.costo',

                //MYSQL
                DB::raw('DATE_FORMAT(m.fecha_pago,\'%d/%m/%Y \' ) AS fecha_pago'),

                //POSTGRESQL
                //DB::raw('TO_CHAR(m.fecha_pago,\'dd/mm/yyyy\') AS fecha_pago'),


                DB::raw('SUM(m.costo) AS total')
                )
            ->where('d.idpago_mensualidad','=',$id)
            ->groupBy('m.num_cuota','m.costo','fecha_pago')
            ->get();


        return view("servicio.cobros.comprobantes.show",["pago"=>$pago,"detalles"=>$detalle_pago]);
    }

    public function comprobantes_cliente_pdf($idcontrato)
    {
        $comprobantes = DB::table('view_pagos')
            ->select(
                'idcontrato',
                'idpago_mensualidad',
                //DB::raw('CONCAT(nombre,\' \',apellido) AS nombre_apellido'),
                'nombre_razon',
                'tipo_comprobante',
                'num_comprobante',
                DB::raw('SUM(costo) AS total'),

                //MYSQL
                DB::raw('DATE_FORMAT(fecha_hora_pagado,\'%d/%m/%Y %h:%i %p\' ) AS fecha_hora_pagado')

            //POSTGRESQL
            //DB::raw('TO_CHAR(fecha_hora_pagado,\'dd/mm/yyyy hh24:mi:ss\') AS fecha_hora_pagado')
            )

            ->where('idcontrato','=', $idcontrato)

            ->groupBy('idpago_mensualidad','nombre_razon','tipo_comprobante','num_comprobante','fecha_hora_pagado')
            ->get();


        $pdf = PDFDOM::loadView('plantillas_pdf.comprobantes.comprobantes_cliente', compact('comprobantes'));

        return $pdf->download('Comprobantes - '.$comprobantes[0]->nombre_razon.'.pdf');
    }

    public function comprobantes_cliente_xls($idcontrato)
    {
        Excel::create('Listado de comprobantes', function($excel) use($idcontrato)
        {
            $excel->sheet('mensualidades', function($sheet) use ($idcontrato){
                //otra opción -> $products = Product::select('name')->get();
                $comprobantes = DB::table('view_pagos')
                    ->select(
                        'idcontrato as Num/contrato',
                        'idpago_mensualidad as Num/pago',
                        'nombre_razon as Cliente',
                        'tipo_comprobante as Tipo/Comprobante',
                        'num_comprobante as Num/Comprobante',
                        DB::raw('SUM(costo) AS Total'),

                        DB::raw('DATE_FORMAT(fecha_hora_pagado,\'%d/%m/%Y %h:%i %p\' ) AS \'Fecha emitido\'')


                    )

                    ->where('idcontrato','=', $idcontrato)

                    ->groupBy('idpago_mensualidad','nombre_razon','tipo_comprobante','num_comprobante','fecha_hora_pagado')
                    ->get()
                    ->map(function ($item) {
                        return get_object_vars($item);
                    });

                $sheet->fromArray($comprobantes);
                $sheet->setOrientation('landscape');
            });
        })->export('xls');

    }


    public function pdf_comprobante($idpagomensualidad)
    {
        $pago = DB::table('pago_mensualidad as pm')
            ->join('cliente as c','pm.idcliente','=','c.idcliente')
            //->join('detalle_pago as dp','pm.idpago_mensualidad')
            ->select('pm.idpago_mensualidad',

                //MYSQL
                DB::raw('DATE_FORMAT(pm.fecha_hora_pagado,\'%d/%m/%Y %h:%i %p\' ) AS fecha_hora_pagado'),

                //POSTGRESQL
                //DB::raw('TO_CHAR(pm.fecha_hora_pagado,\'dd/mm/yyyy hh12:mi:ss\') AS fecha_hora_pagado'),

                //DB::raw('CONCAT(c.nombre,\' \',c.apellido) AS nombre_apellido'),
                'c.nombre_razon',
                'c.dni',
                'c.telefono',
                DB::raw('CONCAT(\'VIA: \',c.tipo_via,\' NOMBRE VIA: \',c.nombre_via,\' NUMERO VIVIENDA: \',c.numero_vivienda,\' TIPO ZONA: \',c.tipo_zona,\' NOMBRE ZONA: \',c.nombre_zona) AS direccion'),
                'pm.tipo_comprobante',
                'pm.serie_comprobante',
                'pm.num_comprobante',
                'pm.total_pago')
            ->where('idpago_mensualidad','=',$idpagomensualidad)
            ->first();

        $detalle_pago =DB::table('detalle_pago as d')
            ->join('mensualidad as m', 'd.idmensualidad','=','m.idmensualidad')
            ->select('m.num_cuota',
                'm.costo',

                //MYSQL
                DB::raw('DATE_FORMAT(m.fecha_pago,\'%d/%m/%Y \' ) AS fecha_pago'),

                //POSTGRESQL
                //DB::raw('TO_CHAR(m.fecha_pago,\'dd/mm/yyyy\') AS fecha_pago'),


                DB::raw('SUM(m.costo) AS total')
            )
            ->where('d.idpago_mensualidad','=',$idpagomensualidad)
            ->groupBy('m.num_cuota','m.costo','fecha_pago')
            ->get();


        $pdf = PDFDOM::loadView('plantillas_pdf.comprobantes.pdf_comprobante', compact('pago','detalle_pago'));

        return $pdf->download('Comprobante N° '.$pago->num_comprobante.' - '.$pago->nombre_razon.'.pdf');
    }

}
