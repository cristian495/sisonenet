<?php

namespace sisonenet\Http\Controllers;


use sisonenet\Http\Requests;
use sisonenet\Http\Requests\PagoMensualidadFormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Input;
use Yajra\Datatables\Facades\Datatables;
use Carbon\Carbon;
use DB;
use Barryvdh\DomPDF\Facade as PDFDOM;
use Excel;
use sisonenet\PagoMensualidad;
use sisonenet\Mensualidad;
use sisonenet\DetallePago;


class PendientesController extends Controller
{
    public function index()
    {
        $contratos_cliente = DB::table('view_contrato_online')
                            ->select('id','nombre_razon_cliente')
                            ->get();
        return view('servicio.cobros.por_cobrar.index',['contratos_cliente'=>$contratos_cliente]);
    }

    public function getPorCobrar($idcontrato_cliente){
        $cobrosPend= DB::table('view_cobros_pendientes')
            ->select('idcontrato',
                'idmensualidad',
                'num_cuota',
                //DB::raw('CONCAT(nombre,\' \',apellido) AS nombre_apellido'),
                'idcliente',
                'nombre_razon',

                //MYSQL
                DB::raw('DATE_FORMAT(fecha_pago,\'%d/%m/%Y\' ) AS fecha_pago'),

                //POSTGRESQL
                //DB::raw('TO_CHAR(fecha_pago,\'dd/mm/yyyy\' ) AS fecha_pago'),


                'costo',
                'estado',
                'color_estado')
            ->where('idcontrato','=',$idcontrato_cliente)
            ->get();
        //$clientes = Cliente::select(['idcliente','nombre','apellido','dni','direccion'])->where('estado','=','1')->get();

        return Datatables::of($cobrosPend)->make(true);
    }


    public function search_men_cliente($id,$idcont)
    {
        $mensualidades = DB::table('view_cobros_pendientes')
            ->select('idmensualidad',
                     'num_cuota',

                     //MYSQL
                     DB::raw('DATE_FORMAT(fecha_pago,\'%d/%m/%Y\' ) AS fecha_pago'),


                     //POSTGRESQL
                     //DB::raw('TO_CHAR(fecha_pago,\'dd/mm/yyyy\' ) AS fecha_pago'),


                     'costo')
            ->where('idcliente','=',$id)
            ->where('idcontrato','=',$idcont)
            ->where('estado','=','pendiente')
            ->get();

        if(count($mensualidades)>0){
            return response()->json([$mensualidades]);
        }else{
            return response()->json('No existen mensualidades por pagar',500);
        }
        //return response()->json(["nombre"=>'cristian']);
    }


    public function pdf($idcontrato)
    {
        $mensualidades = DB::table('view_cobros_pendientes')
            ->select('idmensualidad',
                     'idcontrato',
                    'num_cuota',
                    'nombre_razon',
                    DB::raw('DATE_FORMAT(fecha_pago,\'%d/%m/%Y\' ) AS fecha_pago'),

                    'costo',
                    'estado')
            ->where('idcontrato','=',$idcontrato)
            ->get();


        $pdf = PDFDOM::loadView('plantillas_pdf.mensualidades_cliente', compact('mensualidades','idcontrato'));

        return $pdf->download('Mensualidades - '.$mensualidades[0]->nombre_razon.'.pdf');
    }

    public function xls($idcontrato)
    {
        Excel::create('Listado de mensualidades', function($excel) use($idcontrato)
        {
            $excel->sheet('mensualidades', function($sheet) use ($idcontrato){
                //otra opción -> $products = Product::select('name')->get();
                $mensualidades = DB::table('view_cobros_pendientes')
                    ->select('idmensualidad as ID',
                        'num_cuota as Cuota',
                        //DB::raw('CONCAT(nombre,\' \',apellido) AS nombre_apellido'),
                        'nombre_razon as Cliente',

                        //MYSQL
                        DB::raw('DATE_FORMAT(fecha_pago,\'%d/%m/%Y\' ) as \'Fecha de pago\''),

                        //POSTGRESQL
                        //DB::raw('TO_CHAR(fecha_pago,\'dd/mm/yyyy\' ) as fecha_pago'),


                        'costo',
                        'estado')
                    ->where('idcontrato','=',$idcontrato)
                    ->get()
                    ->map(function ($item) {
                    return get_object_vars($item);
                });
;
                $sheet->fromArray($mensualidades);
                $sheet->setOrientation('landscape');
            });
        })->export('xls');

    }

    public function create_pago($idcliente,$idmensualidad,$idcontrato){
        $cliente =  DB::table('view_contrato_online')
            ->select('id',
                'nombre_razon_cliente',
                'idcliente',
                'dni',
                DB::raw('CONCAT(\'VIA: \',tipo_via,\' NOMBRE VIA: \',nombre_via,\' NUMERO VIVIENDA: \',numero_vivienda,\' TIPO ZONA: \',tipo_zona,\' NOMBRE ZONA: \',nombre_zona) AS direccion'),
                //DB::raw('CONCAT(nombre_cliente,\' \',apellido_cliente) AS nombre_apellido'))
                'telefono'
                )
            //MYSQL
            ->where('estado_cliente','=','1')

            //POSTGRESQL
            //->where('estado','=',true)

            //MYSQL
            ->where('idcliente','=',$idcliente)
            ->where('id','=',$idcontrato)

            //POSTGRESQL
            //->where('estado_cliente','=',true)
            ->first();

        $mensualidad = DB::table('mensualidad')
                ->select(
                         'idmensualidad',
                         'num_cuota',
                         'costo',
                         DB::raw('DATE_FORMAT(fecha_pago,\'%d/%m/%Y\' ) AS fecha_pago'),
                         'estado')
                ->where('idmensualidad','=',$idmensualidad)
                ->first();
        return view('servicio.cobros.por_cobrar.create_pago',['cliente'=>$cliente,'mensualidad'=>$mensualidad]);
    }

    public function store_pago(PagoMensualidadFormRequest $request){
        //try {
            $data = $request->all();

          //  DB::beginTransaction();
            /*REGISTRANDO EL PAGO*/
            $pago_mensualidad = new PagoMensualidad();
            $pago_mensualidad->idcliente = $data['idcliente'];
            $pago_mensualidad->fecha_hora_pagado=$carbon = new Carbon();
            $pago_mensualidad->tipo_comprobante=$data['tipo_comprobante'];
            $pago_mensualidad->serie_comprobante=$data['serie_comprobante'];
            $pago_mensualidad->num_comprobante=$data['num_comprobante'];
            $pago_mensualidad->mora='0.00';
            $pago_mensualidad->costo_mensualidad='0.00';
            $pago_mensualidad->total_pago = $data['costo_total'];
            $pago_mensualidad->estado='pagado';


            $pago_mensualidad->save();

            $idpago_mensualidad = $pago_mensualidad->idpago_mensualidad;

            //for($i=0; $i<count($data['mensualidades']); $i++){


                /*ACTUALIZAR MENSUALIDADES COMO PAGADAS*/
                //Mensualidad::where('idmensualidad','=',$data['mensualidades'][$i])
                Mensualidad::where('idmensualidad','=',$data['idmensualidad'])
                    ->update([
                        'estado' => 'pagado',
                        'color_estado' => 'verde'
                    ]);


                /*REGISTRO DEL DETALLE DEL PAGO*/

                $detalle_pago = new DetallePago();
                $detalle_pago->idpago_mensualidad = $idpago_mensualidad;
                $detalle_pago->idmensualidad = $data['idmensualidad'];
                $detalle_pago->save();

           // }

         //   DB::commit();
            if($detalle_pago->save())
            {
            return view("mensajes.msj_correcto")->with("msj","Pago realizado correctamente.");
            }else
        //}catch (\Exception $e) {
            { return view("mensajes.msj_rechazado")->with("msj","Error al efectuar el pago.");}
        //}

        //return $data['mensualidades'][0];
    }

    public function show($idmensualidad){

        $mensualidad = DB::table('view_cobros_pendientes')
            ->select(
                     'idcontrato',
                     'idmensualidad',
                     'nombre_razon',
                     'num_cuota',
                     'fecha_pago',
                     'costo',
                     'estado'
                     )
            ->where('idmensualidad','=',$idmensualidad)
            ->first();


        $date_obj_corte = date_create( date('Y-m-d',strtotime($mensualidad->fecha_pago. ' + 1 days')));
        $date_obj_pago = date_create( $mensualidad->fecha_pago);

        $corte =  date_format($date_obj_corte,'d/m/Y');
        $pago =  date_format($date_obj_pago,'d/m/Y');
        return view("servicio.cobros.por_cobrar.show",["mensualidad"=>$mensualidad,"fecha_corte"=>$corte,"fecha_pago"=>$pago]);
    }
}
