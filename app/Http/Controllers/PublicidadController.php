<?php

namespace sisonenet\Http\Controllers;

use Illuminate\Http\Request;
use sisonenet\Http\Requests\ImagenPequeFormRequest;
use sisonenet\Publicidad;
use View;
use Illuminate\Support\Facades\Input;
use Storage;
use DB;
use Illuminate\Support\Facades\Response;

class PublicidadController extends Controller
{
    public function publicidadUno(){
        $img01 = DB::table('publicidad')
            ->select('id',
                     'ruta_imagen',
                     'titulo')
            ->where('tipo_imagen','=','peque01')
            ->first();
        return view('publicidad.uno.index',['img'=>$img01]);
    }

    public function subirPublicidadUno(ImagenPequeFormRequest $request){

        //optengo el id de la imagen antigua
        $idimgold = $request->input('id_img_old');

        // optengo la nueva imagen que quiero subir
        $imagen = $request->file('imagen');

        //Optengo la extencion de la imagen que quiero subir
        $extension = $imagen->getClientOriginalExtension();

        // creo un nuevo nombre para la imagen
        $nuevoNombre = 'publicidad01'.$idimgold.'.'.$extension;


        // Hago la subida de la imagen y guardo el resultado en una variable
        $r1 = Storage::disk('publicidad')->put($nuevoNombre,\File::get($imagen));

        // Ruta de la nueva imagen
        $rutadelaimagen="img/paginaweb/publi/".$nuevoNombre;

        // Comparo si la subida de imagen fue exitosa
        if($r1)
        {

            // Busco en la base de datos la imagen antigua que va a ser modificado
            $publicidad = Publicidad::where('id',$request->input('id_img_old'))
                            ->where('tipo_imagen','peque01')
                            ->firstOrFail();

            // Insercion de la nueva data en la base de datos
            $publicidad->ruta_imagen =$rutadelaimagen;
            $publicidad->tipo_imagen = 'peque01';
            $publicidad->titulo = $request->input('titulo_imagen');

            // Realizo la actualizacion
            $publicidad->update();

            // Obtengo el id de la registro de la base de datos que se actualizó
            $lastImg = $publicidad->id;

            // Utilizo ese id para optener la nueva informacion
            // de la imagen que esta guardada en la base de datos
            $nameNewImage = Publicidad::where('id',$lastImg)
                            ->select('ruta_imagen','titulo')
                            ->firstOrFail();

            // Vista que sera mostrada en el frontend
            $html = view::make('mensajes.msj_correcto',["msj"=>"Imagen actualizada correctamente"])->render();

            // Envio de la respuesta al frontend
            return Response::json(['newImg'=>$nameNewImage,'view'=>$html],201);
        }
        else
        {
            // Si la imagen no es subida mostrara un mensaje de error
            return Response::json('error al subir la imagen',404);
        }

    }

    public function publicidadDos(){
        $img02 = DB::table('publicidad')
            ->select('id',
                'ruta_imagen',
                'titulo')
            ->where('tipo_imagen','=','peque02')
            ->first();
        return view('publicidad.dos.index',['img'=>$img02]);
    }

    public function subirPublicidadDos(ImagenPequeFormRequest $request){
        //optengo el id de la imagen antigua
        $idimgold = $request->input('id_img_old');

        // optengo la nueva imagen que quiero subir
        $imagen = $request->file('imagen');

        //Optengo la extencion de la imagen que quiero subir
        $extension = $imagen->getClientOriginalExtension();

        // creo un nuevo nombre para la imagen
        $nuevoNombre = 'publicidad02'.$idimgold.'.'.$extension;


        // Hago la subida de la imagen y guardo el resultado en una variable
        $r1 = Storage::disk('publicidad')->put($nuevoNombre,\File::get($imagen));

        // Ruta de la nueva imagen
        $rutadelaimagen="img/paginaweb/publi/".$nuevoNombre;

        // Comparo si la subida de imagen fue exitosa
        if($r1)
        {

            // Busco en la base de datos la imagen antigua que va a ser modificado
            $publicidad = Publicidad::where('id',$request->input('id_img_old'))
                ->where('tipo_imagen','peque02')
                ->firstOrFail();

            // Insercion de la nueva data en la base de datos
            $publicidad->ruta_imagen =$rutadelaimagen;
            $publicidad->tipo_imagen = 'peque02';
            $publicidad->titulo = $request->input('titulo_imagen');

            // Realizo la actualizacion
            $publicidad->update();

            // Obtengo el id de la registro de la base de datos que se actualizó
            $lastImg = $publicidad->id;

            // Utilizo ese id para optener la nueva informacion
            // de la imagen que esta guardada en la base de datos
            $nameNewImage = Publicidad::where('id',$lastImg)
                ->select('ruta_imagen','titulo')
                ->firstOrFail();

            // Vista que sera mostrada en el frontend
            $html = view::make('mensajes.msj_correcto',["msj"=>"Imagen actualizada correctamente"])->render();

            // Envio de la respuesta al frontend
            return Response::json(['newImg'=>$nameNewImage,'view'=>$html],201);
        }
        else
        {
            // Si la imagen no es subida mostrara un mensaje de error
            return Response::json('error al subir la imagen',404);
        }

    }

}
