<?php

namespace sisonenet;

use Illuminate\Database\Eloquent\Model;

class Publicidad extends Model
{
    protected $table='publicidad';
    protected $primaryKey='id';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'ruta_imagen','tipo_imagen','titulo'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];
}
