<?php

namespace sisonenet;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $table='cliente';
    protected $primaryKey='idcliente';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'tipo_persona',
        'nombre_razon',
        //'apellido',
        'dni',
        'telefono',
        'telefono_adicional',
        'tipo_via',
        'nombre_via',
        'numero_vivienda',
        'tipo_zona',
        'nombre_zona',
        'imagen_satelital',
        'referencia_direccion',
        'estado',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];
}
