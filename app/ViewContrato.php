<?php

namespace sisonenet;

use Illuminate\Database\Eloquent\Model;

class ViewContrato extends Model
{
    protected $table='view_contrato';
    protected $primaryKey='id';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'nombre_cliente',
        'nombre_razon',
        'dni',
        'telefono',
        'telefono_adicional',
        'tipo_via',
        'nombre_via',
        'numero_vivienda',
        'tipo_zona',
        'nombre_zona',
        'departamento',
        'provincia',
        'distrito',
        'referencia_direccion',
        'estado_cliente',
        'idpaquete',
        'paquete',
        'precio_mensual',
        'megas_bajada',
        'megas_subida',
        'megas_descarga_comercial',
        'megas_subida_comercial',
        'fecha_pago',
        'fecha_corte',
        'fecha_inicio_contrato',
        'duracion_contrato',
        'fecha_fin_contrato',
        'costo_instalacion',
        'costo_ap',
        'costo_ap_mensualmente',
        'tipo_pago_ap',
        'estado',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];
}
