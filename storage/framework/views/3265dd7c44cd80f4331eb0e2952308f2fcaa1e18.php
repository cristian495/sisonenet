<?php $__env->startSection('htmlheader_title'); ?>

<?php if(Auth::user()->tipo_usuario == 1): ?>
    <?php echo e("Sistema de pagos y cobros"); ?>

<?php elseif(Auth::user()->tipo_usuario == 0 ): ?>
<?php echo e("Bienvenido " . ucwords(session('cliente_session')->nombre_razon )); ?>


<?php endif; ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('contentheader_description'); ?>
<?php echo e(""); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('main-content'); ?>
<div class="row">
    <div class="col-sm-6 col-xs-12">
        <div class="info-box">
            <!-- Apply any bg-* class to to the icon to color it -->
            <span class="info-box-icon bg-aqua"><i class="fa  fa-users"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">Clientes</span>
                <span class="info-box-number"><?php echo e($num_clientes); ?></span>
            </div>
            <!-- /.info-box-content -->
        </div>
    </div>
    <div class="col-sm-6 col-xs-12">
        <div class="info-box">
            <!-- Apply any bg-* class to to the icon to color it -->
            <span class="info-box-icon bg-green"><i class="fa  fa-file-text"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">Contratos activos</span>
                <span class="info-box-number"><?php echo e($num_contratos); ?></span>
            </div>
            <!-- /.info-box-content -->
        </div>
    </div>
</div>
<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title">Planes de internet</h3>


    </div>
    <div class="box-body" style="position: relative;width: 320px; margin:auto">
        <canvas id="myChart" width="50" height="50"></canvas>
    </div>
    <!-- /.box-body -->
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('adminlte::layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>