<span class="fa fa-close cerrar cerrar_modal"></span>

<div class="row">
    <div class="col-lg-12 col-md-6 col-sm-6 col-xs-12">
        <h3>Cliente: <b>{{$cliente->nombre_razon}}</b></h3>
    </div>
</div>

<fieldset class="scheduler-border">
    <legend class="scheduler-border">Datos Personales</legend>
    <input type="hidden" name="idcliente" value="{{ $cliente->idcliente}}"/>
    <div class="row">
            <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12" >
                <div class="form-group">
                    <label for="nombre">Nombre y Apellidos / Razon Social</label>
                    <input type="text" readonly value="{{ $cliente->nombre_razon}}" name="nombre_razon" class="form-control" placeholder="Nombre o Razón Social.."/>
                </div>
            </div>
            <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12" >
                <div class="form-group">
                    <label for="dni">DNI / RUC</label>
                    <input type="number" readonly value="{{ $cliente->dni}}" name="dni_ruc" class="form-control" placeholder="DNI o RUC..."/>
                </div>
            </div>
            <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12" >
                <div class="form-group">
                    <label for="precio">Teléfono</label>
                    <input type="tel"  readonly value="{{ $cliente->telefono}}" name="telefono" class="form-control" placeholder="Teléfono..."/>
                </div>
            </div>
            <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12" >
                <div class="form-group">
                    <label for="precio">Teléfono adicional</label>
                    <input type="tel"  readonly value="{{ $cliente->telefono_adicional}}" name="telefono_adicional" class="form-control" placeholder="Teléfono..."/>
                </div>
            </div>
    </div>
</fieldset>

<fieldset class="scheduler-border">
    <legend class="scheduler-border">Dirección</legend>
    <div class="row">
        <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
            <div class="form-group ">
                <label class="control-label">Tipo de Vía<span class="text-danger">&nbsp;(*)</span></label>
                <input type="tel"  readonly value="{{ $cliente->tipo_via}}"  class="form-control"/>
            </div>
        </div>
        <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
            <div class="form-group delpmbottom">
                <label class="control-label">Nombre de Vía (Dirección)<span class="text-danger">&nbsp;(*)</span></label>
                <input readonly value="{{$cliente->nombre_via}}" type="text"  class="form-control">
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-4 col-sm-6 col-md-6 col-xs-12">
            <div class="form-group ">
                <label class="control-label">NRO. Vivienda<span class="text-danger">&nbsp;(*)</span> </label>
                <input readonly type="text" value="{{$cliente->numero_vivienda}}"  class="form-control" >
            </div>
        </div>
        <div class="col-lg-4 col-sm-6 col-md-6 col-xs-12">
            <div class="form-group">
                <label class="control-label">Tipo de Zona<span class="text-danger">&nbsp;(*)</span></label>
                <input readonly type="text" value="{{$cliente->tipo_zona}}" class="form-control" >
            </div>
        </div>
        <div class="col-lg-4 col-sm-6 col-md-6 col-xs-12">
            <div class="form-group delpmbottom">
                <label class="control-label">Nombre Zona<span class="text-danger">&nbsp;(*)</span></label>
                <input readonly value="{{$cliente->nombre_zona}}"type="text" class="form-control">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4 col-sm-6 col-md-6 col-xs-12">
            <div class="form-group">
                <label class="control-label">Departamento Residencia<span class="text-danger">&nbsp;(*)</span></label>
                <input readonly type="text" value="{{$cliente->departamento}}" class="form-control" >
            </div>
        </div>
        <div class="col-lg-4 col-sm-6 col-md-6 col-xs-12">
            <div class="form-group delpmbottom">
                <label class="control-label"> Provincia Residencia<span class="text-danger">&nbsp;(*)</span></label>
                <input readonly type="text" value="{{$cliente->provincia}}" class="form-control">
            </div>
        </div>
        <div class="col-lg-4 col-sm-6 col-md-6 col-xs-12">
            <div class="form-group delpmbottom">
                <label class="control-label">Distrito Residencia<span class="text-danger">&nbsp;(*)</span></label>
                <div id="dist2">
                    <input readonly type="text" value="{{$cliente->distrito}}" class="form-control" >
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4 col-sm-6 col-md-6 col-xs-12">
            <div class="form-group">
                <label class="control-label">Referencia de vivienda<span class="text-danger">(*)</span></label>
                <input  readonly type="text" value="{{$cliente->referencia_direccion}}" class="form-control ">
            </div>
        </div>
    </div>
</fieldset>

<fieldset class="scheduler-border">
    <legend class="scheduler-border">Datos de acceso</legend>
    <div class="row">
        <div class="col-lg-4 col-sm-6 col-md-6 col-xs-12">
            <div class="form-group delpmbottom">
                <label class="control-label">Usuario<span class="text-danger">&nbsp;(*)</span></label>
                <input readonly type="text" value="{{$cliente->name}}" class="form-control">
            </div>
        </div>
        <div class="col-lg-4 col-sm-6 col-md-6 col-xs-12">
            <div class="form-group delpmbottom">
                <label class="control-label">Email<span class="text-danger">&nbsp;(*)</span></label>
                <div id="dist2">
                    <input readonly type="text" value="{{$cliente->email}}" class="form-control" >
                </div>
            </div>
        </div>
    </div>
</fieldset>