<span class="fa fa-close cerrar cerrar_modal"></span>

<div class="row">
<div class="col-lg-12 col-md-6 col-sm-6 col-xs-12">
    <h3>Nuevo Cliente</h3>
</div>
</div>
<form  id="f_crear_cliente"  method="post"  action="crear_cliente" class="formentrada" enctype="multipart/form-data">
    {{ csrf_field() }}


        <fieldset class="scheduler-border">
        <legend class="scheduler-border">Datos personales</legend>
        <div class="row">
            <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12" >
                <div class="form-group">
                    <label for="nombre">Nombre y apellidos / Razón social<span class="obligatorio">*</span></label>
                    <input type="text" id="nombre_apellido_razon" required value="" name="nombre_razon" class="form-control" placeholder="Nombre o Razon Social..."/>
                </div>
            </div>
            <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12" >
                <div class="form-group">
                    <label for="dni">DNI / RUC<span class="obligatorio">*</span></label>
                    <input type="number" id="dni" required value="" name="dni_ruc" class="form-control" placeholder="DNI o RUC..."/>
                </div>
            </div>
            <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12" >
                <div class="form-group">
                    <label for="precio">Teléfono<span class="obligatorio">*</span></label>
                    <input type="tel"  required value="" name="telefono" class="form-control" placeholder="Teléfono..."/>
                </div>
            </div>
            <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12" >
                <div class="form-group">
                    <label for="precio">Teléfono adicional<span class="obligatorio"></span></label>
                    <input type="tel"  required value="" name="telefono_adicional" class="form-control" placeholder="Teléfono adicional..."/>
                </div>
            </div>
            <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12" >
                <div class="form-group">
                    <label for="precio">Email</label>
                    <input type="email"value="" name="email" class="form-control" placeholder="Email..." />

                </div>
            </div>
        </div>
    </fieldset>



    <fieldset class="scheduler-border">
        <legend class="scheduler-border">Dirección</legend>
        <div class="row">
            <div class="col-lg-4 col-sm-6 col-md-4 col-xs-12">
                <div class="form-group ">
                    <label class="control-label">Tipo de Vía<span class="text-danger">&nbsp;(*)</span></label>
                    <select required class="form-control" name="tipo_via" data-error="Ingrese un tipo de via">
                        <option value="">Seleccione...</option>
                        <option value="alameda">ALAMEDA</option>
                        <option value="avenida">AVENIDA</option>
                        <option value="bajada">BAJADA</option>
                        <option value="calle">CALLE</option>
                        <option value="camino_afirmado">CAMINO AFIRMADO</option>
                        <option value="camino">CAMINO RURAL</option>
                        <option value="carretera">CARRETERA</option>
                        <option value="galeria">GALERIA</option>
                        <option value="jiron">JIRÓN</option>
                        <option value="malecon">MALECÓN</option>
                        <option value="otros">OTROS</option>
                        <option value="ovalo">OVALO</option>
                        <option value="parque">PARQUE</option>
                        <option value="pasaje">PASAJE</option>
                        <option value="paseo">PASEO</option>
                        <option value="plaza">PLAZA</option>
                        <option value="plazuela">PLAZUELA</option>
                        <option value="portal">PORTAL</option>
                        <option value="prolongacion">PROLONGACIÓN</option>
                        <option value="trocha">TROCHA</option>
                        <option value="troch_carrozable">TROCHA CARROZABLE</option>
                    </select >
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6 col-md-4 col-xs-12">
                <div class="form-group delpmbottom">
                    <label class="control-label">Nombre de Vía (Dirección)<span class="text-danger">&nbsp;(*)</span></label>
                    <input required value="{{old('nombre_via')}}" type="text" name="nombre_via" class="form-control" maxlength="80" placeholder="Introduce Nombre de Vía" data-error="Ingrese un nombre de via">
                    <div class="help-block with-errors"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-sm-6 col-md-4 col-xs-12">
                <div class="form-group ">
                    <label class="control-label">NRO. Vivienda<span class="text-danger">&nbsp;(*)</span> </label>
                    <input required type="text" value="{{old('numero_vivienda')}}" name="numero_vivienda" class="form-control" maxlength="8"
                           data-error="Ingrese su nro de vivienda" placeholder="Ingrese Nro de Vivienda">
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6 col-md-4 col-xs-12">
                <div class="form-group">
                    <label class="control-label">Tipo de Zona<span class="text-danger">&nbsp;(*)</span></label>
                    <select required name="tipo_zona" value="{{old('tipo_zona')}}" class="form-control" data-error="Seleccione tipo de zona">
                        <option value="">Seleccione...</option>
                        <option value="asentamiento humano">ASENTAMIENTO HUMANO </option>
                        <option value="caserio">CASERÍO </option>
                        <option value="conjunto habitacional">CONJUNTO HABITACIONAL</option>
                        <option value="cooperativa">COOPERATIVA</option>
                        <option value="fundo">FUNDO</option>
                        <option value="grupo">GRUPO </option >
                        <option value="otros">OTROS</option>
                        <option value="pueblo joven">PUEBLO JOVEN </option>
                        <option value="residencial">RESIDENCIAL  </option>
                        <option value="unidad vecinal">UNIDAD VECINAL  </option>
                        <option value="urbanizacion">URBANIZACIÓN</option>
                        <option value="zona industrial">ZONA INDUSTRIAL </option>
                    </select>

                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <div class="col-lg-2 col-sm-6 col-md-4 col-xs-12">
                <div class="form-group delpmbottom">
                    <label class="control-label">Nombre Zona<span class="text-danger">&nbsp;(*)</span></label>
                    <input required value="{{old('nombre_zona')}}" name="nombre_zona" type="text" class="form-control"
                           maxlength="80" placeholder="Nombre de zona" data-error="Ingrese un nombre de zona">
                    <div class="help-block with-errors"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-sm-6 col-md-4 col-xs-12">
                <div class="form-group">
                    <label class="control-label">Departamento Residencia<span class="text-danger">&nbsp;(*)</span></label>
                    <select required name="departamento" class="form-control" data-error="Seleccione departamento">
                        <option id="valor_defecto" value="">Seleccione...</option>
                        <option value="lambayeque">LAMBAYEQUE</option>
                        <!--<option value="01">01 AMAZONAS</option>
                        <option value="02">02 ANCASH</option>
                        <option value="03">03 APURIMAC</option>
                        <option value="04">04 AREQUIPA</option>
                        <option value="05">05 AYACUCHO</option>
                        <option value="06">06 CAJAMARCA</option>
                        <option value="07">07 CALLAO</option>
                        <option value="08">08 CUSCO</option>
                        <option value="09">09 HUANCAVELICA</option>
                        <option value="10">10 HUANUCO</option>
                        <option value="11">11 ICA</option>
                        <option value="12">12 JUNIN</option>
                        <option value="13">13 LA LIBERTAD</option>

                        <option value="15">15 LIMA</option>
                        <option value="16">16 LORETO</option>
                        <option value="17">17 MADRE DE DIOS</option>
                        <option value="18">18 MOQUEGUA</option>
                        <option value="19">19 PASCO</option>
                        <option value="20">20 PIURA</option>
                        <option value="21">21 PUNO</option>
                        <option value="22">22 SAN MARTIN</option>
                        <option value="23">23 TACNA</option>
                        <option value="24">24 TUMBES</option>
                        <option value="25">25 UCAYALI</option>-->
                    </select>
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6 col-md-4 col-xs-12">
                <div class="form-group delpmbottom">
                    <label class="control-label"> Provincia Residencia<span class="text-danger">&nbsp;(*)</span></label>
                    <select required name="provincia" id="provincia" class="form-control" data-error="Seleccione provincia">
                        <option id="" value="">Seleccione...</option>
                        <option value="chiclayo">CHICLAYO</option>
                        <option value="ferreñafe">FERREÑAFE</option>
                        <option value="lambayeque">LAMBAYEQUE</option>
                    </select>
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6 col-md-4 col-xs-12">
                    <div class="form-group delpmbottom">
                        <label class="control-label">Distrito Residencia<span class="text-danger">&nbsp;(*)</span></label>
                        <div id="dist2">
                            <select required class="form-control" id="distrito" name="distrito" data-error="Seleccione distrito">
                                <option value="">Seleccione...</option>
                            </select>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                </div>
        </div>
        <div class="row">
            <div class="col-lg-2 col-sm-6 col-md-4 col-xs-12">
                <div class="form-group">
                    <label class="control-label">Referencia de vivienda<span class="text-danger">(*)</span></label>
                    <input  type="text" value="{{old('referencia')}}" class="form-control " maxlength="100"   name="referencia" placeholder="Ingrese referencia de vivienda" >
                </div>
            </div>
        </div>
    </fieldset>



        <fieldset class="scheduler-border">
            <legend class="scheduler-border">Datos de acceso</legend>
            <div class="row">
                <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12" >
                    <div class="form-group">
                        <label for="precio">Usuario<span class="obligatorio">*</span></label>
                        <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                            <input type="text" required value="" name="nombre_de_usuario" id="name_user" class="form-control" placeholder="Usuario..." />
                            <div style="cursor: pointer" class="input-group-addon"><span class="fa fa-user"></span></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12" >
                    <div class="form-group">
                        <label for="precio">Contraseña<span class="obligatorio">*</span></label>
                        <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                            <input type="text" required value="" name="clave" id="pass_user" class="form-control" placeholder="Contraseña..." />
                            <div style="cursor: pointer" class="input-group-addon"><span class="fa fa-lock"></span></div>
                        </div>
                    </div>
                </div>

            </div>
        </fieldset>
         <div class="row">


            <div class="col-lg-12 col-sm-12 col-md-6 col-xs-12" >
                <div id="msj" class="msj"></div>
                <div class="form-group">
                    <button class="btn btn-primary" type="submit">Guardar</button>
                    <button class="btn btn-danger" type="reset">Cancelar</button>
                </div>
            </div>
         </div>

</form>

<script>


    $(document).on('change','#nombre_apellido_razon,#dni',function(){
        var nombre = $('#nombre_apellido_razon'),
            dni = $('#dni'),
            name_user = $('#name_user'),
            pass_user = $('#pass_user'),
            new_name,
            new_pass;

        if(nombre.val() != ""  && dni.val() != "" ){
            new_name = dni.val();
            new_pass = dni.val();

            name_user.val($.trim(new_name));
            pass_user.val($.trim(new_pass));
        }
    })
</script>