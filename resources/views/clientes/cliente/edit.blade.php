<span class="fa fa-close cerrar cerrar_modal"></span>

<div class="row">
    <div class="col-lg-12 col-md-6 col-sm-6 col-xs-12">
        <h3>Editar cliente: <b>{{ $cliente->nombre_razon}}</b></h3>
    </div>
</div>

<form  id="f_editar_cliente"  method="post"  action="editar_cliente" class="formentrada" enctype="multipart/form-data">
    {{ csrf_field() }}

    <fieldset class="scheduler-border">
    <legend class="scheduler-border">Datos Personales</legend>
        <input type="hidden" name="idcliente" value="{{ $cliente->idcliente}}"/>
        <div class="row">
            <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12" >
                <div class="form-group">
                    <label for="nombre">Nombre y Apellidos / Razon Social</label>
                    <input type="text" required value="{{ $cliente->nombre_razon}}" name="nombre_razon" class="form-control" placeholder="Nombre o Razón Social.."/>
                </div>
            </div>
            <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12" >
                <div class="form-group">
                    <label for="dni">DNI / RUC</label>
                    <input type="number" required value="{{ $cliente->dni}}" name="dni_ruc" class="form-control" placeholder="DNI o RUC..."/>
                </div>
            </div>
            <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12" >
                <div class="form-group">
                    <label for="precio">Teléfono</label>
                    <input type="tel"  required value="{{ $cliente->telefono}}" name="telefono" class="form-control" placeholder="Teléfono..."/>
                </div>
            </div>
            <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12" >
                <div class="form-group">
                    <label for="precio">Teléfono adicional</label>
                    <input type="tel"  required value="{{ $cliente->telefono_adicional}}" name="telefono_adicional" class="form-control" placeholder="Teléfono..."/>
                </div>
            </div>
        </div>
    </fieldset>

    <fieldset class="scheduler-border">
        <legend class="scheduler-border">Dirección</legend>
        <div class="row">
        <div class="col-lg-4 col-sm-6 col-md-4 col-xs-12">
            <div class="form-group ">
                <label class="control-label">Tipo de Vía<span class="text-danger">&nbsp;(*)</span></label>
                <select required class="form-control" name="tipo_via" data-error="Ingrese un tipo de via">
                    <option value="">Seleccione...</option>
                    <option value="alameda" {{ $cliente->tipo_via == 'alameda' ? 'selected' : ''}} >ALAMEDA</option>
                    <option value="avenida" {{ $cliente->tipo_via == 'avenida' ? 'selected' : ''}} >AVENIDA</option>
                    <option value="bajada" {{ $cliente->tipo_via == 'bajada' ? 'selected' : ''}}>BAJADA</option>
                    <option value="calle" {{ $cliente->tipo_via == 'calle' ? 'selected' : ''}}>CALLE</option>
                    <option value="camino_afirmado" {{ $cliente->tipo_via == 'camino_afirmado' ? 'selected' : ''}}>CAMINO AFIRMADO</option>
                    <option value="camino" {{ $cliente->tipo_via == 'camino' ? 'selected' : ''}}>CAMINO RURAL</option>
                    <option value="carretera" {{ $cliente->tipo_via == 'carretera' ? 'selected' : ''}}>CARRETERA</option>
                    <option value="galeria" {{ $cliente->tipo_via == 'galeria' ? 'selected' : ''}}>GALERIA</option>
                    <option value="jiron" {{ $cliente->tipo_via == 'jiron' ? 'selected' : ''}}> JIRÓN</option>
                    <option value="malecon" {{ $cliente->tipo_via == 'avenida' ? 'malecon' : ''}}>MALECÓN</option>
                    <option value="otros" {{ $cliente->tipo_via == 'otros' ? 'selected' : ''}}>OTROS</option>
                    <option value="ovalo" {{ $cliente->tipo_via == 'ovalo' ? 'selected' : ''}}>OVALO</option>
                    <option value="parque" {{ $cliente->tipo_via == 'parque' ? 'selected' : ''}}>PARQUE</option>
                    <option value="pasaje" {{ $cliente->tipo_via == 'pasaje' ? 'selected' : ''}}>PASAJE</option>
                    <option value="paseo" {{ $cliente->tipo_via == 'paseo' ? 'selected' : ''}}>PASEO</option>
                    <option value="plaza" {{ $cliente->tipo_via == 'plaza' ? 'selected' : ''}}>PLAZA</option>
                    <option value="plazuela"> {{ $cliente->tipo_via == 'plazuela' ? 'selected' : ''}}PLAZUELA</option>
                    <option value="portal" {{ $cliente->tipo_via == 'portal' ? 'selected' : ''}}>PORTAL</option>
                    <option value="prolongacion" {{ $cliente->tipo_via == 'prolongacion' ? 'selected' : ''}}>PROLONGACIÓN</option>
                    <option value="trocha" {{ $cliente->tipo_via == 'trocha' ? 'selected' : ''}}>TROCHA</option>
                    <option value="trocha_carrozable" {{ $cliente->tipo_via == 'trocha_carrozable' ? 'selected' : ''}}>TROCHA CARROZABLE</option>
                </select >
                <div class="help-block with-errors"></div>
            </div>
        </div>
        <div class="col-lg-4 col-sm-6 col-md-6 col-xs-12">
            <div class="form-group delpmbottom">
                <label class="control-label">Nombre de Vía (Dirección)<span class="text-danger">&nbsp;(*)</span></label>
                <input required value="{{$cliente->nombre_via}}" type="text" name="nombre_via" class="form-control" maxlength="80" placeholder="Introduce Nombre de Vía" data-error="Ingrese un nombre de via">
                <div class="help-block with-errors"></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4 col-sm-6 col-md-6 col-xs-12">
            <div class="form-group ">
                <label class="control-label">NRO. Vivienda<span class="text-danger">&nbsp;(*)</span> </label>
                <input required type="text" value="{{$cliente->numero_vivienda}}" name="numero_vivienda" class="form-control" maxlength="8"
                       data-error="Ingrese su nro de vivienda" placeholder="Ingrese Nro de Vivienda">
                <div class="help-block with-errors"></div>
            </div>
        </div>
        <div class="col-lg-4 col-sm-6 col-md-6 col-xs-12">
            <div class="form-group">
                <label class="control-label">Tipo de Zona<span class="text-danger">&nbsp;(*)</span></label>
                <select required name="tipo_zona"  class="form-control" data-error="Seleccione tipo de zona">
                    <option value="">Seleccione...</option>
                    <option value="asentamiento humano" {{ $cliente->tipo_zona == 'asentamiento humano' ? 'selected' : ''}}>ASENTAMIENTO HUMANO </option>
                    <option value="caserio" {{ $cliente->tipo_zona == 'cacerio' ? 'selected' : ''}}>CASERÍO </option>
                    <option value="conjunto habitacional" {{ $cliente->tipo_zona == 'conjunto habitacional' ? 'selected' : ''}}>CONJUNTO HABITACIONAL</option>
                    <option value="cooperativa" {{ $cliente->tipo_zona == 'cooperativa' ? 'selected' : ''}}>COOPERATIVA</option>
                    <option value="fundo" {{ $cliente->tipo_zona == 'fundo' ? 'selected' : ''}}>FUNDO</option>
                    <option value="grupo" {{ $cliente->tipo_zona == 'grupo' ? 'selected' : ''}}>GRUPO </option >
                    <option value="otros" {{ $cliente->tipo_zona == 'otros' ? 'selected' : ''}}>OTROS</option>
                    <option value="pueblo joven" {{ $cliente->tipo_zona == 'pueblo' ? 'selected' : ''}}>PUEBLO JOVEN </option>
                    <option value="residencial" {{ $cliente->tipo_zona == 'residencial' ? 'selected' : ''}}>RESIDENCIAL  </option>
                    <option value="unidad vecinal" {{ $cliente->tipo_zona == 'unidad vecinal' ? 'selected' : ''}}>UNIDAD VECINAL  </option>
                    <option value="urbanizacion" {{ $cliente->tipo_zona == 'urbanizacion' ? 'selected' : ''}}>URBANIZACIÓN</option>
                    <option value="zona industrial" {{ $cliente->tipo_zona == 'zona industrial' ? 'selected' : ''}}>ZONA INDUSTRIAL </option>
                </select>

                <div class="help-block with-errors"></div>
            </div>
        </div>
        <div class="col-lg-4 col-sm-6 col-md-6 col-xs-12">
            <div class="form-group delpmbottom">
                <label class="control-label">Nombre Zona<span class="text-danger">&nbsp;(*)</span></label>
                <input required value="{{$cliente->nombre_zona}}" name="nombre_zona" type="text" class="form-control"
                       maxlength="80" placeholder="Nombre de zona" data-error="Ingrese un nombre de zona">
                <div class="help-block with-errors"></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4 col-sm-6 col-md-6 col-xs-12">
            <div class="form-group">
                <label class="control-label">Departamento Residencia<span class="text-danger">&nbsp;(*)</span></label>
                <select required name="departamento" class="form-control" data-error="Seleccione departamento">
                    <option id="valor_defecto" value="">Seleccione...</option>
                    <option value="lambayeque" {{ $cliente->departamento == 'lambayeque' ? 'selected' : ''}}>LAMBAYEQUE</option>
                </select>
                <div class="help-block with-errors"></div>
            </div>
        </div>
        <div class="col-lg-4 col-sm-6 col-md-6 col-xs-12">
            <div class="form-group delpmbottom">
                <label class="control-label"> Provincia Residencia<span class="text-danger">&nbsp;(*)</span></label>
                <select required name="provincia" id="provincia" class="form-control" data-error="Seleccione provincia">
                    <option id="" value="">Seleccione...</option>
                    <option value="chiclayo" {{ $cliente->provincia == 'chiclayo' ? 'selected' : ''}}>CHICLAYO</option>
                    <option value="ferreñafe" {{ $cliente->provincia == 'ferreñafe' ? 'selected' : ''}}>FERREÑAFE</option>
                    <option value="lambayeque" {{ $cliente->provincia == 'lambayeque' ? 'selected' : ''}}>LAMBAYEQUE</option>
                </select>
                <div class="help-block with-errors"></div>
            </div>
        </div>
        <div class="col-lg-4 col-sm-6 col-md-6 col-xs-12">
            <div class="form-group delpmbottom">
                <label class="control-label">Distrito Residencia<span class="text-danger">&nbsp;(*)</span></label>
                <div id="dist2">
                    <select required class="form-control" id="distrito" name="distrito" data-error="Seleccione distrito">
                        <option value="">Seleccione...</option>
                        <option value="{{$cliente->distrito}}" selected>{{$cliente->distrito}}</option>
                    </select>
                    <div class="help-block with-errors"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-4 col-sm-6 col-md-6 col-xs-12">
            <div class="form-group">
                <label class="control-label">Referencia de vivienda<span class="text-danger">(*)</span></label>
                <input  type="text" value="{{$cliente->referencia_direccion}}" class="form-control " maxlength="100"   name="referencia" placeholder="Ingrese referencia de vivienda" >
            </div>
        </div>
    </div>
    </fieldset>

    <fieldset class="scheduler-border">
        <legend class="scheduler-border">Datos de acceso</legend>
        <h4>Para editar datos de acceso ir a la sección: Acceso / clientes</h4>
        <input type="hidden" name="clave" value="123"/>
        <input type="hidden" name="nombre_de_usuario" value="123"/>
    </fieldset>
    <div class="col-lg-12 col-sm-12 col-md-6 col-xs-12" >
        <div id="msj" class="msj"></div>
        <div class="form-group">
            <button class="btn btn-primary" type="submit">Guardar</button>
            <button class="btn btn-danger" type="reset">Cancelar</button>
        </div>
    </div>
</form>
