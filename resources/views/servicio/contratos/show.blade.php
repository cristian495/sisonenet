<div class="row">
    <div class="col-lg-12 col-md-6 col-sm-6 col-xs-12">
        <h3>Ver Contrato N°: <b>{{$contrato->id.' - '.$contrato->nombre_razon_cliente}}</b></h3>
        <h5 >Estado del contrato: <span style="color: {{$contrato->estado ? 'green' : 'red'}}"> {{$contrato->estado ? 'Activo' : 'Anulado'}}</span></h5>
    </div>
    <div>

    </div>
</div>



<fieldset class="scheduler-border">
    <legend class="scheduler-border">Datos del cliente</legend>
    <div class="row">
        <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
            <div class="form-group">
                <label for="nombre">Nombre / Razon Social<span class="obligatorio ">*</span></label>

                <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                    <input type="text" readonly class="form-control" value="{{ $contrato->nombre_razon_cliente}}"/>
                    <div style="cursor: pointer" class="input-group-addon"><span class="fa fa-user"></span></div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
            <div class="form-group">
                <label for="dni">DNI / RUC<span class="obligatorio ">*</span></label>
                <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                    <input type="text" readonly required value="{{$contrato->dni}}"  class="form-control" />
                    <div style="cursor: pointer" class="input-group-addon"><span class="fa fa-address-card"></span></div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
            <div class="form-group">
                <label for="precio">Teléfono <span class="obligatorio ">*</span></label>
                <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                    <input type="text" readonly class="form-control" value="{{ $contrato->telefono }}"/>
                    <div style="cursor: pointer" class="input-group-addon"><span class="fa fa-phone"></span></div>
                </div>
            </div>
        </div>

        <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
            <div class="form-group">
                <label for="precio">Dirección <span class="obligatorio ">*</span></label>
                <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                    <input type="text" readonly value="{{ $contrato->direccion }}"  class="form-control"/>
                    <div style="cursor: pointer" class="input-group-addon"><span class="fa fa-map-marker"></span></div>
                </div>
            </div>
        </div>
    </div>
</fieldset>





<fieldset class="scheduler-border">
    <legend class="scheduler-border">Datos del servicio</legend>
    <div class="row">


        <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
            <div class="form-group">
                <label for="nombre">Paquete <span class="obligatorio ">*</span></label>
                <input type="text" readonly value="{{$contrato->paquete}}" class="form-control"/>
            </div>
        </div>

        <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
            <div class="form-group">
                <label for="nombre">Costo del paquete <span class="obligatorio ">*</span></label>
                <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                    <div style="cursor: pointer" class="input-group-addon">S/.</div>
                    <input readonly type="text" value="{{$contrato->precio_mensual}}" class="form-control" id="costo_paquete"/>
                    <div style="cursor: pointer" class="input-group-addon">Nuevos Soles</div>
                </div>
            </div>
        </div>

        <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
            <div class="form-group">
                <label for="mbdesc">Mbps de descarga <span class="obligatorio ">*</span></label>
                <input type="text" readonly required value="{{$contrato->megas_descarga_comercial}}" class="form-control"/>
            </div>
        </div>

        <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
            <div class="form-group">
                <label for="mbsub">Mbps de subida <span class="obligatorio ">*</span></label>
                <input type="text" readonly required value="{{$contrato->megas_subida_comercial}}" class="form-control"/>
            </div>
        </div>







        <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
            <div class="form-group">
                <label for="inicio_contrato">Fecha inicio Contrato <span class="obligatorio ">*</span></label>
                <input  type="text" readonly class="form-control" value="{{$contrato->fecha_inicio_contrato}}"/>
            </div>
        </div>

        <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
            <div class="form-group">
                <label for="duracion_contrato">Duración del contrato <span class="obligatorio ">*</span></label>
                <input type="text" readonly class="form-control"  value="{{$contrato->duracion_contrato}}"/>
            </div>
        </div>

        <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
            <div class="form-group">
                <label for="fin_contrato">Fin del contrato <span class="obligatorio ">*</span></label>
                <input  type="text" readonly class="form-control" value="{{$contrato->fecha_fin_contrato}}"/>
            </div>
        </div>

        <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
            <div class="form-group">
                <label for="fecha_pago">Fecha de pago <span class="obligatorio ">*</span></label>
                <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                    <div class="cont_fecha_desc">
                        <input  disabled type="text" value="{{$contrato->fecha_pago}}" class="form-control" id="fecha_pago"/>
                    </div>
                    <div style="cursor: pointer" class="input-group-addon"><span class="fa fa-calendar"></span></div>
                </div>
            </div>
        </div>

        <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
            <div class="form-group">
                <label for="fecha_corte">Fecha de corte <span class="obligatorio ">*</span></label>
                <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                    <div class="cont_fecha_desc">
                        <input  readonly type="text" value="{{$contrato->fecha_corte}}" class="form-control" />
                    </div>
                    <div style="cursor: pointer" class="input-group-addon"><span class="fa fa-calendar"></span></div>
                </div>
            </div>
        </div>


    </div>
</fieldset>









<fieldset class="scheduler-border">
    <legend class="scheduler-border">Datos de instalación</legend>
    <div class="row">

        <div class=" col-lg-12 col-sm-8 col-md-8 col-xs-12">
            <div class="col-lg-4 col-sm-4 col-md-8 col-xs-12">
                <label for=""><b>Costo de Instalación</b><span class="obligatorio">*</span></label>
                <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                    <div style="cursor: pointer" class="input-group-addon">S/.</div>
                    <input type="text" readonly value="{{$contrato->costo_instalacion}}" required="" name="costo_instalacion" id="costo_instalacion" class="form-control"/>
                    <div style="cursor: pointer" class="input-group-addon">Nuevos Soles</div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-4 col-md-8 col-xs-12">
                <label for=""><b>Costo del Access Point</b><span class="obligatorio">*</span></label>
                <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                    <div style="cursor: pointer" class="input-group-addon">S/.</div>
                    <input type="text" readonly value="{{$contrato->costo_ap}}" required="" name="costo_ap" id="costo_ap" class="form-control"/>
                    <div style="cursor: pointer" class="input-group-addon">Nuevos Soles</div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-4 col-md-8 col-xs-12">
                <label for=""><b>Tipo de pago del AP</b><span class="obligatorio">*</span></label>
                <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                    <label class="radio-inline">
                        <input disabled {{ ($contrato->tipo_pago_ap == 'contado' ? 'checked' : '')}} required name="tipo_pago_ap" type="radio" id="tipo_pago" value="contado" /> Al contado
                    </label>
                    <label class="radio-inline">
                        <input disabled {{ ($contrato->tipo_pago_ap == 'mensual' ? 'checked' : '')}} required name="tipo_pago_ap" type="radio" id="tipo_pago" value="mensual" /> Mensualmente
                    </label>
                </div>
            </div>
        </div>
    </div>
</fieldset>

<fieldset class="scheduler-border" id="detalles_pago" >
    <legend class="scheduler-border">Detalles de Pago</legend>
    <div class="row">
        <div style="margin-top: 3em" class=" col-lg-12 col-sm-8 col-md-8 col-xs-12">
            <div class="col-lg-4 col-sm-6 col-md-12 col-xs-12">
                <div class="form-group">
                    <label for=""><b>Instalación</b><span class="obligatorio">*</span></label>
                    <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                        <div style="cursor: pointer" class="input-group-addon">S/.</div>
                        <input readonly type="text" value="{{$contrato->costo_instalacion}}" class="form-control"/>
                        <div style="cursor: pointer" class="input-group-addon">Nuevos Soles</div>
                    </div>
                </div>

            </div>
            <div class="col-lg-4 col-sm-6 col-md-12 col-xs-12">
                <div class="form-group">
                    <label for=""><b>Costo Access Point Mensualmente</b><span class="obligatorio">*</span></label>
                    <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                        <div style="cursor: pointer" class="input-group-addon">S/.</div>
                        <input readonly type="text" value="{{$contrato->costo_ap_mensualmente}}" class="form-control"/>
                        <div style="cursor: pointer" class="input-group-addon">Nuevos Soles</div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6 col-md-12 col-xs-12">
                <div class="form-group">
                    <label for=""><b>Costo Paquete Adquirido</b><span class="obligatorio">*</span></label>
                    <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                        <div style="cursor: pointer" class="input-group-addon">S/.</div>
                        <input readonly type="text" value="{{$contrato->precio_mensual}}"  class="form-control"/>
                        <div style="cursor: pointer" class="input-group-addon">Nuevos Soles</div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-sm-6 col-md-12 col-xs-12">
                <div class="form-group">
                    <label for=""><b>Costo Primera Mensualidad</b><span class="obligatorio">*</span></label>
                    <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                        <div style="cursor: pointer" class="input-group-addon">S/.</div>
                        <input  readonly type="text" value="{{floatval($contrato->costo_instalacion)+floatval($contrato->precio_mensual)+floatval($contrato->costo_ap_mensualmente)}}" class="form-control"/>
                        <div style="cursor: pointer" class="input-group-addon">Nuevos Soles</div>
                    </div>
                </div>

            </div>
            <div class="col-lg-6 col-sm-6 col-md-12 col-xs-12">
                <div class="form-group">
                    <label for=""><b>Costo Demás Mensualidades</b><span class="obligatorio">*</span></label>
                    <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                        <div style="cursor: pointer" class="input-group-addon">S/.</div>
                        <input readonly type="text" value="{{floatval($contrato->precio_mensual)+floatval($contrato->costo_ap_mensualmente)}}"  class="form-control"/>
                        <div style="cursor: pointer" class="input-group-addon">Nuevos Soles</div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</fieldset>


