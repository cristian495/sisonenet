<span class="fa fa-close cerrar cerrar_modal"></span>
<form  id="f_efectuar_pago" method="post" action="mensualidades/efectuar_pago" class="formentrada" enctype="multipart/form-data">
    {{ csrf_field() }}


    <fieldset class="scheduler-border">
        <legend class="scheduler-border">Datos del cliente</legend>
        <div class="row">
            <div class="col-lg-6 cl-sm-6 col-md-6 colo-xs-12">
                <div class="form-group">
                    <label for="nombre">Nombre <span class="obligatorio ">*</span></label>
                    <input type="hidden" value="{{$cliente->idcliente}}" name="idcliente"/>
                    <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                        <input type="hidden" value="" id="idcliente" name="cliente"/>

                        <select class="form-control " disabled>
                            <option id="" value="">: : Seleccionar cliente</option>
                            <option selected value="{{ $cliente->idcliente}}">{{$cliente->nombre_razon_cliente.' - contrato nº ' .$cliente->id}}</option>
                        </select>
                        <div style="cursor: pointer" class="input-group-addon"><span class="fa fa-search"></span></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                <div class="form-group">
                    <label for="dni">DNI <span class="obligatorio ">*</span></label>
                    <input type="text" readonly required value="{{$cliente->dni}}"  class="form-control"
                           placeholder="DNI..."/>
                </div>
            </div>
            <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                <div class="form-group">
                    <label for="precio">Teléfono <span class="obligatorio ">*</span></label>
                    <input type="tel" readonly required value="{{$cliente->telefono}}"  class="form-control"
                           placeholder="Teléfono..."/>
                </div>
            </div>
            <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                <div class="form-group">
                    <label for="precio">Dirección <span class="obligatorio ">*</span></label>
                    <input type="text" readonly value="{{$cliente->direccion}}"  class="form-control"
                           placeholder="Dirección..."/>

                </div>
            </div>
        </div>
    </fieldset>


    <fieldset id="field_mens" class="scheduler-border" >
        <legend class="scheduler-border">Detalles del pago</legend>
        <div class="row">
            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                <label for="nombre" class="col-lg-2 col-sm-2 col-md-6 col-xs-6" style="color: #03639D; font-weight: bold">Tipo de comprobante<span class="obligatorio ">*</span></label>
                <div class="form-group col-lg-4 col-sm-4 col-md-6 col-xs-6">
                    <select required name="tipo_comprobante" id="" class="form-control">
                        <option value="">: : Seleccione un comprobante</option>
                        <option value="boleta">Boleta</option>
                        <option value="factura">Factura</option>
                    </select>
                </div>
            </div>
            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                <label for="serie_comprobante" class="col-lg-2 col-sm-2 col-md-6 col-xs-6" style="color: #03639D; font-weight: bold">Serie de comprobante</label>
                <div class="form-group col-lg-4 col-sm-4 col-md-6 col-xs-6">
                    <input type="text" name="serie_comprobante" class="form-control"/>
                </div>
            </div>
            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                <label for="num_comprobante" class="col-lg-2 col-sm-2 col-md-6 col-xs-6" style="color: #03639D; font-weight: bold">Número de comprobante<span class="obligatorio ">*</span></label>
                <div class="form-group col-lg-4 col-sm-4 col-md-6 col-xs-6">
                    <input type="text" name="num_comprobante" class="form-control"/>
                </div>
            </div>
            <!--            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">-->
            <!--                <label for="num_comprobante" class="col-lg-2 col-sm-2 col-md-6 col-xs-6" style="color: #03639D; font-weight: bold">% Mora por mensualidad<span class="obligatorio "></span></label>-->
            <!--                <div class="form-group col-lg-4 col-sm-4 col-md-6 col-xs-6">-->
            <!--                    <input type="number" name="mora" class="form-control"  min="0" max="5"/>-->
            <!--                </div>-->
            <!--            </div>-->
            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                <table id="detalles" class="table table-striped table-bordered table-condensed table-hover ">
                    <thead style="background-color:#03639D;color: white">
                    <th >Concepto</th>
                    <th >Precio</th>
                    </thead>

                    <tbody id="comprobante_table">
                        @if($mensualidad->estado=="pagado")
                            <tr class="alert alert-success"><td>Esta mensualidad ya ha sido pagada.</td></tr>
                        @elseif($mensualidad->estado=="anulado")
                        <tr class="alert alert-warning"><td>Esta mensualidad ha sido anulada.</td></tr>
                        @else
                        <tr id="12"><td>Mensualidad Nº {{$mensualidad->num_cuota}} - {{$mensualidad->fecha_pago}}</td><td>{{$mensualidad->costo}}</td><td></td></tr>
                        <input type="hidden" name="idmensualidad" value="{{$mensualidad->idmensualidad}}"/>
                        @endif
                    </tbody>
                    <tfoot>
                    <tr>
                        @if($mensualidad->estado=="pendiente")
                            <td id="comprobante_total">{{$mensualidad->costo}}</td>
                            <input value="{{$mensualidad->costo}}" type="hidden" name="costo_total" id="total_pago"/>
                        @else
                        @endif
                        <td>TOTAL</td>

                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </fieldset>







    <div class="row">
        <div id="msj" class="col-lg-12 col-sm-12 col-md-6 col-xs-12"></div>
        <div class="col-lg-12 col-sm-12 col-md-6 col-xs-12">
            <div class="form-group">
                @if($mensualidad->estado=="pendiente")
                    <button class="btn btn-primary" type="submit">Guardar</button>
                @else
                @endif
                <button class="btn btn-danger cerrar_modal" type="reset">Cerrar</button>
            </div>
        </div>
    </div>
</form>
<script>
    $('.selects_comprobante').selectpicker();
</script>
