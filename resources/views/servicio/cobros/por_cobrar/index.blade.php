
<input type="hidden" id="where_i_am" value="section_mensualidades"/>

<div class="box box-primary">

    <div class="box-header">
        <div class="row">
            <div class="col-md-12" style="margin-top: 6px">
                <h3 class="box-title">Listado de Mensualidades</h3>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                <div class="form-group">
                    <label for="nombre">Cliente <span class="obligatorio ">*</span></label>
                    <input type="hidden" id="idpaquete" name="idpaquete" value=""/>
                    <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                        <select data-live-search="true"
                                class="search  form-control">
                            <option value="">: : Seleccionar cliente</option>
                            @foreach($contratos_cliente as $contrato_cliente)
                            <option value="{{ $contrato_cliente->id}}">{{$contrato_cliente->nombre_razon_cliente . '  Contrato N° '. $contrato_cliente->id}}</option>
                            @endforeach
                        </select>
                        <div style="cursor: pointer" class="input-group-addon" id="buscar_mensualidad_cliente">
                            <span class="fa fa-search"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>


<!--        <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">-->
<!--            <div class="form-group">-->
<!--                <label for="nombre">Mostrar cobros por rango de fechas <span class="obligatorio ">*</span></label>-->
<!--                <input type="hidden" id="idpaquete" name="idpaquete" value=""/>-->
<!--                <div class='input-group date'>-->
<!--                    <input type='text' class="form-control" id='picker_buscar'/>-->
<!--                    <span class="glyphicon glyphicon-calendar input-group-addon "></span>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->

    </div>

    <div class="box-body">
        <div class=" dataTables_wrapper form-inline dt-bootstrap">
            <div class="row col-md-12 col-sm-12" style="padding: 0;margin-bottom: 30px">
                <div class=" col-md-4">
                    <label for="">
                        cantidad de filas
                    </label>
                    <select class="form-control" name="" id="myInputSelectField">
                        <option value="5">5</option>
                        <option value="10" selected>13</option>
                        <option value="500">200</option>
                    </select>
                </div>
                <div class="row col-sm-4">
                    <div class=" col-sm-4">
                        <a  href="#"  id="reporte_pdf" class=" btn btn-danger"><span class="fa fa-file-pdf-o"></span> PDF</a>
                    </div>
                    <div class=" col-sm-4">
                        <a  href="#" id="reporte_xls" class="reporte btn btn-success"><span class="fa fa-file-excel-o"></span> XLS</a>
                    </div>
                </div>
                <div class=" col-md-4" style="text-align: right;">
                    <input class="form-control" type="text" placeholder="Buscar..." id="myInputTextField">
                </div>
            </div>
            <table id="por_cobrar" style="width: 100%" class="table-responsive table table-hover table-bordered">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Cliente</th>
                    <th>Nº Cuota</th>
                    <th>Fecha Cuota</th>
                    <th>Costo</th>
                    <th>Estado</th>
                    <th class="no-sort"><i class="fa fa-cog"></i></th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('.search').selectpicker();
    function generateModal() {
        var data = oTable.column(0).data();
        //console.log(data);
        data.each(function (value, index) {
            html = "<div class=\"modal fade modal-slide-in-rights\"  aria-hidden=\"true\" role=\"dialog\" tabindex=\"-1\" id=\"modal-delete-" + value + "\" >";
            html += "<form action=\"servicio/cobros/realizar_corte\" method=\"get\">";
            html += "<div class=\"modal-dialog\">";
            html += "<div class=\"modal-content\">";
            html += " <div class=\"modal-header\">";
            html += "<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">";
            html += "<span aria-hidden=\"true\">x</span>";
            html += "</button>";
            html += "<h4 class=\"modal-title\">Realizar corte del servicio</h4>";
            html += "</div>";
            html += "<div class=\"modal-body\">";
            html += "<p>Confirme si desea cortar el servicio</p>";
            html += "</div>";
            html += "<div class=\"modal-footer\">";
            html += "<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cerrar</button>";
            html += "<button  type=\"button\" class=\"btn btn-primary\" onclick=\"eliminarRegistro(" + value + ")\">Confirmar</button>";
            html += "</div>";
            html += "</div>";
            html += "</div>";
            html += "</form>";
            html += "</div>";
            $('body').append(html);
        });
    }
    // $.fn.dataTable.moment('DD/MM/YYYY HH:mm');
    function cargar_tabla_contratos(idForSearch) {
        var url = "{{ url('servicio/cobros/listado_porCobrar/') }}/"+idForSearch;
            console.log(url);

       // var _token = $('meta[name="csrf-token"]').attr('content');

        oTable = $('#por_cobrar').DataTable({

            'responsive': true,
            "bDestroy": true,
            "processing": true,
            "serverSide": true,
            bJQueryUI: true,
            //  "dom": "<'row'<'hided'f>>",
            //sDom: '<"form-control"f><"H"lf>t<"F"ip>',
            //sDom: '',
            'lengthChange': false,
            'searching': true,
            "lengthMenu": [13],
            "language": {
                "url": '{!! asset(' / plugins / datatables / latino.json') !!}'
            },

            "ajax": {
                "url": url,
                "type": "post",
                /*'headers': {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }*/
                data: { '_token': '{{ csrf_token() }}' }
            },
            "order": [
                [ 2, "desc" ]
            ],
            "columns": [
                {data: 'idmensualidad', name: 'idmensualidad'},
                {data: 'nombre_razon', name: 'nombre_razon'},
                {data: 'num_cuota', name: 'num_cuota'},
                {data: 'fecha_pago', name: 'fecha_pago',
                    render: function (data) {

                        return '<span style="display:none;">' + data.replace(/[/]/g, "") + '</span><span>' + data + '</span>';
                    }},
                {data: 'costo', name: 'costo'},
                {data: 'estado', name: 'estado',
                    render: function (data, type, row, meta) {

                        if (data === "pendiente") {
                            return '<span class="label label-warning">' + data + '</span>'
                        } else if (data === "anulado") {
                            return '<span class="label label-default">' + data + '</span>'
                        } else if (data === "vencido") {
                            return '<span class="label label-danger">' + data + '</span>'
                        } else if (data === "pagado") {
                            return '<span class="label label-success">' + data + '</span>'
                        }
                        // return (data==="pendiente") ? '<span class="text-yellow">'+data+'</span>' : (data === "anulado") ? '<span class="text-muted">'+data+'</span>' : '' ;

                    }
                },

                { data: null,
                    render: function (data, type, row) {
                        /*return "<a onclick='cargar_formulario(\"mostrar_detalles\"," + data.id + ")' class='btn btn-xs btn-success' data-toggle='tooltip' data-placement='top' title='Ver'>" +
                            "<i class='fa fa-eye'></i>" +
                            "</a>  " +
                            "<a onclick='cargar_formulario(\"form_notificar_cliente\"," + data.id + ")' class='btn btn-xs btn-primary' data-toggle='tooltip' data-placement='top' title='Notificar'>" +
                            "<i class='fa fa-edit'></i>" +
                            "</a>  " +
                            "<a onclick='cargar_formulario(\"form_nuevo_pago_mens\"," + data.idcliente + ","+data.idmensualidad+","+data.idcontrato+")' style='background-color:#FFCE44; color:white' class='btn btn-xs ' data-toggle='tooltip' data-placement='top' title='Cobrar'>" +
                            "<i class='fa fa-money'></i>" +
                            "</a>  " +
                            "<a onclick='showModalDelete(" + data.id + ")' class='btn btn-xs btn-danger' data-toggle='tooltip' data-placement='top' title='Cortar servicio'>" +
                            "<i class='fa fa-warning'></i>" +
                            "</a> "*/
                        return "<a onclick='cargar_formulario(\"detalle_mensualidad\"," + data.idmensualidad + ")' class='btn btn-xs btn-success' data-toggle='tooltip' data-placement='top' title='Ver'>" +
                            "<i class='fa fa-eye'></i>" +
                            "</a>  " +
                            "<a onclick='cargar_formulario(\"form_nuevo_pago_mens\"," + data.idcliente + ","+data.idmensualidad+","+data.idcontrato+")' style='background-color:#FFCE44; color:white' class='btn btn-xs ' data-toggle='tooltip' data-placement='top' title='Cobrar'>" +
                            "<i class='fa fa-money'></i>" +
                            "</a>  "
                    }
                }


            ],
            "columnDefs": [{
                    "targets": "no-sort",
                    "orderable": false,
                    "searchable": false
                }]


        });


        $('#myInputTextField').keyup(function () {
            oTable.search($(this).val()).draw();
            //oTable.fnFilter($(this).val());
        });
        $('#myInputSelectField').on('change', function () {
            // oTable.search($(this).val()).draw() ;
            oTable.page.len($(this).val()).draw();
        });


    }



   /* function RefreshTable(tableId, urlData)

    {
        $.getJSON(urlData, null, function( json )
        {
            table = $(tableId).dataTable();
            oSettings = table.fnSettings();
            table.fnClearTable(this);
            for (var i=0; i<json.aaData.length; i++)
            {
                table.oApi._fnAddData(oSettings, json.aaData[i]);
            }
            oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();
            table.fnDraw();
        });
    }*/


    function hacer_busqueda(e){
       // console.log(e.type)
        if(e.type == 'click' ){
            var idForSearch= $(this).prev('.btn-group.bootstrap-select').find('select').val(),
                reporte_pdf = $('#reporte_pdf'),
                reporte_xls = $('#reporte_xls'),
                new_href_pdf,
                new_href_xls;
             //console.log($(this).prev('.btn-group.bootstrap-select').find('select').val());

             if(idForSearch != "" || e.wich === 13)
             {
                new_href_pdf = '{{url("mensualidades/reporte/pdf")}}/'+idForSearch;
                new_href_xls = '{{url("mensualidades/reporte/xls")}}/'+idForSearch;
                cargar_tabla_contratos(idForSearch);
                reporte_pdf.attr('href',new_href_pdf);
                reporte_xls.attr('href', new_href_xls);
             }else
             {
                alert('debe seleccionar un cliente')
             }
        }

    }

    $('#buscar_mensualidad_cliente').bind("click",hacer_busqueda);
    $('.btn-group.bootstrap-select').bind("keyup",function(e){
        if(e.keyCode==13)
            $('#buscar_mensualidad_cliente').click()
    });

</script>

