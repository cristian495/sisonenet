
<span class="fa fa-close cerrar cerrar_modal"></span>

<div class="col-lg-12 col-md-6 col-sm-6 col-xs-12">
    <h3>Mensualidad N°: <b>{{$mensualidad->num_cuota}}</b> -

        <?php
        $estado = "";
        if($mensualidad->estado=="pendiente")
            $estado = 'warning';
        elseif($mensualidad->estado=="pagado")
            $estado = 'success';
        elseif($mensualidad->estado=="anulado")
            $estado = 'default';

        ?>
        <span class="label label-{{$estado}}">{{$mensualidad->estado}}</span>
    </h3>
    <h4>
        Contrato N° {{$mensualidad->idcontrato}} - {{$mensualidad->nombre_razon}}
    </h4>
</div>
<br>



<fieldset id="field_mens" class="scheduler-border" >
    <legend class="scheduler-border">Detalles de mensualidad</legend>
    <div class="row">
        <div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
            <div class="form-group">
                <label for="dni">N° mensualidad <span class="obligatorio ">*</span></label>
                <input type="text" readonly  value="{{$mensualidad->num_cuota}}"  class="form-control"
                    />
            </div>
        </div>
        <div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
            <div class="form-group">
                <label for="dni">Fecha de pago <span class="obligatorio ">*</span></label>
                <input type="text" readonly  value="{{$fecha_pago}}"  class="form-control"
                    />
            </div>
        </div>
        <div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
            <div class="form-group">
                <label for="dni">Fecha de corte <span class="obligatorio ">*</span></label>
                <input type="text" readonly  value="{{$fecha_corte}}"  class="form-control"
                    />
            </div>
        </div>
        <div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
            <div class="form-group">
                <label for="dni">Costo <span class="obligatorio ">*</span></label>
                <input type="text" readonly  value="{{$mensualidad->costo}}"  class="form-control"
                    />
            </div>
        </div>
    </div>
</fieldset>