<link rel="stylesheet" href="{{asset('css/icheck.css')}}"/>
<input type="hidden" id="where_i_am" value="section_comprobante"/>
<div class="box box-primary">
    <div class="box-header">
        <div class="row col-md-3" style="margin-top: 6px">
            <h3 class="box-title">Listado de Comprobantes</h3>
        </div>
        <div class="row col-md-4">
            <div class="margin-top-2 btn btn-success" onclick="cargar_formulario('form_nuevo_comprobante')">
                <span class="fa fa-plus"></span>
                Nuevo
            </div>
        </div>
        <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
            <div class="form-group">
                <label for="nombre">Cliente <span class="obligatorio ">*</span></label>
                <input type="hidden" id="idpaquete" name="idpaquete" value=""/>
                <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                    <select data-live-search="true"
                            class="search  form-control">
                        <option value="">: : Seleccionar cliente</option>
                        @foreach($contratos_cliente as $contrato_cliente)
                        <option value="{{ $contrato_cliente->id}}">{{$contrato_cliente->nombre_razon_cliente . '  Contrato N° '. $contrato_cliente->id}}</option>
                        @endforeach
                    </select>
                    <div style="cursor: pointer" class="input-group-addon" id="buscar_comprobante_cliente">
                        <span class="fa fa-search"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="box-body">
        <div class=" dataTables_wrapper form-inline dt-bootstrap">
            <div class="row col-md-12 col-sm-12" style="padding: 0;margin-bottom: 30px">
                <div class=" col-md-4">
                    <label for="">
                        cantidad de filas
                    </label>
                    <select class="form-control" name="" id="myInputSelectField">
                        <option value="5">5</option>
                        <option value="10" selected>13</option>
                        <option value="500">200</option>
                    </select>
                </div>
                <div class="row col-sm-4">
                    <div class=" col-sm-4">
                        <a  href="#"  id="reporte_pdf" class=" btn btn-danger"><span class="fa fa-file-pdf-o"></span> PDF</a>
                    </div>
                    <div class=" col-sm-4">
                        <a  href="#" id="reporte_xls" class="reporte btn btn-success"><span class="fa fa-file-excel-o"></span> XLS</a>
                    </div>
                </div>
                <div class=" col-md-4" style="text-align: right;">
                    <input class="form-control" type="text" placeholder="Buscar..." id="myInputTextField">
                </div>
            </div>
            <table id="comprobantes" style="width: 100%" class="table-responsive table table-hover table-bordered">
                <thead>
                <tr>
                    <th>Nº</th>
                    <th>Cliente</th>
                    <th>Tipo comprobante</th>
                    <th>Nº comprobante</th>
                    <th>Total</th>
                    <th>Fecha emision</th>
                    <th class="no-sort"><i class="fa fa-cog"></i></th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
<script type="text/javascript">


    function cargar_tabla_comprobantes(idForSearch){
        var url = "{{ url('/servicio/cobros/listado_comprobantes') }}/"+idForSearch;
        //console.log(url);

        oTable = $('#comprobantes').DataTable({

            'responsive': true,
            "bDestroy": true,
            "processing": true,
            "serverSide": true,
            bJQueryUI:true,
            //  "dom": "<'row'<'hided'f>>",
            //sDom: '<"form-control"f><"H"lf>t<"F"ip>',
            //sDom: '',
            'lengthChange': false,
            'searching'   : true,
            "language": {
                "url": '{!! asset('/plugins/datatables/latino.json') !!}'
            } ,
            "ajax": {
                "url":url,
                "type":"post",
                data: { '_token': '{{ csrf_token() }}' }

            },
            "order": [[ 0, "desc" ]],
            "columns": [
                {data: 'idpago_mensualidad', name: 'idpago_mensualidad'},
                {data: 'nombre_razon', name: 'nombre_razon'},
                {data: 'tipo_comprobante', name: 'tipo_comprobante'},
                {data: 'num_comprobante', name: 'num_comprobante'},
                {data: 'total', name: 'total'},
                {data: 'fecha_hora_pagado', name: 'fecha_hora_pagado'},
                /*{"defaultContent": "<a   class='btn btn-xs btn-success' ><i class='fa fa-eye'></i></a>  " +
                 "<a class='btn btn-xs btn-primary' onclick='alert(\"EDITAR\")' ><i class='fa fa-edit'></i></a>  " +
                 "<a class='btn btn-xs btn-danger' onclick='alert(\"ELIMINAR\")'><i class='fa fa-close'></i></a>"}*/
                { data: null,
                    render: function ( data, type, row) {
                        return "<a onclick='cargar_formulario(\"mostrar_detalles\","+ data.idpago_mensualidad +")' class='btn btn-xs btn-success' >" +
                            "<i class='fa fa-eye'></i>" +
                            "</a>  "+
                            "<a href='comprobante/pdf_coprobante/"+data.idpago_mensualidad+"'  class='btn btn-xs btn-warning' data-toggle='tooltip' data-placement='top' title='Imprimir comprobante'>" +
                            "<i class='fa fa-print'></i>" +
                            "</a> "
                    }
                }
                /* { data: null,  render: function ( data, type, row ) {
                 return "<a href='{{ url('form_editar_contacto/') }}/"+ data.id +"' class='btn btn-xs btn-primary' >Editar</button>"  }
                 },*/


            ],
            "columnDefs": [ {
                "targets": 'no-sort',
                "orderable": false,
                "searchable":false
            }]


        });



        $('#myInputTextField').keyup(function(){
            oTable.search($(this).val()).draw() ;
            //oTable.fnFilter($(this).val());
        });
        $('#myInputSelectField').on('change',function(){
            // oTable.search($(this).val()).draw() ;
            oTable.page.len($(this).val()).draw() ;
        });


    }

    //cargar_tabla_comprobantes(0);

</script>
<script type="text/javascript" src="{{asset('plugins/icheck/icheck.min.js')}}"></script>
<script>
    $('.search').selectpicker();
    function hacer_busqueda(e){
        // console.log(e.type)
        if(e.type == 'click' ){
            var idForSearch= $(this).prev('.btn-group.bootstrap-select').find('select').val(),
                reporte_pdf = $('#reporte_pdf'),
                reporte_xls = $('#reporte_xls'),
                new_href_pdf,
                new_href_xls;
            console.log($(this).prev('.btn-group.bootstrap-select').find('select').val());

            if(idForSearch != "" || e.wich === 13)
            {
                new_href_pdf = '{{url("comprobantes/reporte/pdf")}}/'+idForSearch;
                new_href_xls = '{{url("comprobantes/reporte/xls")}}/'+idForSearch;
                cargar_tabla_comprobantes(idForSearch);
                reporte_pdf.attr('href',new_href_pdf);
                reporte_xls.attr('href', new_href_xls);
            }else
            {
                alert('debe seleccionar un cliente')
            }
        }
 //alert('sd');
    }
    $('#buscar_comprobante_cliente').bind("click",hacer_busqueda);
    $('.btn-group.bootstrap-select').bind("keyup",function(e){
        if(e.keyCode==13)
            $('#buscar_comprobante_cliente').click()
    });
</script>