@extends('adminlte::layouts.layout_pdf')
@section('title')
Mensualidades del contrato N° {{$mensualidades[0]->idcontrato}} - {{$mensualidades[0]->nombre_razon}}
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <table border="1" class="table  table-bordered table-striped">
                <tr class="table-info">
                    <th class="titulo_tabla">id</th>
                    <th class="titulo_tabla">Num cuota</th>
                    <th class="titulo_tabla">Cliente</th>
                    <th class="titulo_tabla">fecha de pago</th>
                    <th class="titulo_tabla">costo</th>
                    <th class="titulo_tabla">estado</th>
                </tr>

                @foreach($mensualidades as $mensualidad)
                <tr>
                    <td>{{$mensualidad->idmensualidad}}</td>
                    <td>{{$mensualidad->num_cuota}}</td>
                    <td>{{$mensualidad->nombre_razon}}</td>
                    <td>{{$mensualidad->fecha_pago}}</td>
                    <td>{{$mensualidad->costo}}</td>
                    <td>{{$mensualidad->estado}}</td>
                </tr>
                @endforeach
            </table>
        </div>
    </div>
</div>
@endsection