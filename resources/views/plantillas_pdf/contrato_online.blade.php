
<body>
<style>
    .titulo_principal{
        text-align: center;
        font-size: 12px;
    }

    .parrafo,.texto{
        font-size: 12px;
        text-align: justify;
        line-height: 1.5;
        font-weight: 100;
    }

    .subtitulo{
        font-size: 10px;
    }
    li{
        list-style: none;
    }

    td{
        text-align: center;
    }
    td.titulo_tabla{
        background-color: #91BCE3;
    }
    .casilla_subtitulo{
        font-weight: bold;
    }

</style>
<?php
/** FUNCION PARA CALCULAR PORCENTAJE**/
/*function porcentaje($cantidad,$porciento,$decimales){
    return number_format($cantidad*$porciento/100 ,$decimales);
}
*/
/** CALCULAR COSTO DEL AP Y EL PRIMER MES **/
/*function costo_access_point($tipo_pago,$costo_soles_ap,$num_meses){
    if($tipo_pago=="mensual")
    {
        $ap_mensual = round((floatval($costo_soles_ap)/intval($num_meses))+floatval(porcentaje(floatval($costo_soles_ap),1.13,1)),1);
    }
    else if($tipo_pago =="contado")
    {
        $ap_mensual = round(floatval($costo_soles_ap),1);
    }
    else
    {
        $ap_mensual = 0.00;
    }

    return $ap_mensual;
}*/
/*$meses = intval($contrato->duracion_contrato)*intval($numMeses);
$costo_mensual_ap = costo_access_point($contrato->tipo_pago_ap,$contrato->costo_ap,$meses);*/
?>
<h1 class="titulo_principal">CONTRATO DE PRESTACIÓN DE SERVICIO DE INTERNET INALÁMBRICA</h1>
<p class="parrafo">
    Conste por el presente documento el Contrato denominado
    “Prestación de Servicio de Internet Inalámbrica”, que
    celebran de una parte <b>“ONE NET SAC.”</b>, con
    RUC N° 20602445004, inscrita en la partida
    electrónica N° 11262984 del registro de personas
    jurídicas de la zona registral N° II – Sede Chiclayo,
    debidamente representada por don <b>Wilfredo Picón García</b>,
    con domicilio en la Mz. “K” – Lote N° 04, de la
    Urbanización Tres de Octubre, del distrito y provincia
    de Chiclayo, departamento de Lambayeque; a quien en
    adelante se le denominará <b>“EL PRESTADOR”</b>; y, <b>“EL CLIENTE”</b>,
    cuyos datos constan en el <b>Anexo denominado</b>,
    Datos del CLIENTE del presente Contrato; en los
    términos y condiciones señaladas en las siguientes
    Cláusulas:
</p>

    <div>&nbsp;</div>
    <h3 class="subtitulo">EL OBJETO DEL CONTRATO</h3>
<p class="parrafo">
    <b class="puestos">Primera:</b> El objeto del presente Contrato es
    la prestación del servicio de venta de Internet
    inalámbrico por banda libre, por parte de
    “EL PRESTADOR”, regulado conforme al artículo 1757°
    del Código Civil, y a las estipulaciones y Anexos
    que se pactan, dentro del área de cobertura de
    “EL PRESTADOR”.
</p>


<div>&nbsp;</div>
<h3 class="subtitulo">EL MEDIO DE LA PRESTACIÓN DEL SERVICIO</h3>
<p class="parrafo">
    <b class="puestos">Segunda:</b> El servicio de acceso
    a internet inalámbrico, será prestado a través de un
    receptor inalámbrico – ACCES POINT [Equipo], el mismo
    que al ser conectado por EL CLIENTE a una computadora
    personal, o computadora portátil, le proporcionará
    la posibilidad de transferir datos, tales como la
    navegación en internet, intercambio de correos
    electrónicos, entre otros.
</p>


<div>&nbsp;</div>
<h3 class="subtitulo">LA FORMA O MODO DE LA PRESTACIÓN DEL SERVICIO</h3>
<p class="parrafo">
    <b class="puestos">Tercera:</b> La forma de adquirir
    el servicio por EL CLIENTE, es a través de planes
    tarifarios conteniendo paquetes de diferentes
    costos, de acuerdo a las velocidades de navegación y
    capacidades de descarga establecidas en el <b>Anexo de
    planes tarifarios </b>adjunto, así como las velocidades
    mínimas garantizadas.
</p>



<div>&nbsp;</div>
<div>&nbsp;</div>
<h3 class="subtitulo">PRECIO DEL SERVICIO Y PAGOS QUE INCLUYE </h3>
<p class="parrafo">
    <b class="puestos">Cuarto:</b> Como contraprestación
    por el servicio contratado, EL CLIENTE, pagará a
    EL PRESTADOR las tarifas [costos] vigentes según el
    plan y paquete tarifario contratado. Los recibos por
    el servicio deben ser cancelados en los lugares
    establecidos por EL PRESTADOR.
</p>
<p class="parrafo">
    <b class="puestos">Quinta:</b> Constituye un costo [pago]
    del servicio contratado a cargo de EL CLIENTE, el
    precio de la instalación de S/. 30.00 Soles por única
    vez, y el precio del receptor inalámbrico [Equipo]
    de S/. 300.00 Soles. La adquisición del receptor
    inalámbrico [Equipo] se efectúa pagando el precio
    al contado a la suscripción del presente Contrato,
    o en su defecto, en cuotas de <span style="background-color: yellow;">{{$contrato->costo_ap_mensualmente}} </span>Soles mensuales
    por el plazo de dos años, pago éste último que será
    incluido [cargado] en el recibo mensual que se le hará
    llegar a EL CLIENTE de acuerdo al costo del plan tarifario
    y paquete contratado [servicio contratado].
</p>


<div>&nbsp;</div>
<h3 class="subtitulo">FORMA DE PAGO Y FACTURACIÓN </h3>
<p class="parrafo">
    <b class="puestos">Sexta:</b> La forma de pago del
    servicio contratado por EL CLIENTE, es por adelantado
    a su instalación, según el plan y paquete tarifario
    que adquiera, siendo posteriormente las facturaciones
    en forma mensual o periódica de conformidad con lo
    regulado en el artículo 1759° del Código Civil, y
    a través del recibo que se le hará llegar al domicilio
    de EL CLIENTE; a un correo electrónico o visualizarlo
    en la página web de la empresa.
</p>



<div>&nbsp;</div>
<h3 class="subtitulo">PLAZO DEL CONTRATO </h3>
<p class="parrafo">
    <b class="puestos">Sétima:</b> El Contrato que suscriben
    EL PRESTADOR y EL CLIENTE tiene una vigencia de <span style="background-color: yellow">{{$contrato->duracion_contrato}} </span> año(s),
    y se inicia con la instalación del servicio.
</p>


<div>&nbsp;</div>
<h3 class="subtitulo">PROMOCIONES POR CONTRATACIÓN DEL SERVICIO  </h3>
<p class="parrafo">
    <b class="puestos">Octava:</b> EL PRESTADOR, con ocasión
    del lanzamiento del servicio objeto de contratación,
    ofrece como promociones u ofertas las que se detallan
    a continuación, siempre y cuando la contratación del
    servicio por parte de EL CLIENTE, <b>tenga lugar entre
    dentro del primer mes de haberse lanzado u ofrecido
    el producto:</b>
</p>



<span class="numero" >8.1</span>
<span class="texto">
    El costo [precio] de instalación del servicio
    contratado, es de S/. 1.00 Sol, por única vez.
</span>
<br>
<span class="numero" >8.2</span>
<span class="texto">
    El descuento del 20% sobre el costo [precio] de los
    planes y paquetes tarifarios ofrecidos, y que
    EL CLIENTE adquiera o contrate.
</span>



<div>&nbsp;</div>
<h3 class="subtitulo">OPERATIVIDAD DEL PLAN ADQUIRIDO </h3>
<p class="parrafo">
    <b class="puestos">Noveno:</b>EL PRESTADOR garantiza
    la operatividad y el funcionamiento del plan
    contratado en los estándares de velocidad mínima
    y máxima, siendo el 40% la velocidad mínima contratada,
    y el 100% la velocidad máxima contratada, según el
    paquete del plan tarifario contratado por EL CLIENTE.

</p>
<p class="parrafo">
    <b class="puestos">Décimo:</b>Si la velocidad del plan
    contratado disminuye por debajo del estándar señalado
    en la cláusula anterior, debido a factores externos,
    como fenómenos naturales, caso fortuito, o fuerza mayor,
    conforme al artículo 1315° del Código Civil; EL PRESTADOR,
    no será responsable por dicho suceso o circunstancia
    acaecida fuera de su alcance, por lo que, no existe
    lugar a reclamo alguno por parte de EL CLIENTE.
</p>
<p class="parrafo">
    La disminución de la velocidad del plan contratado
    por EL CLIENTE por debajo de la velocidad mínima [40%],
    sólo pude ser debido a factores internos, como acciones
    de mantenimiento, reparación, y otras cuestiones técnicas
    propias, o relacionadas, al servicio que se ofrece; ante
    lo cual, EL PRESTADOR dará aviso con una anticipación no
    menor a cuarenta y ocho [48] horas con la finalidad de
    informar y no perjudicar a EL CLIENTE; siendo el plazo
    de.……días calendarios para reponer la operatividad normal
    del servicio por parte de EL PRESTADOR.
</p>
<p class="parrafo">
    En ambos supuestos descritos, se procederá a
    realizar por parte de EL PRESTADOR, la devolución
    de la parte proporcional al tiempo de interrupción
    del servicio según el plan contratado, en tanto
    corresponda.
</p>
<p class="parrafo">
    <b class="puestos">Décimo primero:</b>Si la velocidad del
    plan contratado se produce por averías o destrucción del
    receptor inalámbrico [Equipo], imputables a EL CLIENTE,
    que lo hagan inoperativo o deficiente en su operatividad,
    el precio de su renovación o restitución asciende a
    S/. 360.00 Soles efectuados al contado.
</p>
<p class="parrafo">
    Sin perjuicio de ello, y mientras EL CLIENTE no
    ponga en conocimiento dicho suceso, EL PRESTADOR
    continuará emitiendo el recibo mensual por
    facturación del plan contratado.
</p>



<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<h3 class="subtitulo">OBLIGACIONES DEL CLIENTE </h3>
<p class="parrafo">
    <b class="puestos">Décimo segundo:</b>Son obligaciones del Cliente:
    <br>

    <span class="numero" >12.1</span>
    <span class="texto">
       Utilizar adecuadamente y conforme a las leyes
        vigentes el servicio contratado.
    </span>
    <br>

    <span class="numero" >12.2</span>
    <span class="texto">
      Queda pactado que EL CLIENTE es el responsable del
      uso que haga del servicio contratado,
      <span>&nbsp; &nbsp;
      &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      </span>por lo que, está prohibido:
    </span>
    <br>

    <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
    <span class="numero">
        12.2.1.
    </span>
    <span class="texto">
      Que utilice el servicio para su reventa.
    </span>
    <br>


    <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
    <span class="numero" >
        12.2.2.
    </span>
    <span class="texto">
      Cualquier otro uso que desnaturalice el uso
        personal del servicio contratado.
    </span>
    <br>



    <span class="numero" >12.3</span>
    <span class="texto">Pagar puntualmente el servicio contratado en la forma
        y plazo pactados, de acuerdo al plan
        <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
        tarifario y paquete contratado.
    </span>
    <br>

    <span class="numero" >12.4</span>
    <span class="texto">
      Comunicar el cambio de su domicilio,
        a través de una comunicación verbal o
        escrita dirigida a
        <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
        EL PRESTADOR. En caso
        de no comunicar el cambio de domicilio,
        se consideran válidos los
        <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
        requerimientos de pago, la notificación del
        recibo, y demás notificaciones que se cursen,
        al
        <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
        domicilio
        que dio a conocer a EL PRESTADOR.
    </span>
    <br>


    <span class="numero" >12.5</span>
    <span class="texto">Informar inmediatamente a EL PRESTADOR, sobre el
        hurto, robo, extravío, o avería, del
        <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
        receptor inalámbrico [Equipo], para proceder a la restauración del mismo y del servicio.
    </span>
    <br>
</p>


<div>&nbsp;</div>
<h3 class="subtitulo">INTERÉS POR MORA</h3>
<p class="parrafo">
    <b class="puestos">Décimo tercero:</b>
    Las partes acuerdan que todo pago no efectuado hasta <span style="background-color: yellow">
    {{$contrato->fecha_pago}}</span> de la fecha de vencimiento del
    recibo, constituirá a EL CLIENTE en mora en forma
    automática sin necesidad de requerimiento alguno,
    y generará el cobro por parte de EL PRESTADOR de la
    tasa de interés legal fijada por el BCR del Perú; y
    corte del servicio
</p>

<div>&nbsp;</div>
<h3 class="subtitulo">CLÁUSULAS DE RESOLUCIÓN DEL CONTRATO</h3>
<p class="parrafo">
    <b class="puestos">Décimo cuarto:</b>Son causales de resolución del contrato:<br>
    12.1. En caso EL CLIENTE no proporcione
    información cierta en relación a sus datos.<br>
    12.2. En caso EL CLIENTE no renueve el
    receptor inalámbrico [Equipo] para la
    operatividad del plan contratado y del
    servicio, dentro de los sesenta días calendarios,
    contados a partir de que EL CLIENTE ponga en
    conocimiento la avería o destrucción del mismo.<br>
    12.3. En caso EL CLIENTE haga uso indebido
    del servicio, debiendo EL PRESTADOR en dicho
    caso, seguir el procedimiento establecido por
    OSIPTEL.<br>
    12.4. En cualquiera de los casos contemplados
    en la normativa vigente, conforme al artículo 1371°
    del Código Civil.

</p>


<div>&nbsp;</div>
<h3 class="subtitulo">RESTABLECIMIENTO DEL SERVICIO POR CORTE Y ACCIÓN PARA EL COBRO DE DEUDA</h3>
<p class="parrafo">
    <b class="puestos">Décimo quinto:</b>: EL PRESTADOR,
    ante el incumplimiento de EL (LOS) CLIENTE(S) en
    el pago de dos meses consecutivos por el plan
    tarifario contratado, está facultado al corte
    del servicio, el cual será repuesto una vez que
    EL(LOS) CLIENTE(S) cancele(n) la totalidad de la
    deuda con los intereses moratorios generados a la
    fecha de pago.
</p>

<p class="parrafo">
    <b class="puestos">Décimo sexto:</b>En caso EL (LOS)
    CLIENTE(S), persista(n) en el incumplimiento del
    pago de las cuotas vencidas, EL PRESTADOR, está
    facultado para proceder conforme al artículo
    1323° del Código Civil, e iniciar las acciones
    judiciales que contempla el artículo 1219° del
    Código Civil, y lograr así la satisfacción de la
    acreencia o el monto adeudado por EL (LOS) CLIENTE(S).
</p>


<div>&nbsp;</div>
<h3 class="subtitulo">LEY Y JURISDICCIÓN APLICABLE</h3>
<p class="parrafo">
    <b class="puestos">Décimo sétimo:</b>: : El presente Contrato y demás
    documentos contractuales suscritos por EL CLIENTE
    y EL PRESTADOR del servicio, se rigen por las
    leyes de la República del Perú. Las partes someten
    cualquier conflicto a la competencia de los jueces
    y tribunales de la Provincia de Chiclayo.
</p>



<div>&nbsp;</div>
<h3 class="subtitulo">INFORMACIÓN DE CONTENIDO PROMOCIONAL O PUBLICITARIO    </h3>
<p class="parrafo">
    <b class="puestos">Décimo octavo:</b>: Durante la
    vigencia del Contrato, y siempre que sea autorizado
    por EL CLIENTE, EL PRESTADOR podrá mandar mensajes
    [correos electrónicos] con contenido informativo,
    publicitario y/o promocional respecto de ofertas,
    planes tarifarios de EL PRESTADOR; y demás información
    relacionada al servicio y cualquier bien y/o servicio
    comercializado por EL PRESTADOR.
</p>
<p class="parrafo">
    EL PRESTADOR, entrega a EL CLIENTE copia del
    Contrato y documentación informativa sobre las
    condiciones del servicio, las condiciones tarifarias
    contratadas, entre otros.
</p>

<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>
    <table style="width: 100%">
        <tr>
            <td width="45%" style="font-size:10px;border-top: 1px solid black; text-align: center">
                Representante Legal de ONE NET SAC
            </td>
            <td width="10%"></td>
            <td width="45%"style="font-size:10px;border-top: 1px solid black; text-align: center">
                Cliente/Representante legal del Cliente
            </td>
        </tr>

    </table>

</div>
<div>&nbsp;</div>

<h1 class="titulo_principal">ANEXOS 01</h1>
<h1 class="titulo_principal">DATOS DEL CLIENTE</h1>
<div class="casilla">
    <span class="casilla_subtitulo" >
        DNI/RUC:</span>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    {{$contrato->dni}}
</div>
<div class="casilla">
    <span class="casilla_subtitulo" >
        NOMBRES Y APELLIDOS / RAZON SOCIAL:
    </span>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    {{ ucwords($contrato->nombre_razon_cliente)}}
</div>
<div class="casilla">
    <span class="casilla_subtitulo" >
        E-MAIL:
    </span>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    {{ $email->email }}
</div>

<div class="casilla">
    <span class="casilla_subtitulo" >
        DIRECCION:
    </span>
    <div class="casilla">
        <span class="casilla_subtitulo" >
            TIPO VIA:
         </span>
        {{ $contrato->tipo_via}}
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <span class="casilla_subtitulo" >
            NOMBRE DE VIA:
         </span>
        {{ $contrato->nombre_via}}
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <br>
        <span class="casilla_subtitulo" >
            NUMERO DE VIVIENDA:
         </span>
        {{$contrato->numero_vivienda}}
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <span class="casilla_subtitulo" >
            TIPO DE ZONA:
         </span>
        {{ $contrato->tipo_zona}}
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <br>
         <span class="casilla_subtitulo" >
            NOMBRE DE ZONA:
         </span>
        {{ $contrato->nombre_zona}}
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <br>
        <span class="casilla_subtitulo" >
            DEPARTAMENTO:
         </span>
        {{ $contrato->departamento}}
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <span class="casilla_subtitulo" >
            PROVINCIA:
         </span>
        {{ $contrato->provincia}}
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <span class="casilla_subtitulo" >
            DISTRITO:
         </span>
        {{ $contrato->distrito}}
        <br>
        <span class="casilla_subtitulo" >
            REFERENCIA:
        </span>
        {{$contrato->referencia}}
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    </div>
</div>

<div class="casilla">
    <span class="casilla_subtitulo" >
        TELEFONO:
    </span>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    {{ $contrato->telefono}}
</div>

<div class="casilla">
    <span class="casilla_subtitulo" >
        TELEFONO ADICIONAL:
    </span>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    {{ $contrato->telefono_adicional}}
</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<h1 class="titulo_principal">ANEXOS 02</h1>
<h1 class="titulo_principal">PLANES TARIFARIOS</h1>
<?php $cont=0 ?>
<table border="1">
    <tr>
        <td class="titulo_tabla">ITEM</td>
        <td class="titulo_tabla">PAQUETE</td>
        <td class="titulo_tabla">COSTO</td>
        <td class="titulo_tabla">PAQUETE ADQUIRIDO</td>
    </tr>

    @foreach($planes as $plan)
    <tr>
        <td>{{$cont=$cont+1}}</td>
        <td>{{$plan->nombre}}</td>
        <td>{{$plan->precio_mensual}}</td>
        <td>{{'(' }} {{$plan->idpaquete  == $contrato->idpaquete ?  ' x ' : ' '}} {{ ')'}}</td>
    </tr>
    @endforeach
</table>

</body>