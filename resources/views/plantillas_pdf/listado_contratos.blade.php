@extends('adminlte::layouts.layout_pdf')
@section('title')
Listado de clientes
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <table border="1" class="table  table-bordered table-striped">
                <tr class="table-info">
                    <th class="titulo_tabla">id</th>
                    <th class="titulo_tabla">Cliente</th>
                    <th class="titulo_tabla">DNI</th>
                    <th class="titulo_tabla">Dirección</th>
                    <th class="titulo_tabla">Plan</th>
                    <th class="titulo_tabla">Fecha pago</th>
                    <th class="titulo_tabla">Fecha corte</th>
                    <th class="titulo_tabla">Incio del contrato</th>
                    <th class="titulo_tabla">Duración</th>
                    <th class="titulo_tabla">Fin del contrato</th>
                    <th class="titulo_tabla">Costo de instalación</th>
                    <th class="titulo_tabla">Tipo pago/AP</th>
                    <th class="titulo_tabla">Costo mensual/AP</th>
                    <th class="titulo_tabla">Estado</th>
                </tr>


                @foreach($contratos as $contrato)
                <tr>
                    <td>{{$contrato->id}}</td>
                    <td>{{$contrato->nombre_razon_cliente}}</td>
                    <td>{{$contrato->dni}}</td>
                    <td>{{$contrato->direccion}}</td>
                    <td>{{$contrato->paquete}}</td>
                    <td>{{$contrato->fecha_pago}}</td>
                    <td>{{$contrato->fecha_corte}}</td>
                    <td>{{$contrato->fecha_inicio_contrato}}</td>
                    <td>{{$contrato->duracion_contrato}}</td>
                    <td>{{$contrato->fecha_fin_contrato}}</td>
                    <td>{{$contrato->costo_instalacion}}</td>
                    <td>{{$contrato->tipo_pago_ap}}</td>
                    <td>{{$contrato->costo_ap_mensualmente}}</td>
                    <td>{{$contrato->estado}}</td>
                </tr>
                @endforeach
            </table>
        </div>
    </div>
</div>
@endsection