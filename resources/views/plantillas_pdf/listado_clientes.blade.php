@extends('adminlte::layouts.layout_pdf')
@section('title')
Listado de clientes
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <table border="1" class="table  table-bordered table-striped">
                <tr class="table-info">
                    <th class="titulo_tabla">id</th>
                    <th class="titulo_tabla">Cliente</th>
                    <th class="titulo_tabla">DNI</th>
                    <th class="titulo_tabla">Telefono</th>
                    <th class="titulo_tabla">Telefono adicional</th>
                    <th class="titulo_tabla">Dirección</th>
                </tr>

                @foreach($clientes as $cliente)
                <tr>
                    <td>{{$cliente->idcliente}}</td>
                    <td>{{$cliente->nombre_razon}}</td>
                    <td>{{$cliente->dni}}</td>
                    <td>{{$cliente->telefono}}</td>
                    <td>{{$cliente->telefono_adicional}}</td>
                    <td>{{$cliente->direccion}}</td>
                </tr>
                @endforeach
            </table>
        </div>
    </div>
</div>
@endsection