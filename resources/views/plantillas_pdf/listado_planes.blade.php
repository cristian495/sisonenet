@extends('adminlte::layouts.layout_pdf')
@section('title')
Listado de clientes
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <table border="1" class="table  table-bordered table-striped">
                <tr class="table-info">
                    <th class="titulo_tabla">id</th>
                    <th class="titulo_tabla">Plan</th>
                    <th class="titulo_tabla">Megas subida</th>
                    <th class="titulo_tabla">Megas descarga</th>
                    <th class="titulo_tabla">Megas subida comercial</th>
                    <th class="titulo_tabla">Megas descarga comercial</th>
                    <th class="titulo_tabla">Costo mensual</th>
                </tr>

                @foreach($planes as $plan)
                <tr>
                    <td>{{$plan->idpaquete}}</td>
                    <td>{{$plan->nombre}}</td>
                    <td>{{$plan->megas_subida}}</td>
                    <td>{{$plan->megas_bajada}}</td>
                    <td>{{$plan->megas_subida_comercial}}</td>
                    <td>{{$plan->megas_descarga_comercial}}</td>
                    <td>{{$plan->precio_mensual}}</td>
                </tr>
                @endforeach
            </table>
        </div>
    </div>
</div>
@endsection