@extends('adminlte::layouts.layout_pdf')
@section('title')
Comprobantes del contrato N° {{$comprobantes[0]->idcontrato}} - {{$comprobantes[0]->nombre_razon}}
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <table border="1" class="table  table-bordered table-striped">


                <tr class="table-info">
                    <th class="titulo_tabla">id</th>
                    <th class="titulo_tabla">Cliente</th>
                    <th class="titulo_tabla">Fecha emitido</th>
                    <th class="titulo_tabla">Tipo Comprobante</th>
                    <th class="titulo_tabla">Numero Comprobante</th>
                    <th class="titulo_tabla">Total</th>
                </tr>

             @foreach($comprobantes as $comprobante)
                <tr>
                    <td>{{$comprobante->idpago_mensualidad}}</td>
                    <td>{{$comprobante->nombre_razon}}</td>
                    <td>{{$comprobante->fecha_hora_pagado}}</td>
                    <td>{{$comprobante->tipo_comprobante}}</td>
                    <td>{{$comprobante->num_comprobante}}</td>
                    <td>{{$comprobante->total}}</td>
                </tr>
                @endforeach
            </table>
        </div>
    </div>
</div>
@endsection