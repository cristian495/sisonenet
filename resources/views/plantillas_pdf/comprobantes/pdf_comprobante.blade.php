@extends('adminlte::layouts.layout_pdf')
@section('title')
Comprobante N° {{$pago->num_comprobante}} - {{$pago->nombre_razon}}
@endsection

@section('content')
<style>
    .table_cabecera{
        width: 100%;
    }
    .table_cabecera .colum{
        width: 50%;

    }

    .table_cabecera2
    {
        width: 100%;
    }
    .table_cabecera2 .colum
    {
        width: 50%;
        text-align: right;
    }
    .table_cabecera3{
        width: 100%;
    }
    .table_cabecera3 .colum{
        width: 50%;
    }

</style>
<table border="0" class="table_cabecera">
    <tr>
        <td class="colum" style="width: 50%">OneNet SAC</td>
        <td class="colum" rowspan="3">
        </td >
    </tr>
    <tr>
        <td class="colum">Urb. 3 de octubre - Chiclayo</td>
    </tr>
    <tr>
        <td class="colum">RUC: 20602445004</td>
    </tr>
</table>
<br/>
<br/>

<table class="table_cabecera2" border="">
   <tr>
       <td></td>
       <td>
           <table border="" class="table_cabecera3">
               <tr>
                   <td class="colum">Factura #:</td>
                   <td class="colum"> {{$pago->num_comprobante}}</td>
               </tr>
               <tr>
                   <td class="colum">Fecha:</td>
                   <td class="colum"> {{$pago->fecha_hora_pagado}}</td>
               </tr>
               <tr>
                   <td class="colum">Monto total:</td>
                   <td class="colum"> {{$pago->total_pago}}</td>
               </tr>
           </table>
       </td>
   </tr>

</table>

<br/>
<br/>
<br/>
<table border="1" class="table  table-bordered table-striped">


    <tr class="table-info">
        <th class="titulo_tabla">Concepto</th>
        <th class="titulo_tabla">precio</th>
    </tr>

    @foreach($detalle_pago as $detalle)
    <tr>
        <td>Mensualidad Nº {{$detalle->num_cuota}} - {{$detalle->fecha_pago}}</td>
        <td>{{$detalle->costo}}</td>
    </tr>
    @endforeach
</table>
@endsection