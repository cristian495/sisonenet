<!-- REQUIRED JS SCRIPTS -->

<!-- JQuery and bootstrap are required by Laravel 5.3 in resources/assets/js/bootstrap.js-->
<!-- Laravel App -->
<script src="{{ asset ('/js/app.js') }}" type="text/javascript"></script>
<script src="{{ asset ('/js/plugins.js') }}" type="text/javascript"></script>
<script src="{{ asset ('/js/scripts.js') }}" type="text/javascript"></script>
<script src="{{asset('plugins/chart/chart.min.js')}}"></script>
<script>
    var ctx = document.getElementById("myChart").getContext("2d");
    data= {
        labels: [
            @foreach($planes as $plan)
            "{{$plan->nombre}}",
            @endforeach
        ],
        datasets: [{
            label: '# of Votes',
            data: [12, 19, 3, 5],
            backgroundColor: [
                '#FF6384',
                '#4BC0C0',
                '#36A2EB',
                '#FFCD56'
            ],
            borderColor: [
                '#FF6384',
                '#4BC0C0',
                '#36A2EB',
                '#FFCD56'
            ],
            borderWidth: 1
        }]
    };
    options= {
        scales: {

        }
    };
    var myPieChart = new Chart(ctx,{
        type: 'pie',
        data: data,
        options: options
    });
</script>
