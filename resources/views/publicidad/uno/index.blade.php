<input type="hidden" id="where_i_am" value="publicidad01"/>

<div class="box box-primary">

    <div class="box-header">
        <div class="row col-md-5 col-sm-12" style="margin-top: 6px">
            <h3 class="box-title">Publicidad 01 - imagen 580px * 300px</h3>
        </div>
    </div>

    <div class="box-body">
        <div id="msj" style="display: none">
        </div>
        <div id="cargando" style="display: none">
            <img src="{{asset('/img/cargando.gif')}}" alt=""/>
        </div>

        <table class="table table-bordered">
            <tbody>
                <tr>
                    <th style="width: 50%">Imagen actual</th>
                    <th style="width: 50%">Nueva imagen</th>
                </tr>
                <tr>
                    <td>
                        <img style="margin:auto; display:block;width: 80%;height: 200px" id="old_img_peque01" src="{{ asset($img->ruta_imagen)}}" alt="{{$img->titulo}}"/>
                    </td>
                    <td>
                        <form id="formulario_imagen" action="publi/uno/subir" enctype="multipart/form-data" method="POST">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="row">
                                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12" >
                                    <div class="form-group">
                                        <input type="hidden" name="id_img_old" value="{{$img->id}}"/>
                                        <input required name="titulo_imagen" type="text" placeholder="titulo de la imagen" class="form-control"/>
                                        <input required="" name="imagen" type="file" placeholder="Elegir imagen" class="form-control"/>
                                    </div>

                                </div>
                            </div>
                            <input type="submit" value="Cambiar imagen" class="btn btn-default subir_imagen"/>
                        </form>
                    </td>
                </tr>
                <tr>
                    <td>
                        <h5 style="text-align: center" id="old_titulo01">{{$img->titulo}}</h5>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>


