<!doctype html>
<html lang="es">
<head>
    <title>@yield('title') - OneNet</title>
    @include('webPage.layout.head')
</head>
<body>
    @if(Session::has('mensaje_contacto'))
    <div class="alert alert-success" alert-dismissable style="position:fixed;z-index:100;top:0;left:0;width:100%;">
        <a href="#" class="close" data-dismiss="alert" aria-label="close"><span class="fa fa-close"></span></a>
        <strong>{{ Session::get('mensaje_contacto') }}!</strong> Nos pondremos en contacto con usted en breve.

    </div>
    @endif
    <input type="hidden" id="url_raiz_proyecto" value="{{ url('/') }}"/>
<!-- falta poner mensaje de correo enviado correctamente-->
    @include('webPage.layout.header')
    <div class="container" style="padding: 0;">
        @yield('content')
        @include('webPage.layout.contactenos_footer')
    </div>
    @include('webPage.layout.footer')
    @yield('scripts')

</body>
</html>