<div class="col-md-4">
    <div class="wow bounceIn pricing hover-effect" data-wow-delay=".2s" data-wow-offset="320">
        <div class="pricing-head">
            <h3>Económico
                <!--                                <span>Officia deserunt mollitia </span>-->
            </h3>
            <h4><i>S/.</i>49<i>.90</i>
                    <span>
                    Mensualmente</span>
            </h4>
        </div>
        <ul class="pricing-content list-unstyled">
            <li>
                <i class="fa fa-check"></i>Internet ilimitado
            </li>
            <li>
                <i class="fa fa-check"></i>Ancho de banda de hasta 1 mbps
            </li>
<!--            <li>-->
<!--                <i class="fa fa-check"></i>0.5 mbps de subida-->
<!--            </li>-->
            <li>
                <i class="fa fa-check"></i>Equipos con garantía
            </li>
            <li>
                <i class="fa fa-check"></i>Soporte técnico
            </li>
        </ul>
        <div class="pricing-footer">
<!--           <p>-->
<!--               Instalacion a $1.00-->
<!--           </p>-->
            <a href="{{url('adquirir_plan/economico')}}" class="btn yellow-crusta">
                Adquirir Plan
            </a>
        </div>
    </div>
</div>

<div class="col-md-4">
    <div class="wow bounceIn pricing pricing-active hover-effect" data-wow-delay=".2s" data-wow-offset="320">
        <div class="pricing-head pricing-head-active">
            <h3>Stándar
                <!--                                <span>Officia deserunt mollitia </span>-->
            </h3>
            <h4><i>S/.</i>79<i>.90</i>
                    <span>
                    Mensualmente </span>
            </h4>
        </div>
        <ul class="pricing-content list-unstyled">
            <li>
                <i class="fa fa-check"></i>Internet ilimitado
            </li>
            <li>
                <i class="fa fa-check"></i>Ancho de banda de hasta 2 mbps
            </li>
<!--            <li>-->
<!--                <i class="fa fa-check"></i>1 mbps de subida-->
<!--            </li>-->
            <li>
                <i class="fa fa-check"></i>Equipos con garantía
            </li>
            <li>
                <i class="fa fa-check"></i>Soporte técnico
            </li>
        </ul>
        <div class="pricing-footer">
<!--                                        <p>-->
<!--                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non libero magna psum olor .-->
<!--                                        </p>-->
            <a href="{{url('adquirir_plan/standar')}}" class="btn yellow-crusta">
                Adquirir Plan
            </a>
        </div>
    </div>
</div>
<div class="col-md-4">
    <div class="wow bounceIn pricing hover-effect" data-wow-delay=".2s" data-wow-offset="320">
        <div class="pricing-head ">
            <h3>Premium
                <!--                                <span>Officia deserunt mollitia </span>-->
            </h3>
            <h4><i>S/.</i>139<i>.90</i>
                    <span>
                    Mensualmente </span>
            </h4>
        </div>
        <ul class="pricing-content list-unstyled">
            <li>
                <i class="fa fa-check"></i>Internet ilimitado
            </li>
            <li>
                <i class="fa fa-check"></i>Ancho de banda de hasta 4 mbps
            </li>
<!--            <li>-->
<!--                <i class="fa fa-check"></i>2 mbps de subida-->
<!--            </li>-->
            <li>
                <i class="fa fa-check"></i>Equipos con garantía
            </li>
            <li>
                <i class="fa fa-check"></i>Equipos especializados y configurados a tu medida
            </li>
            <li>
                <i class="fa fa-check"></i>Soporte técnico y asesoria
            </li>
        </ul>
        <div class="pricing-footer">
<!--                                        <p>-->
<!--                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non libero magna psum olor .-->
<!--                                        </p>-->
            <a href="{{url('adquirir_plan/premium')}}" class="btn yellow-crusta">
                Adquirir Plan
            </a>
        </div>
    </div>
</div>
