<table border="0" cellpadding="0" cellspacing="0" width="570">
    <tbody>
    <tr>
        <td style="text-align: center">
            <img
                src="http://www.onenet.com.pe/img/paginaweb/onenet_logo.png"
                alt="" class="CToWUd a6T" tabindex="0" style="    width: 160px;">

            <h3 style="margin-top: 0;">Felicidades! has adquirido el plan "{{$cliente['nombre_plan']}}"</h3>
        </td>
    </tr>
    <tr>
        <td>
            <table width="360" align="center" cellpadding="6" cellspacing="0" style="border-collapse:collapse">
                <tbody>
                <tr>
                    <td colspan="2" align="center"><p> Y queremos aprovechar la oportunidad para confirmar tus
                            datos:</p></td>
                </tr>
                <tr style="border-bottom:solid 1px #fff">
                    <td class="" bgcolor="#0053a4" style="color: white" width="128"> TITULAR:</td>
                    <td bgcolor="#f7f7f7">{{$cliente['nombre_cliente']}}</td>
                </tr>
                <tr>
                    <td class="m_-1108311476415020897label_box" bgcolor="#0053a4" style="color: white"> DIRECCIÓN:</td>
                    <td bgcolor="#f7f7f7">{{$cliente['direccion_cliente']}}</td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                           &nbsp;</td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td style="background:#f7f7f7;padding:12px 0">
            <table width="500" align="center" cellpadding="4" cellspacing="0" style="border-collapse:collapse">
                <tbody>
                <tr>

                    <td width="218px;"><h2 class=""
                                           style="font-size:24px;font-style:italic;margin:0">Dia de facturación:</h2>

                        <p class="m_-1108311476415020897blue"
                           style="font-size:14px;font-style:italic;margin-top:0;font-weight:bold"> El inicio de tu ciclo
                            de facturación es el {{$cliente['fecha_de_pago']}}. </p>

                    </td>


                </tr>
                </tbody>
            </table>
        </td>
    </tr>


    <tr>
        <td>
            <table width="360" align="center" cellpadding="6" cellspacing="0" style="border-collapse:collapse">
                <tbody>
                <tr>
                    <td colspan="2" align="center"><p> Tus datos de acceso son los siguientes:</p></td>
                </tr>
                <tr style="border-bottom:solid 1px #fff">
                    <td class="" bgcolor="#0053a4" style="color: white" width="128"> USUARIO:</td>
                    <td bgcolor="#f7f7f7">{{$cliente['usuario']}}</td>
                </tr>
                <tr>
                    <td class="m_-1108311476415020897label_box" bgcolor="#0053a4" style="color: white"> CONTRASEÑA:</td>
                    <td bgcolor="#f7f7f7">{{$cliente['password']}}</td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        &nbsp;</td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>

    <tr>
        <td bgcolor="#0053a4"
            background="https://ci6.googleusercontent.com/proxy/u2wBp6e6qGnbKL7bTtJAo2YosLYYSkSSVQfzUtDZLE1Qny_vZAn2VBQK-Uy0TV0RBCTzdv5B6O8DSn5b3dUNJeXwOtI9GBns5VYMoK3X7w9-TIYduwtuCDTesxXVVo6Lc2w=s0-d-e1-ft#http://www.enotriasa.com/ElMensajero/Clientes/Entel/20160407/recibo_back.jpg"
            style="background-repeat:no-repeat;background-position:right center">
            <table border="0" width="516" align="center" cellpadding="0" cellspacing="0">
                <tbody>
                <tr>
                    <td>
                        <img
                            src="https://ci6.googleusercontent.com/proxy/WmNxD8DqICQZBtmxuwp_tcSirFyqukJJK6Ousy5ZaYVE44ywf3qVkSr8xpqlU26unH1LHNi4HccJT_uoaKZyNcemDmyWFkEGjSnboRM4RXGC7md7kGMNBKlmLsCeQRpaW84=s0-d-e1-ft#http://www.enotriasa.com/ElMensajero/Clientes/Entel/20160407/recibo_icon.jpg"
                            alt="" class="CToWUd">
                    </td>
                    <td>
                        <p class="m_-1108311476415020897white" style="color: white"><strong>Consultas online</strong> <br>
                            Puede consultar sus pagos hechos y por hacer ingresando a <strong><a style=" color: white" target="_blank" href="http://www.onenet.com.pe/login">www.onenet.com.pe/login</a></strong>
                        </p>
                    </td>
                    <td>

                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;
        </td>

    </tr>
    <tr>
        <td>
            <table border="0" width="516" align="center" cellpadding="0" cellspacing="0">
                <tbody>

                <tr>
                    <td colspan="7" align="center"><h3 class="m_-1108311476415020897blue"> Saludos <span
                                class="m_-1108311476415020897orange">cordiales ;)</span></h3>

                        <p>Rosalina Pico Garcia <br> Gerente de Experiencia del Cliente y Operaciones <br> Canal
                            Personas </p></td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td bgcolor="#f0f0f0">
            <table border="0" width="516" align="center" cellpadding="0" cellspacing="0">
                <tbody>
                <tr>
                    <td valign="center" height="64"><p> Copyright onenet SAC, Todos los derechos reservados.</p>
                    </td>
                    <td valign="center"> Siguenos en</td>
                    <td valign="center" width="106">
                        <a href="https://twitter.com/Onenet15" target="_blank"
                                                       data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=https://www.twitter.com/EntelPeru&amp;source=gmail&amp;ust=1512704037143000&amp;usg=AFQjCNEXFz2ElJ0WPiYvqpDo3ieISYeHFg">
                            <img
                                src="https://ci6.googleusercontent.com/proxy/rvtmAYnB8zdTDII79WsH_yZ5DHfLE4jjGhYJfY4gXpA1DJ0s6zDD-UdzofnHaAgRXYL-Kju-dA3KFXBIQaGMwEwIVBxtICZANZ-gcIwh8NBd-Rx7bXbglKaxPPHilgeLJo3p=s0-d-e1-ft#http://www.enotriasa.com/ElMensajero/Clientes/Entel/20160407/twitter_icon.jpg"
                                alt="twitter" style="border:none" class="CToWUd">
                        </a>
                        <a href="https://www.facebook.com/Onenetcix" target="_blank" >
                            <img
                                src="https://ci3.googleusercontent.com/proxy/34T9nefzwMY3JjKsIZhq884FjgPi19wQGtqIFWPpmC8eFLqU5_5awIRE4N3VAnMCMxpKJr1BlYpk_Zt-36z1MMYquSJitIyqEMNGYhTBI_93nOCY2X7vgohuoe_oNA=s0-d-e1-ft#http://www.enotriasa.com/ElMensajero/Clientes/Entel/20160407/fb_icon.jpg"
                                alt="facebook" style="border:none" class="CToWUd">
                        </a>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>