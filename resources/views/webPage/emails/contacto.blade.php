<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Email de contacto</title>
</head>
<body>

    <div><strong>Nombre: </strong>{{$nombre_cliente}}</div>
    <div><strong>Correo: </strong>{{$email_cliente}}</div>
    <div><strong>Telefono: </strong>{{$numero_cliente}}</div>
    <div><strong>Mensaje: </strong>{{$mensaje_cliente}}</div>
</body>
</html>