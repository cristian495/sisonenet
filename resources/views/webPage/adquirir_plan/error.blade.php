@extends('webPage.layout.app')

@section('title', 'Cliente ya existe')



@section('content')
<div class="container">
    <div class="row">

        <div class="alert alert-danger fade in">
            <h4>Hubo un error al realizar el contrato</h4>
            <p>
                La persona / empresa con DNI / RUC ingresados anteriormente
                ya cuentan con un plan de internet.</p>
            <p>
                Puede utilizar nuestros datos de contacto para obtener mas información.
            </p>
        </div>



    </div>
</div>
{{session()->forget('error_registro')}}
@endsection