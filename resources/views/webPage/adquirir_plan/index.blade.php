@extends('webPage.layout.app')

@section('title', 'Adquirir Plan')


@section('content')
<script>
  /*  window.onunload = function(){};

    function formatTime(t) {
        return t.getHours() + ':' + t.getMinutes() + ':' + t.getSeconds();
    }

    if (window.history.state != null && window.history.state.hasOwnProperty('historic')) {
        if (window.history.state.historic == true) {
            document.body.style.display = 'none';
            //console.log('I was here before at ' + formatTime(window.history.state.last_visit));
            //window.scrollTo(0, 0);
            window.history.replaceState({historic: false}, '');
            window.location.reload();
        } else {
           // console.log('I was forced to reload, but WELCOME BACK!');
            window.history.replaceState({
                historic  : true,
                last_visit: new Date()
            }, '');
        }
    } else {
        //console.log('This is my first visit to ' + window.location.pathname);
        window.history.replaceState({
            historic  : true,
            last_visit: new Date()
        }, '');
    }*/
</script>
<link href="{{ asset('webpage/plugins/datepicker.css') }}" rel="stylesheet" type="text/css" />
<script src="{{asset('webpage/plugins/datepicker.js')}}"></script>
<script src="{{asset('webpage/plugins/validator.js')}}"></script>
<script src="{{asset('webpage/plugins/moment.js')}}"></script>


<style>
    .panel-body.simple{
        border: 1px solid rgba(48,151,209,0.7);
        border-radius: 6px;
    }

</style>

<main style="max-width: 1280px; margin:auto auto 220px auto">
    @if (count($errors)>0)
    <div class="alert alert-danger">
        <ul>
            @foreach($errors->all() as $error)
            <li>{{$error}}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <div class="contenedor_titulo_contrato">
        <h4 class="titulo_contrato">Registro de contrato</h4>
        <p class="titulo_contrato_descripcion">
            Usted esta realizando el registro de contrato por el tipo de plan
            <strong>
                @if($plan->nombre == 'economico')
                    {{'"Económico"'}}
                @elseif($plan->nombre == 'standar')
                    {{'"Stándar"'}}
                @elseif($plan->nombre == 'premium')
                    {{'"Premium"'}}
                @endif
            </strong
        </p>
    </div>


    <br/><br/>


    <div class="panel panel-primary">
        <div class="panel-heading">Reglas para el llenado de datos</div>
        <div class="panel-body">
            <ul>
                <li>
                    <span  class="text-danger">(*) </span>Datos Obligatorios.
                </li>
<!--                <li>-->
<!--                    <span  class="text-danger">(**) </span>Llenar obligatoriamente uno de ellos.-->
<!--                </li>-->
            </ul>

        </div>
    </div>


    <form action="{{url('/payment')}}" id="formulario" method="get">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">
                    1. Datos Personales&nbsp; &nbsp;
    <!--                <span class="call">(Verifique sus datos tal como figuran en su DNI)</span>-->
                </h3>
            </div>

            <div class="panel-body" >
                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                    <label for=""><b>Quién contrata el servicio</b><span class="obligatorio">*</span></label>
                    <div class="form-group">
                            <label class="radio-inline">
                                <input checked required  {{ old('tipo_persona')=="persona" ? 'checked='.'"'.'checked'.'"' : '' }}
                                name="tipo_persona" type="radio" id="persona" value="persona" data-error="Seleccione quien contrata el servicio">
                                Persona
                            </label>
                        <label class="radio-inline" >
                            <input required="" {{ old('tipo_persona')=="empresa" ? 'checked='.'"'.'checked'.'"' : '' }}
                                   name="tipo_persona" type="radio" id="empresa" value="empresa" >
                            Empresa
                        </label>
                        <div class="help-block with-errors"></div>
                    </div>

                </div>
                <div id="panel_datos_personales">

                    @if(old('tipo_persona')=='persona' or !old('tipo_persona'))
                    <div class="col-xs-12" id="panel_persona">
                        <div class="panel panel-default-border">
                            <div class="panel-body simple">
                                <div class="form-group">
                                    <input type="hidden"  name="quien" value="persona"/>
                                    <label  class="control-label">Apellido Paterno<span class="text-danger">&nbsp;(*)</span></label>
                                    <input required type="text" value="{{old('apellido_paterno')}}" class="form-control"  name="apellido_paterno" data-error="Escriba su apellido paterno">
                                    <div class="help-block with-errors"></div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label">Apellido Materno<span class="text-danger">&nbsp;(*)</span></label>
                                    <input required type="text" value="{{old('apellido_materno')}}" class="form-control" name="apellido_materno"  data-error="Escriba su apellido materno">
                                    <div class="help-block with-errors"></div>
                                </div>

                                <div class="form-group">
                                    <label  class="control-label">Nombres <span class="text-danger">&nbsp;(*)</span></label>
                                    <input  required type="text" value="{{old('nombres')}}" class="form-control" name="nombres"  data-error="Escriba sus Nombres">
                                    <div class="help-block with-errors"></div>
                                </div>

                                <div class="form-group" >
                                    <label class="control-label" for="DNI">DNI <span class="text-danger">&nbsp;(*)</span></label>
                                    <input required type="text" value="{{old('dni')}}" class="form-control" name="dni" placeholder="Escriba su DNI" data-error="Escriba su DNI"\>
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Sexo <span class="text-danger">&nbsp;(*)</span></label>
                                    <select required class="form-control" value="{{old('sexo')}}" id="sexo" name="sexo" data-error="Seleccione su sexo">
                                        <option selected value="">Seleccione...</option>
                                        <option value="F">FEMENINO</option>
                                        <option value="M">MASCULINO</option>
                                    </select>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @elseif(old('tipo_persona')=='empresa')
                    <div class="col-xs-12" id="panel_empresa">
                        <div class="panel panel-default-border">
                        <div class="panel-body simple">
                        <div class="form-group">
                            <input type="hidden"  name="quien" value='empresa'/>
                            <label class="control-label" for="RUC">Razon social <span class="text-danger">&nbsp;(*)</span></label>
                            <input required type="text" value="{{old('razon_social')}}"  name="razon_social" class="form-control" maxlength="200"   placeholder="Razon social RUC" data-error="Escriba la razon social de su empresa" >
                            <div class="help-block with-errors"></div>
                        </div>
                    <div class="form-group">
                        <label class="control-label">RUC<span class="text-danger">&nbsp;(*)</span></label>
                        <input required name="ruc" type="text" value="{{old('ruc')}}"  class="form-control" maxlength="11" placeholder="Escrbiba su RUC"  data-error="Escriba su RUC">
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
            </div>
        </div>

                    @endif
                <div class="col-xs-12">
                    <div class="panel panel-default-border">
                        <div class="panel-body simple">
                            <div class="col-xs-3">
                                <div class="form-group">
                                    <label class="control-label">Teléfono / Celular<span class="text-danger">(*)</span></label>
                                    <input required type="text" value="{{old('telefono')}}" id="telefono" class="form-control " maxlength="10"   name="telefono" placeholder="Introduce Teléfono" data-error="Ingrese su numero de telefono o celular">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="form-group">
                                    <label class="control-label">Teléfono / Celular adicional</label>
                                    <input  type="text" value="{{old('telefono_adicional')}}" class="form-control " maxlength="10"   name="telefono_adicional" placeholder="Introduce Teléfono adicional" >
                                </div>
                            </div>
<!--                            <div class="col-xs-3">-->
<!--                                <div class="form-group ">-->
<!--                                    <label class="control-label">Oper. Celular <span class="text-danger">(*)</span></label>-->
<!--                                    <select required class="form-control"  name="operador_telefono" data-error="Seleccione el operador de su telefono o celular">-->
<!--                                        <option selected="selected" value="">Seleccione...</option>-->
<!--                                        <option value="MOVISTAR">MOVISTAR</option>-->
<!--                                        <option value="CLARO">CLARO</option>-->
<!--                                        <option value="RPM MOVISTAR">RPM MOVISTAR</option>-->
<!--                                        <option value="RPC CLARO">RPC CLARO</option>-->
<!--                                        <option value="NEXTEL">NEXTEL</option>-->
<!--                                        <option value="ENTEL">ENTEL</option>-->
<!--                                        <option value="BITEL">BITEL</option>-->
<!--                                        <option value="OTROS">OTROS</option>-->
<!--                                    </select>-->
<!--                                    <div class="help-block with-errors"></div>-->
<!--                                </div>-->
<!--                            </div>-->
                            <div class="col-xs-3 ">
                                <div class="form-group">
                                    <label class="control-label"> Correo Electrónico
                                        <span class="text-danger">(*)</span>
                                    </label>
                                    <input  required type="email" value="{{old('email')}}" class="form-control"  name="email" maxlength="80" placeholder="Introduce Correo Electrónico" data-error="Ingrese un correo electronico valido">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        </div>


        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">
                    2. Datos de Residencia&nbsp; &nbsp;
                    <span class="call">(Residencia para donde se contratara el servicio de internet)</span>
                </h3>
            </div>

            <div class="panel-body" >

                <div class="col-xs-12">
                    <div class="panel panel-default-border">
                        <div class="panel-body simple">
                            <div class="col-xs-3">
                                <div class="form-group ">
                                    <label class="control-label">Tipo de Vía<span class="text-danger">&nbsp;(*)</span></label>
                                    <select required class="form-control" name="tipo_via" data-error="Ingrese un tipo de via">
                                        <option value="">Seleccione...</option>
                                        <option value="alameda">ALAMEDA</option>
                                        <option value="avenida">AVENIDA</option>
                                        <option value="bajada">BAJADA</option>
                                        <option value="calle">CALLE</option>
                                        <option value="camino_afirmado">CAMINO AFIRMADO</option>
                                        <option value="camino">CAMINO RURAL</option>
                                        <option value="carretera">CARRETERA</option>
                                        <option value="galeria">GALERIA</option>
                                        <option value="jiron">JIRÓN</option>
                                        <option value="malecon">MALECÓN</option>
                                        <option value="otros">OTROS</option>
                                        <option value="ovalo">OVALO</option>
                                        <option value="parque">PARQUE</option>
                                        <option value="pasaje">PASAJE</option>
                                        <option value="paseo">PASEO</option>
                                        <option value="plaza">PLAZA</option>
                                        <option value="plazuela">PLAZUELA</option>
                                        <option value="portal">PORTAL</option>
                                        <option value="prolongacion">PROLONGACIÓN</option>
                                        <option value="trocha">TROCHA</option>
                                        <option value="troch_carrozable">TROCHA CARROZABLE</option>
                                    </select >
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="form-group delpmbottom">
                                    <label class="control-label">Nombre de Vía (Dirección)<span class="text-danger">&nbsp;(*)</span></label>
                                    <input required value="{{old('nombre_via')}}" type="text" name="nombre_via" class="form-control" maxlength="80" placeholder="Introduce Nombre de Vía" data-error="Ingrese un nombre de via">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
<!--                            <div class="col-xs-2">-->
<!--                                <div class="form-group delpmbottom ">-->
<!--                                    <label class="control-label"> N° / km / mz</label>-->
<!--                                    <span class="text-danger">&nbsp;(*)</span>-->
<!--                                    <input required value="{{old('numero_via')}}" type="text" class="form-control"  name="numero_via"-->
<!--                                           maxlength="8"  placeholder="Ingrese N°" data-error="Ingrese un km o n° o mz">-->
<!--                                    <div class="help-block with-errors"></div>-->
<!--                                </div>-->
<!--                            </div>-->
                        </div>
                    </div>


                    <div class="panel panel-default-border ">
                        <div class="panel-body simple">
<!--                            <div class="col-xs-2">-->
<!--                                <div class="form-group ">-->
<!--                                    <label>Interior <span class="text-danger">&nbsp;(**)</span></label>-->
<!--                                    <input  type="text" class="form-control" id="numero01" name="txtNumInterior" value="" maxlength="8"  placeholder="Introduce Interior">-->
<!--                                </div>-->
<!--                            </div>-->
                            <div class="col-xs-3">
                                <div class="form-group ">
                                    <label class="control-label">NRO. Vivienda<span class="text-danger">&nbsp;(*)</span> </label>
                                    <input required type="text" value="{{old('numero_vivienda')}}" name="numero_vivienda" class="form-control" maxlength="8"
                                           data-error="Ingrese su nro de vivienda" placeholder="Ingrese Nro de Vivienda">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="form-group">
                                    <label class="control-label">Tipo de Zona<span class="text-danger">&nbsp;(*)</span></label>
                                    <select required name="tipo_zona" value="{{old('tipo_zona')}}" class="form-control" data-error="Seleccione tipo de zona">
                                        <option value="">Seleccione...</option>
                                        <option value="asentamiento humano">ASENTAMIENTO HUMANO </option>
                                        <option value="caserio">CASERÍO </option>
                                        <option value="conjunto habitacional">CONJUNTO HABITACIONAL</option>
                                        <option value="cooperativa">COOPERATIVA</option>
                                        <option value="fundo">FUNDO</option>
                                        <option value="grupo">GRUPO </option >
                                        <option value="otros">OTROS</option>
                                        <option value="pueblo joven">PUEBLO JOVEN </option>
                                        <option value="residencial">RESIDENCIAL  </option>
                                        <option value="unidad vecinal">UNIDAD VECINAL  </option>
                                        <option value="urbanizacion">URBANIZACIÓN</option>
                                        <option value="zona industrial">ZONA INDUSTRIAL </option>
                                    </select>

                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-xs-2">
                                <div class="form-group delpmbottom">
                                    <label class="control-label">Nombre Zona<span class="text-danger">&nbsp;(*)</span></label>
                                    <input required value="{{old('nombre_zona')}}" name="nombre_zona" type="text" class="form-control"
                                           maxlength="80" placeholder="Nombre de zona" data-error="Ingrese un nombre de zona">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default-border delpmbottom">
                        <div class="panel-body simple">
                            <div class="col-xs-4">
                                <div class="form-group">
                                    <label class="control-label">Departamento Residencia<span class="text-danger">&nbsp;(*)</span></label>
                                    <select required name="departamento" class="form-control" data-error="Seleccione departamento">
                                        <option id="valor_defecto" value="">Seleccione...</option>
                                        <option value="lambayeque">LAMBAYEQUE</option>
                                        <!--<option value="01">01 AMAZONAS</option>
                                        <option value="02">02 ANCASH</option>
                                        <option value="03">03 APURIMAC</option>
                                        <option value="04">04 AREQUIPA</option>
                                        <option value="05">05 AYACUCHO</option>
                                        <option value="06">06 CAJAMARCA</option>
                                        <option value="07">07 CALLAO</option>
                                        <option value="08">08 CUSCO</option>
                                        <option value="09">09 HUANCAVELICA</option>
                                        <option value="10">10 HUANUCO</option>
                                        <option value="11">11 ICA</option>
                                        <option value="12">12 JUNIN</option>
                                        <option value="13">13 LA LIBERTAD</option>

                                        <option value="15">15 LIMA</option>
                                        <option value="16">16 LORETO</option>
                                        <option value="17">17 MADRE DE DIOS</option>
                                        <option value="18">18 MOQUEGUA</option>
                                        <option value="19">19 PASCO</option>
                                        <option value="20">20 PIURA</option>
                                        <option value="21">21 PUNO</option>
                                        <option value="22">22 SAN MARTIN</option>
                                        <option value="23">23 TACNA</option>
                                        <option value="24">24 TUMBES</option>
                                        <option value="25">25 UCAYALI</option>-->
                                    </select>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div class="form-group delpmbottom">
                                    <label class="control-label"> Provincia Residencia<span class="text-danger">&nbsp;(*)</span></label>
                                    <select required name="provincia" id="provincia" class="form-control" data-error="Seleccione provincia">
                                        <option id="" value="">Seleccione...</option>
                                        <option value="chiclayo">CHICLAYO</option>
                                        <option value="ferreñafe">FERREÑAFE</option>
                                        <option value="lambayeque">LAMBAYEQUE</option>
                                    </select>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div class="form-group delpmbottom">
                                    <label class="control-label">Distrito Residencia<span class="text-danger">&nbsp;(*)</span></label>
                                    <div id="dist2">
                                        <select required class="form-control" id="distrito" name="distrito" data-error="Seleccione distrito">
                                            <option value="">Seleccione...</option>
                                        </select>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <div class="panel panel-default-border">
                    <div class="panel-body simple">
                        <div class="col-xs-3">
                            <div class="form-group">
                                <label class="control-label">Referencia de vivienda<span class="text-danger">(*)</span></label>
                                <input  type="text" value="{{old('referencia')}}" class="form-control " maxlength="100"   name="referencia" placeholder="Ingrese referencia de vivienda" >
                            </div>
                        </div>
                    </div>
                </div>

                </div>
            </div>
        </div>


        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">3. Datos del Plan solicitado</span></h3>
            </div>

            <div class="panel-body" >

                <div class="col-xs-12">
                    <div class="panel panel-default-border">
                        <div class="panel-body simple">
                            <div class="col-xs-3">
                                <div class="form-group delpmbottom">
                                    <label class="control-label">Nombre del plan<span class="text-danger">&nbsp;(*)</span></label>
                                    <input required readonly name="nombre_plan" type="text" class="form-control"  value="{{$plan->nombre}}" data-error="Ingrese nombre del plan">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="form-group ">
                                    <label class="">Precio mensual<span class="text-danger">&nbsp;(*)</span></label>
                                    <input  readonly name="precio_mensual" type="text" id="costo_paquete" class="form-control"  value="{{$plan->precio_mensual}}"  >
                                </div>
                            </div>



                            <div class="col-xs-3">
                                <div class="form-group delpmbottom ">
                                    <label class="control-label"> Megas de descarga</label>
                                    <span class="text-danger">&nbsp;(*)</span>
                                    <input required type="number" readonly class="form-control" value="{{$plan->megas_bajada}}" data-error="Ingrse megas de descarga del plan" >
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="form-group delpmbottom ">
                                    <label class="control-label"> Megas de subida</label>
                                    <span class="text-danger">&nbsp;(*)</span>
                                    <input required type="number" name="megas" readonly class="form-control" value="{{$plan->megas_subida}}" data-error="Ingrese megas de subida del plan" >
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>



                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">4. Datos del servicio</span></h3>
            </div>

            <div class="panel-body" >

                <div class="col-xs-12">
                    <div class="panel panel-default-border">
                        <div class="panel-body simple">
                            <div class="col-xs-3">
                                <div class="form-group delpmbottom">
                                    <label class="control-label">Inicio del contrato (dd/mm/aaaa)<span class="text-danger">&nbsp;(*)</span></label>
                                    <input  required name="inicio_del_contrato" value="inicio_del_contrato" type="text" class="form-control" id="iniciocontrato"  placeholder="dd-mm-aaaa"
                                            data-error="Ingrese fecha de inicio del contrato">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="form-group delpmbottom">
                                    <label class="control-label">Duración del contrato<span class="text-danger">&nbsp;(*)</span></label>
                                    <select required name="duracion_del_contrato" id="duracion_contrato" class="form-control" data-error="Seleccione duracion del contrato">
                                        <option value="">Seleccione ...</option>
                                        <option value="1">1 año</option>
                                        <option value="2">2 años</option>
                                        <option value="3">3 años</option>
                                    </select>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-xs-2">
                                <div class="form-group delpmbottom">
                                    <label class="control-label">Fin del contrato<span class="text-danger">&nbsp;(*)</span></label>
                                    <input  required readonly name="fin_del_contrato" type="text" class="form-control" id="fincontrato"  placeholder="dd-mm-aaaa"
                                            data-error="Fin de contrato inválido">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-xs-2">
                                <div class="form-group delpmbottom">
                                    <label class="control-label">Fecha de pago<span class="text-danger">&nbsp;(*)</span></label>
                                    <input  required readonly name="fecha_de_pago" type="text" class="form-control" id="fechapago"  placeholder="dd-mm-aaaa"
                                            data-error="Fecha de pago inválido">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-xs-2">
                                <div class="form-group delpmbottom">
                                    <label class="control-label">Fecha de corte<span class="text-danger">&nbsp;(*)</span></label>
                                    <input  required readonly name="fecha_de_corte" type="text" class="form-control" id="fechacorte"  placeholder="dd-mm-aaaa"
                                            data-error="Fecha de corte inválido">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default-border">
                        <div class="panel-body simple">



                            <div class="col-xs-3">
                                <div class="form-group delpmbottom ">
                                    <label> Costo de Instalación</label>
                                    <span class="text-danger">&nbsp;(*)</span>
                                    <input type="text" id="costo_instalacion" readonly class="form-control" value="1.00 sol" >
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="form-group">
                                    <label>Costo del Access Point</label>
                                    <span class="text-danger">&nbsp;(*)</span>
                                    <input type="text" id="costo_ap" readonly class="form-control" value="300.00 soles" >
                                </div>
                            </div>


                            <div class="col-lg-4 col-sm-4 col-md-8 col-xs-12">
                                <label for=""><b>Tipo de pago del Access Point</b><span class="obligatorio">*</span></label>
                                <div class="form-group">
                                    <label class="radio-inline">
                                        <input  required  name="tipo_pago_ap"
                                                type="radio" id="mensual" value="mensual" data-error="Seleccione una de estas opciones" > Mensualmente
                                    </label>
                                    <label class="radio-inline">
                                        <input required type="radio" name="tipo_pago_ap"
                                               id="contado" value="contado"  data-error="Seleccione una de estas opciones"> Al contado
                                    </label>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">5. Detalle de costos</span></h3>
            </div>

            <div class="panel-body" >

                <div class="col-xs-12">
                    <div class="panel panel-default-border">
                        <div id="ayuda">
                            <h4>
                                Para visualizar el detalle de costos verifique que esten llenados correctamente los siguientes campos:
                            </h4>
                            <ul style="list-style: circle; margin-left: 17px">
                                <li>Precio mensual del paquete</li>
                                <li>Duración del contrato</li>
                                <li>Costo de instalación</li>
                                <li>Costo del Access Point</li>
                                <li>Tipo de pago del Access point</li>
                            </ul>

                        </div>
                        <div id="detalle_costos" style="display: none">
                            <!-- PANEL INTERNO PRIMERA MENSUALIDAD-->
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Primera Mensualidad</span></h3>
                                </div>
                                <div class="panel-body" >
                                    <div class="col-xs-12">
                                        <div class="panel panel-default-border">
                                            <div class="panel-body simple">

                                                <div class="col-xs-2">

                                                    <div class="form-group ">
                                                        <label>Plan<span class="text-danger">&nbsp;(*)</span></label>
                                                        <div class="input-group">
                                                            <span class="input-group-addon" >S/.</span>
                                                            <input readonly type="text" id="resul_plan" class="form-control" >
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-1">
                                                    <div class="form-group ">
                                                        <span class="fa fa-plus" style="text-align: center;display: block;margin-top: 38px;"></span>
                                                    </div>
                                                </div>
                                                <div class="col-xs-2">
                                                    <div class="form-group ">
                                                        <label>Instalación<span class="text-danger">&nbsp;(*)</span></label>
                                                        <div class="input-group">
                                                            <span class="input-group-addon" >S/.</span>
                                                            <input readonly type="text" id="resul_instalacion" class="form-control" >
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-1">
                                                    <div class="form-group ">
                                                        <span class="fa fa-plus" style="text-align: center;display: block;margin-top: 38px;"></span>
                                                    </div>
                                                </div>
                                                <div class="col-xs-2">
                                                    <div class="form-group ">
                                                        <label>Access Point<span class="text-danger">&nbsp;(*)</span></label>
                                                        <div class="input-group">
                                                            <span class="input-group-addon" >S/.</span>
                                                            <input readonly type="text" id="resul_ap" class="form-control" >
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-1">
                                                    <div class="form-group ">
                                                    <span class="" style="font-weight: bold;font-size: 21px;text-align: center;display: block;margin-top: 28px;">
                                                        =
                                                    </span>
                                                    </div>
                                                </div>
                                                <div class="col-xs-2">
                                                    <div class="form-group ">
                                                        <label>Total<span class="text-danger">&nbsp;(*)</span></label>
                                                        <div class="input-group">
                                                            <span class="input-group-addon" >S/.</span>
                                                            <input readonly type="text" id="resul_total" class="form-control" >
                                                        </div>
                                                    </div>
                                                </div>


<!--                                                <div class="col-xs-12">-->
<!--                                                    <h4>Dolares</h4>-->
<!--                                                </div>-->
<!---->
<!--                                                <div class="col-xs-2">-->
<!--                                                    <div class="form-group ">-->
<!--                                                        <label>Plan<span class="text-danger">&nbsp;(*)</span></label>-->
<!--                                                        <input   type="text" readonly class="form-control" id="resul_plan" >-->
<!--                                                    </div>-->
<!--                                                </div>-->
<!--                                                <div class="col-xs-1">-->
<!--                                                    <div class="form-group ">-->
<!--                                                        <span class="fa fa-plus" style="text-align: center;display: block;margin-top: 38px;"></span>-->
<!--                                                    </div>-->
<!--                                                </div>-->
<!--                                                <div class="col-xs-2">-->
<!--                                                    <div class="form-group ">-->
<!--                                                        <label>Instalación<span class="text-danger">&nbsp;(*)</span></label>-->
<!--                                                        <input   type="text" readonly class="form-control" id="resul_instalacion" >-->
<!--                                                    </div>-->
<!--                                                </div>-->
<!--                                                <div class="col-xs-1">-->
<!--                                                    <div class="form-group ">-->
<!--                                                        <span class="fa fa-plus" style="text-align: center;display: block;margin-top: 38px;"></span>-->
<!--                                                    </div>-->
<!--                                                </div>-->
<!--                                                <div class="col-xs-2">-->
<!--                                                    <div class="form-group ">-->
<!--                                                        <label>Access Point<span class="text-danger">&nbsp;(*)</span></label>-->
<!--                                                        <input   type="text" readonly class="form-control" id="resul_ap" >-->
<!--                                                    </div>-->
<!--                                                </div>-->
<!--                                                <div class="col-xs-1">-->
<!--                                                    <div class="form-group ">-->
<!--                                                    <span class="" style="font-weight: bold;font-size: 21px;text-align: center;display: block;margin-top: 28px;">-->
<!--                                                        =-->
<!--                                                    </span>-->
<!--                                                    </div>-->
<!--                                                </div>-->
<!--                                                <div class="col-xs-2">-->
<!--                                                    <div class="form-group ">-->
<!--                                                        <label>Total<span class="text-danger">&nbsp;(*)</span></label>-->
<!--                                                        <input   type="text" readonly class="form-control" id="resul_total" >-->
<!--                                                    </div>-->
<!--                                                </div>-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- FIN PRIMERA PENSUALIDAD-->

                            <!-- PANEL INTERNO DEMAS MENSUALIDADES -->
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Demás mensualidades</span></h3>
                                </div>
                                <div class="panel-body" >
                                    <div class="col-xs-12">
                                        <div class="panel panel-default-border">
                                            <div class="panel-body simple">
                                                <div class="col-xs-2">
                                                    <div class="form-group ">
                                                        <label>Plan<span class="text-danger">&nbsp;(*)</span></label>
                                                        <div class="input-group">
                                                            <span class="input-group-addon" >S/.</span>
                                                            <input readonly type="text" id="resul_demas_meses_plan" class="form-control" >
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-1">
                                                    <div class="form-group ">
                                                        <span class="fa fa-plus" style="text-align: center;display: block;margin-top: 38px;"></span>
                                                    </div>
                                                </div>
                                                <div class="col-xs-2">
                                                    <div class="form-group ">
                                                        <label>Access Point<span class="text-danger">&nbsp;(*)</span></label>
                                                        <div class="input-group">
                                                            <span class="input-group-addon" >S/.</span>
                                                            <input readonly type="text" id="resul_demas_meses_ap" class="form-control" >
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-1">
                                                    <div class="form-group ">
                                                    <span class="" style="font-weight: bold;font-size: 21px;text-align: center;display: block;margin-top: 28px;">
                                                        =
                                                    </span>
                                                    </div>
                                                </div>
                                                <div class="col-xs-2">
                                                    <div class="form-group ">
                                                        <label>Total<span class="text-danger">&nbsp;(*)</span></label>
                                                        <div class="input-group">
                                                            <span class="input-group-addon" >S/.</span>
                                                            <input readonly type="text" id="resul_demas_meses_total" class="form-control" >
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- FIN DEMAS MENSUALIDADES-->
                            <div class="col-lg-4 col-sm-4 col-md-8 col-xs-12">
                                <label for=""><b>Tipo de comprobante</b><span class="obligatorio">*</span></label>
                                <div class="form-group">
                                    <label class="radio-inline">
                                        <input  required  name="tipo_comprobante"
                                                type="radio" value="boleta" data-error="Seleccione un tipo de comprobante" > Boleta
                                    </label>
                                    <label class="radio-inline">
                                        <input required type="radio" name="tipo_comprobante"
                                                value="factura"  data-error="Seleccione un tipo de comprobante"> Factura
                                    </label>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<!--        <input type="submit" src="https://www.paypalobjects.com/en_US/i/btn/btn_buynow_LG.gif" value="Realizar contrato" class="btn btn-default"/>-->
        <div class="row" style="display: flex">
            <button class="button
                          button--antiman
                          button--text-thick
                          button--text-upper
                          button--size-l
                          button--inverted-alt
                          button--round-s
                          button--border-thick">
                <i class="button__icon icon icon-plus"></i>
                <span>Realizar contrato <i class="fa fa-paypal" ></i></span>
            </button>
        </div>
    </form>
</main>

@endsection



@section('scripts')
<script>
//$('#txtTelefono'),$('#txtCelular')
$('#formulario').validator({
    focus: true,
    custom: {
            equals: function($el) {
                var value = $el.data('min_uno');
                return false;
               /* var numero01	= $('#numero01').val() != '';
                var numero03	= $('#numero02').val() != '';
                var numero02	= $('#numero03').val() != '';

                var valResult = numero01 || numero02 || numero03;
                console.log('valResult = '+valResult);
                return valResult;*/
            }
        },
        errors: {
            'equals': "at least one fillled ErrMsg: At least one field should be filled"
        }

    }).on('submit',function(e){

    });

    $(document).on('change','#persona,#empresa',function(){
        var p=$('#persona'),
            e=$('#empresa'),
            panelpersona = $('#panel_persona'),
            panelempresa = $('#panel_empresa'),
            panelDatosPersonales = $('#panel_datos_personales'),contPersona,contEmpresa;

        contPersona = "<div class=\"col-xs-12\" id=\"panel_persona\">";
        contPersona +=  "<div class=\"panel panel-default-border\">";
        contPersona +=  "<div class=\"panel-body simple\">";
        contPersona +=  "<div class=\"form-group\">";
        contPersona +=  "<input type=\"hidden\"  name=\"quien\" value='persona'/>";
        contPersona +=  "<label class=\"control-label\">Apellido Paterno<span class=\"text-danger\">&nbsp;(*)</span></label>";
        contPersona +=  "<input required value=\"{{old('apellido_paterno')}}\" type=\"text\"  class=\"form-control\"  id=\"txtApePaterno\" name=\"apellido_paterno\" data-error=\"Escriba su apellido paterno\">";
        contPersona +=  "<div class=\"help-block with-errors\"></div>";
        contPersona +=  "</div>";
        contPersona +=  "<div class=\"form-group\">";
        contPersona +=  "<label class=\"control-label\">Apellido Materno<span class=\"text-danger\">&nbsp;(*)</span></label>";
        contPersona +=  "<input required type=\"text\" value=\"{{old('apellido_materno')}}\" class=\"form-control\" id=\"txtApeMaterno\" name=\"apellido_materno\"  data-error=\"Escriba su apellido materno\">";
        contPersona +=  "<div class=\"help-block with-errors\"></div>";
        contPersona +=  "</div>";
        contPersona +=  "<div class=\"form-group\">";
        contPersona +=  "<label class=\"control-label\">Nombres <span class=\"text-danger\">&nbsp;(*)</span></label>";
        contPersona +=  "<input required  type=\"text\" value=\"{{old('nombres')}}\"  class=\"form-control\" id=\"txtNombre1\" name=\"nombres\"  data-error=\"Escriba sus nombres\">";
        contPersona +=  "<div class=\"help-block with-errors\"></div>";
        contPersona +=  "</div>";
        contPersona +=  "<div class=\"form-group\">";
        contPersona +=  "<label for=\"DNI\" class=\"control-label\">DNI <span class=\"text-danger\">&nbsp;(*)</span></label>";
        contPersona +=  "<input required type=\"text\" value=\"{{old('dni')}}\"  class=\"form-control\"id=\"txtDNI\" name=\"dni\" placeholder=\"Introduce DNI\"\  data-error=\"Escriba su DNI\">";
        contPersona +=  "<div class=\"help-block with-errors\"></div>";
        contPersona +=  "</div>";
        contPersona +=  "<div class=\"form-group\">";
        contPersona +=  "<label class=\"control-label\">Sexo <span class=\"text-danger\">&nbsp;(*)</span></label>";
        contPersona +=  "<select required class=\"form-control\" id=\"cmbSexo\" name=\"sexo\"  data-error=\"Seleccione su sexo\">";
        contPersona +=  "<option value=\"\">Seleccione</option>";
        contPersona +=  "<option value=\"F\">FEMENINO</option>";
        contPersona +=  "<option value=\"M\">MASCULINO</option>";
        contPersona +=  "</select>";
        contPersona +=  "<div class=\"help-block with-errors\"></div>";
        contPersona +=  "</div>";
        contPersona +=  "</div>";
        contPersona +=  "</div>";


        contEmpresa = "<div class=\"col-xs-12\" id=\"panel_empresa\">";
        contEmpresa += "<div class=\"panel panel-default-border\">";
        contEmpresa += "<div class=\"panel-body simple\">";
        contEmpresa += "<div class=\"form-group\">";
        contEmpresa +=  "<input type=\"hidden\"  name=\"quien\" value='empresa'/>";
        contEmpresa += "<label class=\"control-label\" for=\"RUC\">Razon social <span class=\"text-danger\">&nbsp;(*)</span></label>";
        contEmpresa += "<input required type=\"text\" value=\"{{old('razon_social')}}\"  name=\"razon_social\" class=\"form-control\" maxlength=\"501\"   placeholder=\"Razon social RUC\" data-error=\"Escriba la razon social de su empresa\" >";
        contEmpresa += "<div class=\"help-block with-errors\"></div>";
        contEmpresa += "</div>";
        contEmpresa += "<div class=\"form-group\">";
        contEmpresa += "<label class=\"control-label\">RUC<span class=\"text-danger\">&nbsp;(*)</span></label>";
        contEmpresa += "<input required name=\"ruc\" type=\"text\" value=\"{{old('ruc')}}\"  class=\"form-control\" maxlength=\"11\" placeholder=\"Escrbiba su RUC\"  data-error=\"Escriba su RUC\">";
        contEmpresa += "<div class=\"help-block with-errors\"></div>";
        contEmpresa += "</div>";
        contEmpresa += "</div>";
        contEmpresa += "</div>";
        contEmpresa += "</div>";



        if(p.is(':checked'))
        {
            if(panelempresa){
                panelempresa.remove();
            }
            panelDatosPersonales.prepend(contPersona);
            $('#formulario').validator('update')
        }
        else if (e.is(':checked'))
        {
            if(panelpersona){
                panelpersona.remove();
            }
            panelDatosPersonales.prepend(contEmpresa);
            $('#formulario').validator('update')
        }

    });

    (function (){
        $.fn.datepicker.dates['es'] = {
            days: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado", "Domingo"],
            daysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab", "Dom"],
            daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa", "Do"],
            months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Setiembre", "Octubre", "Noviembre", "Diciembre"],
            monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Set", "Oct", "Nov", "Dic"],
            today: "Hoy"
        };

        var date = new Date();
        date.setDate(date.getDate());

        var inicioContrato =$('#iniciocontrato');
        inicioContrato.datepicker({
            format: 'dd/mm/yyyy',
            autoclose:true,
            language: 'es',
            startDate: date,
            endDate: '+7d'
        }).datepicker("setDate",'now');

    })();




    $(document).on('change','#provincia',function(){
        var distChiclayo = "",
            distFerreñafe = "",
            distLambayeque = "",
            provincia = $(this),
            distrito = $('#distrito');


        distChiclayo  = "<option value=\"0\">Seleccione</option>";
        distChiclayo += "<option value=\"chiclayo\">CHICLAYO</option>";
        distChiclayo += "<option value=\"chongoyape\">CHONGOYAPE</option>";
        distChiclayo += "<option value=\"140103ETEN\">ETEN</option>";
        distChiclayo += "<option value=\"eten_puerto\">ETEN PUERTO</option>";
        distChiclayo += "<option value=\"jose_leonardo_ortiz\">JOSE LEONARDO ORTIZ</option>";
        distChiclayo += "<option value=\"la_victoria\">LA VICTORIA</option>";
        distChiclayo += "<option value=\"lagunas\">LAGUNAS</option>";
        distChiclayo += "<option value=\"monsefu\">MONSEFU</option>";
        distChiclayo += "<option value=\"nueva_arica\">NUEVA ARICA</option>";
        distChiclayo += "<option value=\"oyotun\">OYOTUN</option>";
        distChiclayo += "<option value=\"picsi\">PICSI</option>";
        distChiclayo += "<option value=\"pimentel\">PIMENTEL</option>";
        distChiclayo += "<option value=\"reque\">REQUE</option>";
        distChiclayo += "<option value=\"santa_rosa\">SANTA ROSA</option>";
        distChiclayo += "<option value=\"saña\">SAÑA</option>";
        distChiclayo += "<option value=\"cayalti\">CAYALTI</option>";
        distChiclayo += "<option value=\"patapo\">PATAPO</option>";
        distChiclayo += "<option value=\"pomalca\">POMALCA</option>";
        distChiclayo += "<option value=\"pucala\">PUCALA</option>";
        distChiclayo += "<option value=\"tuman\">TUMAN</option>";

        distFerreñafe  = "<option value=\"0\">Seleccione</option>";
        distFerreñafe += "<option value=\"ferreñafe\">FERRÑAFE</option>";
        distFerreñafe += "<option value=\"cañaris\">CAÑARIS</option>";
        distFerreñafe += "<option value=\"incahuasi\">INCAHUASI</option>";
        distFerreñafe += "<option value=\"manuel_a._mesones_muro\">MANUEL A. MESONES MURO</option>";
        distFerreñafe += "<option value=\"pitipo\">PITIPO</option>";
        distFerreñafe += "<option value=\"pueblo_nuevo\">PUEBLO NUEVO</option>";

        distLambayeque = "<option value=\"0\">Seleccione</option>";
        distLambayeque += "<option value=\"lambayeque\">LAMBAYEQUE</option>";
        distLambayeque += "<option value=\"chochope\">CHOCHOPE</option>";
        distLambayeque += "<option value=\"illimo\">ILLIMO</option>";
        distLambayeque += "<option value=\"jayanca\">JAYANCA</option>";
        distLambayeque += "<option value=\"mochumi\">MOCHUMI</option>";
        distLambayeque += "<option value=\"morrope\">MORROPE</option>";
        distLambayeque += "<option value=\"motupe\">MOTUPE</option>";
        distLambayeque += "<option value=\"olmos\">OLMOS</option>";
        distLambayeque += "<option value=\"pacora\">PACORA</option>";
        distLambayeque += "<option value=\"salas\">SALAS</option>";
        distLambayeque += "<option value=\"san_jose\">SAN JOSE</option>";
        distLambayeque += "<option value=\"tucume\">TUCUME</option>";


        if(provincia.val() === "chiclayo"){
            distrito.html(distChiclayo);
        }else if(provincia.val() === "lambayeque"){
            distrito.html(distLambayeque);
        }else if(provincia.val() === "ferreñafe"){
            distrito.html(distFerreñafe);
        }else{
            distrito.html('<option>Seleccione</option>');
        }

    })

    /*
    * DETALLE DE COSTOS
    * */
    function porcentaje(value,porcentaje)
    {
        return parseFloat(value)*(parseFloat(porcentaje)/100);
    }

     function calcularCostos (){
        var costo_paquete = parseFloat($('#costo_paquete').val()),
            costo_ap = parseFloat($('#costo_ap').val()),
            instalacion = parseFloat($('#costo_instalacion').val()),
            mensual=$('#mensual'),
            contado=$('#contado'),
            duracion_contrato= $('#duracion_contrato').val(),
            costoPrimerMes = 0.00,
            apmensual=0.00,
            demasMeses= 0.00,
            detalle_costos= $('#detalle_costos');


        if(duracion_contrato != '' && costo_paquete != '' && costo_ap!= '' && instalacion != '' && (mensual.is(':checked') || contado.is(':checked')))
        {
            if(contado.is(':checked'))
            {
                costoPrimerMes = costo_paquete + costo_ap + instalacion;
                demasMeses = costo_paquete;

                $('#ayuda').css("display","none");
                $('#resul_plan').val(costo_paquete.toFixed(1));
                $('#resul_instalacion').val(instalacion.toFixed(1));
                $('#resul_ap').val(costo_ap.toFixed(1));
                $('#resul_total').val(costoPrimerMes.toFixed(1));
                $('#resul_demas_meses_plan').val(costo_paquete.toFixed(1));
                $('#resul_demas_meses_ap').val('0.00');
                $('#resul_demas_meses_total').val(costo_paquete.toFixed(1));


            }
            else if (mensual.is(':checked'))
            {
                var cantmeses,
                    inicioContrato = $('#iniciocontrato').val(),
                    finContrato = $('#fincontrato').val(),
                    startDate = moment(inicioContrato, "DD/MM/YYYY"),
                    endDate = moment(finContrato, "DD/MM/YYYY").endOf("month"),
                    allMonthsInPeriod = [];

                while (startDate.isBefore(endDate)) {
                    allMonthsInPeriod.push(startDate.format("MM/YYYY"));
                    startDate = startDate.add(1, "month");
                }
                cantmeses = parseInt(allMonthsInPeriod.length);
                apmensual = (costo_ap / cantmeses)+porcentaje(costo_ap,1.13);
                costoPrimerMes = costo_paquete + apmensual + instalacion;
                demasMeses = costo_paquete + apmensual;
                console.log(allMonthsInPeriod.length);

                // console.log(costo_ap);
                // console.log(duracion_contrato*13);

                $('#ayuda').css("display","none");

                $('#resul_plan').val(costo_paquete.toFixed(1));
                $('#resul_instalacion').val(instalacion.toFixed(1));
                $('#resul_ap').val(apmensual.toFixed(1));
                $('#resul_total').val(costoPrimerMes.toFixed(1));
                $('#resul_demas_meses_plan').val(costo_paquete.toFixed(1));
                $('#resul_demas_meses_ap').val(apmensual.toFixed(1));
                $('#resul_demas_meses_total').val(demasMeses.toFixed(1));
            }


            detalle_costos.css('display','block');
        }else{
            detalle_costos.css('display','none');
            $('#ayuda').css("display","block");

        }
    }
    $(document).on('change',"#costo_paquete,#costo_ap,#costo_instalacion,#mensual,#contado,#duracion_contrato",function(){
        /*var duracion_contrato= $('#duracion_contrato').val();

        if(duracion_contrato != '')*/
            calcularCostos();
    });



///calcularCostos();
    $(document).on('change','#iniciocontrato,#duracion_contrato',function(){

        var duracionContrato = parseInt($('#duracion_contrato').val()),
            inicioContrato = $('#iniciocontrato').val(),
            diaInicioContrato = moment(inicioContrato,'DD.MM.YYYY').date();
           diaCorte = moment(inicioContrato,'DD.MM.YYYY hh:mm:ss a').add(1,'days').date();

        if(duracionContrato > 0)
        {
            finContrato = moment(inicioContrato,'DD.MM.YYYY hh:mm:ss a').add(duracionContrato,'years').format('DD/MM/YYYY');
            $('#fincontrato').val(finContrato);
            $('#fechapago').val(diaInicioContrato+' de cada mes');
            $('#fechacorte').val(diaCorte+' de cada mes');
            calcularCostos();
        }

    });
</script>

@endsection

