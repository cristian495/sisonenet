@extends('webPage.layout.app')

@section('title', 'Felicidades')


@section('content')
<style>

</style>

<main style="max-width: 1280px; margin:auto auto 120px auto">

    <div class="alert alert-success" style="font-size: 18px">
        <span class="info-box-icon bg-green"><i class="fa fa-check"></i></span>
        <strong>Felicidades!</strong> la adquisicion del plan
        "{{session('cliente_plan.plan.nombre_plan')}}"
        se efectuó correctamente.
    </div>

    <p class="exito_descripcion" style="font-size: 17px">
        Puede consultar online sus pagos hechos haciendo click
        <a href="{{url('/login')}}" class="">Aquí</a>, o
        ingresando al siguiente enlace.
    </p>
    <div class="panel-group">
        <div class="panel panel-default">
            <div class="panel-body"><strong>{{url('login')}}</strong></div>
        </div>
    </div>



    <div class="alert alert-info fade in alert-dismissable" style="font-size: 17px">
        <strong>Recuerde!</strong> <br>Para poder acceder a sus consultas online debe ingresar el usuario y contraseña
        que se le envió al correo: <strong>{{session('cliente_plan.cliente.email')}} </strong>.
    </div>

    <a href="{{'/contrato_pdf'}}" target="_blank" class="btn btn-success">Descargar contrato</a>

</main>
@endsection



@section('scripts')
@endsection
