

<div id="msj" class="msj"></div>
<input type="hidden" id="where_i_am" value="section_usuario_logeado"/>
<form  id="f_editar_user_loged"  method="post"  action="consultas/cambiar_contrasenia" class="formentrada" enctype="multipart/form-data">
    {{ csrf_field() }}
        <div class="row">
            <input type="hidden" value="{{$user_client->id}}" name="idusuario"/>
            <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12" >
                <div class="form-group">
                    <label for="precio">Usuario<span class="obligatorio">*</span></label>
                    <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                        <input type="text"  value="{{$user_client->name}}"   class="form-control"  readonly />
                        <div style="cursor: pointer" class="input-group-addon"><span class="fa fa-user"></span></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12" >
                <div class="form-group">
                    <label for="precio">Email <span class="obligatorio">*</span></label>
                    <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                        <input type="text"  value="{{$user_client->email}}" class="form-control" readonly />
                        <div style="cursor: pointer" class="input-group-addon"><span class="fa fa-envelope"></span></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12" >
                <div class="form-group">
                    <label for="precio">Nueva contraseña<span class="obligatorio">*</span></label>
                    <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                        <input type="text"  name="new_pass"  class="form-control" placeholder="Nueva contraseña..." />
                        <div style="cursor: pointer" class="input-group-addon"><span class="fa fa-lock"></span></div>
                    </div>
                </div>
            </div>

        </div>


    <div class="row">


        <div class="col-lg-11 col-sm-12 col-md-6 col-xs-12" >
            <div class="form-group">
                <button class="btn btn-primary" type="submit">Cambiar contraseña</button>
            </div>
        </div>
    </div>

</form>

<script>


</script>